package com.innomalist.taxi.rider.location;

import android.text.TextUtils;
import android.util.Log;

import com.innomalist.taxi.rider.events.UserLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.innomalist.taxi.common.utils.Config.MAP_API_KEY;

public class LocationUtil {

    public static ArrayList<UserLocation> getAutoSuggestionLocations(String input, String pickupLatLng) {
        ArrayList<UserLocation> resultList = new ArrayList<>();
        StringBuilder jsonResults = new StringBuilder();
        HttpURLConnection conn = null;
        try {
            StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/autocomplete/json");
            sb.append("?key=" + MAP_API_KEY);
            sb.append("&components=country:IN");
            if (!TextUtils.isEmpty(pickupLatLng)) {
                sb.append("&radius=40000");
                sb.append("&location=");
                sb.append(pickupLatLng);
                sb.append("&strictbounds");
            }
            sb.append("&input=");
            sb.append(URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            Log.e("url", "url is " + sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                jsonResults.append(line);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject predictions = new JSONObject(jsonResults.toString());
            JSONArray ja = new JSONArray(predictions.getString("predictions"));

            for (int i = 0; i < ja.length(); i++) {
                JSONObject jo = (JSONObject) ja.get(i);
                UserLocation location = new UserLocation();
                location.setLocality(jo.getString("description"));
                location.setPlaceId(jo.getString("place_id"));
                resultList.add(location);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resultList;
    }
}