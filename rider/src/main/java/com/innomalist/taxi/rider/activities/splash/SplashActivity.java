package com.innomalist.taxi.rider.activities.splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.activities.signup.SignUpSecondStepActivity;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.events.BackgroundServiceStartedEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageResultEvent;
import com.innomalist.taxi.common.events.ConnectEvent;
import com.innomalist.taxi.common.events.ConnectResultEvent;
import com.innomalist.taxi.common.events.GetAppVersionResultEvent;
import com.innomalist.taxi.common.events.LoginEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.models.AppVersion;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.models.RiderResultEvent;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.LocationHelper;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.BuildConfig;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.RiderEventBusIndex;
import com.innomalist.taxi.rider.activities.main.MainActivity;
import com.innomalist.taxi.rider.databinding.ActivitySplashBinding;
import com.innomalist.taxi.rider.services.RiderService;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseActivity implements LocationListener, LanguageChooseDialog.LanguageChangeListener {
    private static final int REGISTER_INTENT = 124;
    MyPreferenceManager mPref;
    ActivitySplashBinding binding;
    int RC_SIGN_IN = 123;
    Handler locationTimeoutHandler;
    LocationManager locationManager;
    LatLng currentLocation;
    private long mMobileNumber;
    private int count = 0;
    private boolean isLoginClicked = false;
    private boolean isRegInProcess = false;
    private String mProfilePicPath;

    private PermissionListener permissionlistener = new PermissionListener() {
        @SuppressLint("MissingPermission")
        @Override
        public void onPermissionGranted() {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(SplashActivity.this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(SplashActivity.this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        }
                    });
            boolean isServiceRunning = isMyServiceRunning(RiderService.class);
            if (!isServiceRunning)
                startService(new Intent(SplashActivity.this, RiderService.class));
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            boolean isServiceRunning = isMyServiceRunning(RiderService.class);
            if (!isServiceRunning)
                startService(new Intent(SplashActivity.this, RiderService.class));
        }
    };
  /*  private View.OnClickListener onLoginButtonClicked = v -> {
        String resourceName = "testMode";
        int testExists = SplashActivity.this.getResources().getIdentifier(resourceName, "string", SplashActivity.this.getPackageName());
        if (testExists > 0) {
            tryLogin(getString(testExists));
            return;
        }
        if (getResources().getBoolean(R.bool.use_custom_login)) {
            startActivityForResult(new Intent(SplashActivity.this, LoginActivity.class), RC_SIGN_IN);
        } else
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .setTheme(getCurrentTheme())
                            .build(),
                    RC_SIGN_IN);
    };*/


    private void startNumberVerification() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                        .setTheme(getCurrentTheme())
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setImmersive(true);
        showConnectionDialog = false;
        OneSignal.setSubscription(true);
        try {
            EventBus.builder().addIndex(new RiderEventBusIndex()).installDefaultEventBus();
        } catch (Exception ignored) {
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(SplashActivity.this, R.layout.activity_splash);
        binding.loginButton.setOnClickListener(view -> {
            isLoginClicked = true;
            startNumberVerification();
        });
        binding.registrationButton.setOnClickListener(view -> {
            isLoginClicked = false;
            startNumberVerification();
        });
        mPref = MyPreferenceManager.getInstance(this);
        checkPermissions();

        if (mPref.getString("LANG", null) == null) {
            mPref.putString("LANG", "en");
        }

        binding.chooseLanguage.setText(mPref.getString("LANG", "EN"));
        binding.chooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LanguageChooseDialog languageChooseDialog = new LanguageChooseDialog(SplashActivity.this);
                languageChooseDialog.setLanguageChangeListener(SplashActivity.this);
                languageChooseDialog.show();
            }
        });
        isRegInProcess = false;
    }

    private void checkPermissions() {
        if (CommonUtils.isInternetDisabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_enable_wifi), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    @SuppressLint("MissingPermission")
    private void searchCurrentLocation() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 1, this);
    }

    private void startMainActivity(LatLng latLng) {
        String token = mPref.getString("rider_token", null);
        if (TextUtils.isEmpty(token)) {
            appFinishDialog(getString(R.string.relogin));
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        double[] array = LocationHelper.LatLngToDoubleArray(latLng);
        intent.putExtra("currentLocation", array);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginResultEvent(RiderResultEvent event) {
        if (event.status == 200) {
            CommonUtils.rider = event.user;
            mPref.putString("rider_user", new Gson().toJson(event.user, Rider.class));
            mPref.putString("rider_token", event.token);
            tryConnect();
        } else if (event.status == 411) {
            appFinishDialog(getString(R.string.disable_msg));
        } else if (event.status == 412) {
            appFinishDialog(getString(R.string.blocked_msg));
        } else if (event.status == 413) {
            tryRegistration("" + mMobileNumber);
        }
    }

    public void tryConnect() {
        String token = mPref.getString("rider_token", null);
        if (token != null && !token.isEmpty()) {
            eventBus.post(new ConnectEvent(token));
            goToLoadingMode();
        } else {
            goToLoginMode();
        }
    }


    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        tryConnect();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        if (event.hasError()) {
            binding.progressBar.setVisibility(View.GONE);
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    tryConnect();
                } else {
                    goToLoginMode();
                }
            });
            return;
        }

        if (!isLoginClicked && isRegInProcess) {
            EventBus.getDefault().post(new ChangeProfileImageEvent(mProfilePicPath));
        } else {
            CommonUtils.rider = Rider.fromJson(mPref.getString("rider_user", "{}"));
            eventBus.post(new AppVersion());
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileImageChanged(ChangeProfileImageResultEvent event) {
        if (event.hasError()) {
            Toast.makeText(this, getString(R.string.unable_upload_file), Toast.LENGTH_SHORT).show();
            return;
        }
        CommonUtils.rider.setMedia(event.media);
        mPref.putString("rider_user", new Gson().toJson(CommonUtils.rider, Rider.class));
        CommonUtils.rider = Rider.fromJson(mPref.getString("rider_user", "{}"));

        eventBus.post(new AppVersion());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAppVersionResult(GetAppVersionResultEvent event) {
        if (event.status == 200) {
            Log.e("TAg", "Ride Version : " + event.data.rider_min_ver_android);
            if (count == 0) {
                count++;
                if (BuildConfig.VERSION_CODE < event.data.rider_min_ver_android) {
                    showVersionAlert();
                } else {
                    sendNotificationPlayerIdToServer();
                }
            }
        } else {
            mPref.clearPreferences();
            appFinishDialog(getString(R.string.network_issue_msg));
        }


    }

    private void sendNotificationPlayerIdToServer() {
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();

        if (status != null) {
            eventBus.post(new NotificationPlayerId(status.getSubscriptionStatus().getUserId()));
        } else {
            OneSignal.idsAvailable((userId, registrationId) -> {
                Log.d("debug", "User:" + userId);
                eventBus.post(new NotificationPlayerId(userId));

            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationSentResponse(CommonResponse event) {
        Log.e("TAg", "onNotificationSentResponse: " + event.status);
        if (event.status == 200) {
            getCurrentLocationAndMoveNextActivity();
        } else {
            mPref.clearPreferences();
            appFinishDialog(getString(R.string.login_issue_msg));
        }
    }


    private void getCurrentLocationAndMoveNextActivity() {
        if (currentLocation != null) {
            startMainActivity(currentLocation);
        } else {

            locationTimeoutHandler.postDelayed(() -> {
                locationManager.removeUpdates(this);
                if (currentLocation == null) {
                    String[] location = getString(R.string.defaultLocation).split(",");
                    double lat = Double.parseDouble(location[0]);
                    double lng = Double.parseDouble(location[1]);
                    currentLocation = new LatLng(lat, lng);
                }
                startMainActivity(currentLocation);

            }, 5000);
            searchCurrentLocation();
        }
    }


    private void tryLogin(String phone) {
        goToLoadingMode();
        if (phone.substring(0, 1).equals("+"))
            phone = phone.substring(1);

        mMobileNumber = Long.valueOf(phone);
        eventBus.post(new LoginEvent(Long.valueOf(phone), BuildConfig.VERSION_CODE));
    }

    private void goToLoadingMode() {
        binding.llSigninSignup.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void goToLoginMode() {
        binding.llSigninSignup.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                if (getResources().getBoolean(R.bool.use_custom_login)) {
                    tryLogin(data.getStringExtra("mobile"));
                } else {
                    IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                    String phone;
                    if (idpResponse != null) {
                        phone = idpResponse.getPhoneNumber();
                        if (isLoginClicked) {
                            tryLogin(phone);
                        } else {
                            tryRegistration(phone);
                        }
                        return;
                    }
                }
                return;
            }
        } else if (requestCode == REGISTER_INTENT) {
            if (resultCode == RESULT_OK) {
                int status = data.getIntExtra("statusCode", -1);
                if (status == 200) {
                    mProfilePicPath = data.getStringExtra("profilePicPath");
                    if (!TextUtils.isEmpty(mProfilePicPath)) {
                        isRegInProcess = true;
                    }
                    tryConnect();
                } else if (status == 413) {
                    appFinishDialog(getString(R.string.already_exist_msg));
                } else {
                    appFinishDialog(getString(R.string.unknown_error_msg));
                }
            } else {
                finish();
            }
            return;
        }
        AlerterHelper.showError(SplashActivity.this, getString(R.string.login_failed));
        goToLoginMode();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onLanguageSelected(LanguageChooseDialog.Language language) {
        Toast.makeText(this, String.valueOf(language), Toast.LENGTH_LONG).show();
        if (language == LanguageChooseDialog.Language.HINDI) {
            mPref.putString("LANG", "HI");
            LocaleHelper.setLocale(this, "hi");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ENGLISH) {
            mPref.putString("LANG", "EN");
            LocaleHelper.setLocale(this, "en");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.SPANISH) {
            mPref.putString("LANG", "ES");
            LocaleHelper.setLocale(this, "es");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.BENGALI) {
            mPref.putString("LANG", "BN");
            LocaleHelper.setLocale(this, "bn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PUNJABI) {
            mPref.putString("LANG", "PA");
            LocaleHelper.setLocale(this, "pa");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.FRENCH) {
            mPref.putString("LANG", "FR");
            LocaleHelper.setLocale(this, "fr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ARABIC) {
            mPref.putString("LANG", "AR");
            LocaleHelper.setLocale(this, "ar");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.IRISH) {
            mPref.putString("LANG", "GA");
            LocaleHelper.setLocale(this, "ga");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GUJARATI) {
            mPref.putString("LANG", "GU");
            LocaleHelper.setLocale(this, "gu");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GERMAN) {
            mPref.putString("LANG", "DE");
            LocaleHelper.setLocale(this, "de");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.INDONESIAN) {
            mPref.putString("LANG", "ID");
            LocaleHelper.setLocale(this, "id");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ITALIAN) {
            mPref.putString("LANG", "IT");
            LocaleHelper.setLocale(this, "it");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KANNADA) {
            mPref.putString("LANG", "KN");
            LocaleHelper.setLocale(this, "kn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KOREAN) {
            mPref.putString("LANG", "KO");
            LocaleHelper.setLocale(this, "ko");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAYALAM) {
            mPref.putString("LANG", "ML");
            LocaleHelper.setLocale(this, "ml");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAY) {
            mPref.putString("LANG", "MS");
            LocaleHelper.setLocale(this, "ms");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MARATHI) {
            mPref.putString("LANG", "MR");
            LocaleHelper.setLocale(this, "mr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.NEPALI) {
            mPref.putString("LANG", "NE");
            LocaleHelper.setLocale(this, "ne");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.DUTCH) {
            mPref.putString("LANG", "NL");
            LocaleHelper.setLocale(this, "nl");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ORIYA) {
            mPref.putString("LANG", "OR");
            LocaleHelper.setLocale(this, "or");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PORTUGUESE) {
            mPref.putString("LANG", "PT");
            LocaleHelper.setLocale(this, "pt");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.RUSSIAN) {
            mPref.putString("LANG", "RU");
            LocaleHelper.setLocale(this, "ru");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TAMIL) {
            mPref.putString("LANG", "TA");
            LocaleHelper.setLocale(this, "ta");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TELUGU) {
            mPref.putString("LANG", "TE");
            LocaleHelper.setLocale(this, "te");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.URDU) {
            mPref.putString("LANG", "UR");
            LocaleHelper.setLocale(this, "ur");
            this.recreate();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleHelper.onAttach(newBase);
        super.attachBaseContext(newBase);
    }


    private void showVersionAlert() {
        new AlertDialog.Builder(this).setMessage(getString(R.string.imp_update_msg))
                .setTitle("Alert")
                .setPositiveButton("Ok", (dialog, which) -> {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {

                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }).setNegativeButton("No", (dialog, which) -> {

        }).create().show();
    }

    private void tryRegistration(String mobileNumber) {

        startActivityForResult(new Intent(this, SignUpSecondStepActivity.class).putExtra("User", getString(R.string.user_registration)).putExtra("Mobile", "" + mobileNumber), REGISTER_INTENT);
    }

    private void appFinishDialog(String string) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.test))
                .cancelable(false)
                .content(string)
                .positiveText(com.innomalist.taxi.common.R.string.alert_ok)
                .positiveText(getString(R.string.alert_ok))
                .onPositive((dialog, which) -> {
                    mPref.clearPreferences();
                    finish();
                }).show();
    }
}
