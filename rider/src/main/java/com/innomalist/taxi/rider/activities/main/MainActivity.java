package com.innomalist.taxi.rider.activities.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.afollestad.materialdialogs.MaterialDialog;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.activities.chargeAccount.QCabPaymentActivity;
import com.innomalist.taxi.common.activities.transactions.TransactionsActivity;
import com.innomalist.taxi.common.activities.travels.TravelsActivity;
import com.innomalist.taxi.common.events.GetStatusEvent;
import com.innomalist.taxi.common.events.GetStatusResultEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.events.ProfileInfoChangedEvent;
import com.innomalist.taxi.common.location.MapHelper;
import com.innomalist.taxi.common.models.CRUD;
import com.innomalist.taxi.common.models.DriverLocation;
import com.innomalist.taxi.common.models.Service;
import com.innomalist.taxi.common.models.ServiceCategory;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.DataBinder;
import com.innomalist.taxi.common.utils.ItemClickSupport;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.LocationHelper;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.activities.about.AboutActivity;
import com.innomalist.taxi.rider.activities.addresses.AddressesActivity;
import com.innomalist.taxi.rider.activities.coupon.CouponActivity;
import com.innomalist.taxi.rider.activities.main.adapters.ServiceCategoryViewPagerAdapter;
import com.innomalist.taxi.rider.activities.main.adapters.ServicesListAdapter;
import com.innomalist.taxi.rider.activities.main.dialogs.DriverAcceptedDialog;
import com.innomalist.taxi.rider.activities.profile.ProfileActivity;
import com.innomalist.taxi.rider.activities.promotions.PromotionsActivity;
import com.innomalist.taxi.rider.activities.splash.LanguageChooseDialog;
import com.innomalist.taxi.rider.activities.travel.TravelActivity;
import com.innomalist.taxi.rider.databinding.ActivityMainBinding;
import com.innomalist.taxi.rider.events.AcceptDriverUIEvent;
import com.innomalist.taxi.rider.events.CRUDAddressRequestEvent;
import com.innomalist.taxi.rider.events.CRUDAddressResultEvent;
import com.innomalist.taxi.rider.events.CalculateFareRequestEvent;
import com.innomalist.taxi.rider.events.CalculateFareResultEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationResultEvent;
import com.innomalist.taxi.rider.events.ServiceRequestErrorEvent;
import com.innomalist.taxi.rider.events.UserLocation;
import com.innomalist.taxi.rider.ui.RiderBaseActivity;
import com.innomalist.taxi.rider.ui.gravitySnapHelper.GravitySnapHelper;
import com.innomalist.taxi.rider.ui.trail.TrailSupportMapFragment;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import static com.innomalist.taxi.common.utils.Config.MAP_API_KEY;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class MainActivity extends RiderBaseActivity implements OnMapReadyCallback, LanguageChooseDialog.LanguageChangeListener, LocationListener, OSSubscriptionObserver {

    private ActivityMainBinding binding;
    private MyPreferenceManager mPref;
    private GoogleMap mMap;
    private Marker pickupPoint;
    private Marker destinationPoint;
    private MarkerMode markerMode = MarkerMode.origin;
    private DriverAcceptedDialog driverAcceptedDialog;
    private static final int ACTIVITY_PROFILE = 11;
    private static final int ACTIVITY_WALLET = 12;
    private static final int ACTIVITY_PLACES = 13;
    private static final int ACTIVITY_TRAVEL = 14;
    private static final int ACTIVITY_VOICE_RECOGNITION = 15;
    private static final int SEARCH_LOCATION_REQEST_CODE = 16;
    private ServiceCategoryViewPagerAdapter serviceCategoryViewPagerAdapter;
    private BottomSheetBehavior bottomSheetBehavior;
    private LatLng currentLocation;
    public Service selectedService;
    private Travel travel = new Travel();
    private HashMap<Integer, Marker> driverMarkers;
    private boolean isPickupSelect = true;
    private Timer timer;
    private TimerTask timerTask;
    private boolean isResponseReceived;
    private boolean isCameraMoveStopped = true;
    private int lastSelectedPos;

    public void onServiceSelected(Service service) {
        selectedService = service;
        travel.setServiceId("" + service.getId());
        binding.buttonRequest.setEnabled(true);
        binding.buttonRequest.setText(getString(R.string.confirm_service, service.getTitle()));
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom().getSubscribed() && stateChanges.getTo().getSubscribed())
            eventBus.post(new NotificationPlayerId(stateChanges.getTo().getUserId()));
    }

    @Override
    public void onLanguageSelected(LanguageChooseDialog.Language language) {
        Toast.makeText(this, String.valueOf(language), Toast.LENGTH_LONG).show();
        if (language == LanguageChooseDialog.Language.HINDI) {
            mPref.putString("LANG", "HI");
            LocaleHelper.setLocale(this, "hi");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ENGLISH) {
            mPref.putString("LANG", "EN");
            LocaleHelper.setLocale(this, "en");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.SPANISH) {
            mPref.putString("LANG", "ES");
            LocaleHelper.setLocale(this, "es");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.BENGALI) {
            mPref.putString("LANG", "BN");
            LocaleHelper.setLocale(this, "bn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PUNJABI) {
            mPref.putString("LANG", "PA");
            LocaleHelper.setLocale(this, "pa");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.FRENCH) {
            mPref.putString("LANG", "FR");
            LocaleHelper.setLocale(this, "fr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ARABIC) {
            mPref.putString("LANG", "AR");
            LocaleHelper.setLocale(this, "ar");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.IRISH) {
            mPref.putString("LANG", "GA");
            LocaleHelper.setLocale(this, "ga");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GUJARATI) {
            mPref.putString("LANG", "GU");
            LocaleHelper.setLocale(this, "gu");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GERMAN) {
            mPref.putString("LANG", "DE");
            LocaleHelper.setLocale(this, "de");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.INDONESIAN) {
            mPref.putString("LANG", "ID");
            LocaleHelper.setLocale(this, "id");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ITALIAN) {
            mPref.putString("LANG", "IT");
            LocaleHelper.setLocale(this, "it");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KANNADA) {
            mPref.putString("LANG", "KN");
            LocaleHelper.setLocale(this, "kn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KOREAN) {
            mPref.putString("LANG", "KO");
            LocaleHelper.setLocale(this, "ko");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAYALAM) {
            mPref.putString("LANG", "ML");
            LocaleHelper.setLocale(this, "ml");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAY) {
            mPref.putString("LANG", "MS");
            LocaleHelper.setLocale(this, "ms");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MARATHI) {
            mPref.putString("LANG", "MR");
            LocaleHelper.setLocale(this, "mr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.NEPALI) {
            mPref.putString("LANG", "NE");
            LocaleHelper.setLocale(this, "ne");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.DUTCH) {
            mPref.putString("LANG", "NL");
            LocaleHelper.setLocale(this, "nl");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ORIYA) {
            mPref.putString("LANG", "OR");
            LocaleHelper.setLocale(this, "or");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PORTUGUESE) {
            mPref.putString("LANG", "PT");
            LocaleHelper.setLocale(this, "pt");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.RUSSIAN) {
            mPref.putString("LANG", "RU");
            LocaleHelper.setLocale(this, "ru");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TAMIL) {
            mPref.putString("LANG", "TA");
            LocaleHelper.setLocale(this, "ta");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TELUGU) {
            mPref.putString("LANG", "TE");
            LocaleHelper.setLocale(this, "te");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.URDU) {
            mPref.putString("LANG", "UR");
            LocaleHelper.setLocale(this, "ur");
            this.recreate();
        }
    }

    private enum MarkerMode {
        origin,
        destination,
        serviceSelection
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // super.setImmersive(true);
        super.onCreate(savedInstanceState);
        OneSignal.addSubscriptionObserver(this);
        //Places.initialize(getApplicationContext(), AUTOCOMPLETE_API_KEY);
        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
        isResponseReceived = false;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        initializeToolbar("", false);
        showProgress();
        eventBus.post(new GetStatusEvent());

        if (getIntent().getDoubleArrayExtra("currentLocation") != null) {
            currentLocation = LocationHelper.DoubleArrayToLatLng(getIntent().getDoubleArrayExtra("currentLocation"));
        }

        binding.buttonConfirmPickup.setEnabled(false);
        binding.buttonConfirmPickup.setOnClickListener(view -> onButtonConfirmPickupClicked());
        binding.buttonConfirmDestination.setOnClickListener(view -> onButtonConfirmDestinationClicked());
        binding.buttonRequest.setOnClickListener(view -> onButtonConfirmServiceClicked());
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        binding.searchText.setSelected(true);
        binding.drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });


        binding.searchText.setOnClickListener(view -> {
            isPickupSelect = true;
            findPlace("");
        });

        binding.searchText.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (binding.searchText.getRight() - binding.searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    Log.e("TAg", "setOnTouchListener");
                    isPickupSelect = true;
                    displaySpeechRecognizer();
                    return true;
                }
            }
            return false;
        });

        binding.destination.setOnClickListener(view -> {
            isPickupSelect = false;
            markerMode = MarkerMode.destination;
            cancelTimer();
            findPlace("");
        });

        binding.destination.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (binding.destination.getRight() - binding.destination.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    Log.e("TAg", "setOnTouchListener");
                    isPickupSelect = false;
                    displaySpeechRecognizer();
                    return true;
                }
            }
            return false;
        });

        driverMarkers = new HashMap<>();
        mPref = MyPreferenceManager.getInstance(getApplicationContext());
        if (mPref.getString("LANG", null) == null) {
            mPref.putString("LANG", "EN");
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.qcab));
            //actionBar.setSubtitle(getString(R.string.app_tagline));
        }
        if (binding.navigationView != null) {
            binding.navigationView.setNavigationItemSelectedListener(menuItem -> {
                binding.drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case (R.id.nav_item_favorites):
                        Intent intent = new Intent(MainActivity.this, AddressesActivity.class);
                        double[] array = LocationHelper.LatLngToDoubleArray(currentLocation);
                        intent.putExtra("currentLocation", array);
                        startActivity(intent);
                        break;
                    case (R.id.nav_item_travels):
                        startActivity(new Intent(MainActivity.this, TravelsActivity.class));
                        break;
                    case (R.id.nav_item_promotions):
                        startActivity(new Intent(MainActivity.this, PromotionsActivity.class));
                        break;
                    case (R.id.nav_item_profile):
                        startActivityForResult(new Intent(MainActivity.this, ProfileActivity.class), ACTIVITY_PROFILE);
                        break;
                    case (R.id.nav_item_charge_account):
                        startActivityForResult(new Intent(MainActivity.this, QCabPaymentActivity.class), ACTIVITY_WALLET);
                        break;
                    case (R.id.nav_item_transactions):
                        startActivity(new Intent(MainActivity.this, TransactionsActivity.class));
                        break;
                    case (R.id.nav_item_coupons):
                        startActivity(new Intent(MainActivity.this, CouponActivity.class));
                        break;
                    case (R.id.nav_item_about):
                        startActivity(new Intent(MainActivity.this, AboutActivity.class));
                        break;
                    case (R.id.nav_item_exit):
                        AlertDialogBuilder.show(MainActivity.this, getString(R.string.message_logout), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                            if (result == AlertDialogBuilder.DialogResult.OK)
                                logout();
                        });
                        break;
                    default:
                        Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            });
        }

        fillInfo();
    }


    void getAllDriverLocationTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                runOnUiThread(() -> {
                    Log.e("TAG", "getAllDriverLocationTask called");
                    eventBus.post(new GetDriversLocationEvent(mMap.getCameraPosition().target));
                });
            }
        };

        timer.schedule(timerTask, 1000, 5000);
    }

    void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timerTask = null;
        }
    }

    private void logout() {
        mPref.clearPreferences();
        finish();
    }

    private void showCurvedPolyline(LatLng p1, LatLng p2, double k) {

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .key(MAP_API_KEY)
                .withListener(new RoutingListener() {
                    @Override
                    public void onRoutingFailure(RouteException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onRoutingStart() {
                        Log.e("Started", "started");
                    }

                    @Override
                    public void onRoutingSuccess(ArrayList<Route> route, int position) {

                        PolylineOptions polyOptions = new PolylineOptions();
                        polyOptions.color(Color.parseColor("#950534"));
                        polyOptions.width(10);
                        polyOptions.zIndex(1000);
                        polyOptions.addAll(route.get(position).getPoints());
                        MapAnimator.getInstance().animateRoute(mMap, route.get(position).getPoints());

                    }

                    @Override
                    public void onRoutingCancelled() {
                        Log.e("Cancelled", "Cancelled");
                    }
                })
                .waypoints(p1, p2)
                .build();
        routing.execute();
    }

    public void findPlace(String initialQuery) {
        try {

            AutocompleteFilter autocompleteFilter = (new AutocompleteFilter.Builder()).setCountry("IN").build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(autocompleteFilter)
                            .build(this);
            if (!TextUtils.isEmpty(initialQuery)) {
                intent.putExtra("initial_query", initialQuery);
            }
            startActivityForResult(intent, ACTIVITY_PLACES);

            /*if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), AUTOCOMPLETE_API_KEY);
            }

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.NAME, Place.Field.LAT_LNG);
            Intent intent;
            if (isPickupSelect) {
                intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields).setInitialQuery(initialQuery).setCountry("IN")
                        .build(this);
            } else {
                intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields).setInitialQuery(initialQuery).setCountry("IN").setLocationRestriction(toBounds(new LatLng(travel.getPickupPoint().latitude, travel.getPickupPoint().longitude), 40000))
                        .build(this);
            }

            startActivityForResult(intent, ACTIVITY_PLACES);*/

/*             Fully Customizations if search ui and implementation
            Intent intent = new Intent(this, SearchLocationActivity.class);

            if (!isPickupSelect) {
                intent.putExtra("PickupLatLng", travel.getPickupPoint().latitude + "," + travel.getPickupPoint().longitude);
            }
            if (!TextUtils.isEmpty(initialQuery)) {
                intent.putExtra("InitialQuery", initialQuery);
            }
            startActivityForResult(intent, SEARCH_LOCATION_REQEST_CODE);*/

        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
    }

    /*public RectangularBounds toBounds(LatLng center, double radiusInMeters) {

        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);

        return RectangularBounds.newInstance(southwestCorner, northeastCorner);
    }
*/
    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.RECORD_AUDIO)
                .check();
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            try {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, getString(R.string.default_language));

                MainActivity.this.startActivityForResult(intent, ACTIVITY_VOICE_RECOGNITION);
            } catch (ActivityNotFoundException e) {
                AlertDialogBuilder.show(MainActivity.this, getString(R.string.question_install_speech), getString(R.string.error), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                    if (result == AlertDialogBuilder.DialogResult.OK) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.voicesearch"));
                        startActivity(browserIntent);
                    }
                });
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };

    @Subscribe(threadMode = MAIN)
    public void onAddressesReceived(CRUDAddressResultEvent event) {
        if (event.crud != CRUD.READ || !isInForeground)
            return;
        if (event.addresses.size() < 1) {
            AlerterHelper.showWarning(MainActivity.this, getString(R.string.warning_no_favorite_place));
            return;
        }
        List<String> addressStrings = new ArrayList<>();
        for (com.innomalist.taxi.common.models.Address address :
                event.addresses) {
            addressStrings.add(address.getTitle());
        }
        new MaterialDialog.Builder(this)
                .title(R.string.drawer_favorite_locations)
                .items(addressStrings)
                .itemsCallback((dialog, view, which, text) -> {
                    if (event.addresses.get(which).getLocation() != null) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(event.addresses.get(which).getLocation()));
                    }
                    binding.searchText.setText(event.addresses.get(which).getAddress());

                })
                .show();
    }

    private void onButtonConfirmPickupClicked() {
        binding.buttonConfirmPickup.setEnabled(false);
        binding.buttonConfirmDestination.setEnabled(false);
        //showDestinationMarker();
        //TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), (new TransitionSet()).addTransition(new Slide()).addTransition(new Fade()));
        binding.buttonConfirmPickup.setVisibility(View.GONE);
        binding.buttonConfirmDestination.setVisibility(View.VISIBLE);
        markerMode = MarkerMode.destination;
        //binding.searchPlace.openMenu(true);
        travel.setPickupPoint(mMap.getCameraPosition().target);
        pickupPoint = mMap.addMarker(new MarkerOptions()
                .position(travel.getPickupPoint())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_pickup)));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mMap.getCameraPosition().target.latitude + 0.001, mMap.getCameraPosition().target.longitude)));
    }

    private void onButtonConfirmDestinationClicked() {
        cancelTimer();
        if (travel.getPickupPoint() == null || travel.getDestinationPoint() == null) {
            // goBackFromServiceSelection();
            return;
        }
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        binding.buttonRequest.setEnabled(false);
        mMap.clear();
        driverMarkers.clear();
        hideMarkers();
        markerMode = MarkerMode.serviceSelection;
        binding.imageDestination.setVisibility(View.GONE);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        // travel.setDestinationPoint(mMap.getCameraPosition().target);
        mMap.setPadding(0, binding.bottomSheet.getHeight() / 10, 0, binding.bottomSheet.getHeight());
        List<LatLng> latLngs = new ArrayList<>();
        latLngs.add(travel.getPickupPoint());
        latLngs.add(travel.getDestinationPoint());
        if (pickupPoint != null)
            pickupPoint.remove();
        MapHelper.centerLatLngsInMap(mMap, latLngs, true);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        binding.mapLayout.postDelayed(() -> {
            Bitmap pickUpBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.marker_pickup);
            pickupPoint = mMap.addMarker(new MarkerOptions().position(travel.getPickupPoint()).icon(BitmapDescriptorFactory.fromBitmap(pickUpBitmap)));
            Bitmap dropBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.marker_destination);
            destinationPoint = mMap.addMarker(new MarkerOptions().position(travel.getDestinationPoint()).icon(BitmapDescriptorFactory.fromBitmap(dropBitmap)));
            showCurvedPolyline(travel.getPickupPoint(), travel.getDestinationPoint(), 0.2);
        }, 1500);
        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), (new TransitionSet()).addTransition(new Fade()));
        binding.buttonConfirmDestination.setVisibility(View.GONE);
        binding.searchText.setEnabled(false);
        binding.destination.setEnabled(false);
        //getSupportActionBar().hide();
        eventBus.post(new CalculateFareRequestEvent(travel.getPickupPoint(), travel.getDestinationPoint()));
    }


    private void goBackFromDestinationSelection() {
        isPickupSelect = true;
        driverMarkers.clear();
        mMap.clear();
        isCameraMoveStopped = true;
        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), (new TransitionSet()).addTransition(new Slide()).addTransition(new Fade()));
        binding.buttonConfirmDestination.setVisibility(View.GONE);
        binding.buttonConfirmPickup.setVisibility(View.VISIBLE);
        markerMode = MarkerMode.origin;
        if (mMap != null) {
            LatLng finalLatLon;
            if (pickupPoint != null && pickupPoint.getPosition() != null) {
                finalLatLon = pickupPoint.getPosition();
            } else {
                finalLatLon = currentLocation;
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(finalLatLon, 16.0f));
        }
        if (pickupPoint != null) {
            pickupPoint.remove();
        }

        showPickupMarker();
    }

    private void goBackFromServiceSelection() {

        isPickupSelect = true;
        isCameraMoveStopped = true;
        /*if (getSupportActionBar() != null) {
            getSupportActionBar().show();
           // updateLayoutHeight(true);
        }*/
        driverMarkers.clear();
        mMap.clear();
        LatLng finalLatLon;
        if (pickupPoint != null && pickupPoint.getPosition() != null) {
            finalLatLon = pickupPoint.getPosition();
        } else {
            finalLatLon = currentLocation;
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(finalLatLon, 16.0f));
        if (pickupPoint != null) {
            pickupPoint.remove();
        }
        if (destinationPoint != null) {
            destinationPoint.remove();
        }
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        markerMode = MarkerMode.origin;
        showPickupMarker();

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mMap.setPadding(0, 0, 0, 0);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        binding.buttonConfirmPickup.setEnabled(false);
        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), (new TransitionSet()).addTransition(new Fade()));
        binding.buttonConfirmPickup.setVisibility(View.GONE);
        binding.searchText.setVisibility(View.VISIBLE);
        binding.destination.setVisibility(View.VISIBLE);
        binding.searchText.setEnabled(true);
        binding.destination.setEnabled(true);
        binding.destination.setText("");

    }

    private void onButtonConfirmServiceClicked() {
        MapAnimator.getInstance().stopAndRemovePolyLine();
        driverAcceptedDialog = DriverAcceptedDialog.newInstance(travel);
        driverAcceptedDialog.show(getSupportFragmentManager(), "DialogDriversAccepted");
    }

    private void showPickupMarker() {
        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), new Fade());
        if (binding.imageDestination.getVisibility() == View.VISIBLE)
            binding.imageDestination.setVisibility(View.GONE);
        binding.imagePickup.setVisibility(View.VISIBLE);
    }

    private void showDestinationMarker() {
        //   TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), new Fade());
        if (binding.imagePickup.getVisibility() == View.VISIBLE)
            binding.imagePickup.setVisibility(View.GONE);
        binding.imageDestination.setVisibility(View.VISIBLE);
    }

    private void hideMarkers() {
        if (binding.imagePickup.getVisibility() == View.VISIBLE)
            binding.imagePickup.setVisibility(View.GONE);

        if (binding.imageDestination.getVisibility() == View.VISIBLE)
            binding.imageDestination.setVisibility(View.GONE);
    }

    @Subscribe(threadMode = MAIN)
    public void onCalculateFareReceived(CalculateFareResultEvent event) {
        if (event.status != 200) {
            //event.showAlert(this);
            goBackFromServiceSelection();
            return;
        }

        List<Service> modelList = new ArrayList<>();
        for (ServiceCategory serviceCategory : event.data) {
            modelList.addAll(serviceCategory.getServices());
        }

        RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelper = new GravitySnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(recyclerView);
        ServicesListAdapter adapter = new ServicesListAdapter(modelList);
        recyclerView.setAdapter(adapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener((recyclerView1, position, v) -> {
            if (lastSelectedPos != -1) {
                modelList.get(lastSelectedPos).setSelected(false);
            }
            modelList.get(position).setSelected(true);
            onServiceSelected(modelList.get(position));
            lastSelectedPos = position;
            adapter.notifyDataSetChanged();

        });
        /*serviceCategoryViewPagerAdapter = new ServiceCategoryViewPagerAdapter(getSupportFragmentManager(), event.data);
        binding.serviceTypesViewPager.setAdapter(serviceCategoryViewPagerAdapter);
        binding.tabCategories.setupWithViewPager(binding.serviceTypesViewPager);*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view, menu);

        menu.findItem(R.id.action_language).setTitle(mPref.getString("LANG", "EN"));
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                binding.drawerLayout.openDrawer(GravityCompat.START);
                break;
            case (R.id.action_favorites):
                eventBus.post(new CRUDAddressRequestEvent(CRUD.READ));
                break;
            case (R.id.action_location):
                isPickupSelect = true;
                markerMode = MarkerMode.origin;
                isCameraMoveStopped = true;
                isResponseReceived = true;
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16f));
                break;
            case (R.id.action_language):
                LanguageChooseDialog languageChooseDialog = new LanguageChooseDialog(MainActivity.this);
                languageChooseDialog.setLanguageChangeListener(MainActivity.this);
                languageChooseDialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setTrafficEnabled(false);
        mMap.setOnCameraMoveStartedListener(onCameraMoveStartedListener);
        mMap.setOnCameraIdleListener(onCameraIdleListener);

        mMap.setOnCameraMoveListener(() -> ((TrailSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).onCameraMove(mMap));

        if (getResources().getBoolean(R.bool.isNightMode)) {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_night));
        } else {
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_map_style));
        }
        TedPermission.with(MainActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(MainActivity.this, location -> {
                                    if (location != null) {
                                        isCameraMoveStopped = true;
                                        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
                                    }
                                });
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .setDeniedMessage("If not allowed, the invoice would not be saved")
                .check();


    }


    GoogleMap.OnCameraMoveStartedListener onCameraMoveStartedListener = reason -> {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            isCameraMoveStopped = true;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
            isCameraMoveStopped = true;
        }
    };

    GoogleMap.OnCameraIdleListener onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
        @Override
        public void onCameraIdle() {

            if (isCameraMoveStopped) {
                isCameraMoveStopped = false;
                if (markerMode == MarkerMode.origin && isResponseReceived) {
                    Log.e("TAG", "setOnCameraIdleListener: ");
                    GetMarkerAddress task = new GetMarkerAddress();
                    task.execute(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
                    if (timerTask == null || timer == null) {
                        getAllDriverLocationTask();
                    }
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (markerMode == MarkerMode.origin)
            AlertDialogBuilder.show(MainActivity.this, getString(R.string.message_exit), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                if (result == AlertDialogBuilder.DialogResult.OK)
                    MainActivity.this.finishAffinity();
            });
        if (markerMode == MarkerMode.destination)
            goBackFromDestinationSelection();
        if (markerMode == MarkerMode.serviceSelection)
            goBackFromServiceSelection();

        MapAnimator.getInstance().stopAndRemovePolyLine();
    }

    private void fillInfo() {
        try {
            String name;
            if (CommonUtils.rider == null) {
                return;
            }
            if (CommonUtils.rider.status != null && CommonUtils.rider.status.equals("blocked")) {
                logout();
                return;
            }
            if ((CommonUtils.rider.getFirstName() == null || CommonUtils.rider.getFirstName().isEmpty()) && (CommonUtils.rider.getLastName() == null || CommonUtils.rider.getLastName().isEmpty()))
                name = String.valueOf(CommonUtils.rider.getMobileNumber());
            else
                name = CommonUtils.rider.getFirstName() + " " + CommonUtils.rider.getLastName();
            View header = binding.navigationView.getHeaderView(0);
            ((TextView) header.findViewById(R.id.navigation_header_name)).setText(name);
            ((TextView) header.findViewById(R.id.navigation_header_charge)).setText(getString(R.string.drawer_header_balance, CommonUtils.rider.getBalance()));
            ImageView imageView = header.findViewById(R.id.navigation_header_image);
            DataBinder.setMedia(imageView, CommonUtils.rider.getMedia());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onDriversLocationResult(GetDriversLocationResultEvent event) {
        if (event.status != 200 || mMap == null)
            return;
        Log.e("TAG", "Size: " + event.data.size());
        Set<Integer> set = new HashSet<>();
        for (DriverLocation driverLocation : event.data) {
            Marker marker = driverMarkers.get(driverLocation.getDriver_id());
            LatLng latLng = new LatLng(driverLocation.getLat(), driverLocation.getLng());
            if (marker == null) {
                driverMarkers.put(driverLocation.getDriver_id(), mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .anchor(.5f, .5f)
                        .flat(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon))));
            } else {
                MapHelper.animateMarkerToLocation(latLng, marker);
            }
            set.add(driverLocation.getDriver_id());
        }

        Map<Integer, Marker> tempMap = new HashMap<>();
        tempMap.putAll(driverMarkers);
        tempMap.keySet().removeAll(set);

        Iterator<Integer> it = tempMap.keySet().iterator();
        while (it.hasNext()) {
            Integer key = it.next();
            driverMarkers.remove(key);
            Marker marker = tempMap.get(key);
            if (marker != null) {
                marker.remove();
            }
            it.remove();
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onRequestTaxiError(ServiceRequestErrorEvent event) {
        if (driverAcceptedDialog != null)
            driverAcceptedDialog.dismiss();
        event.showAlert(MainActivity.this);
    }

    @Subscribe(threadMode = MAIN, sticky = true)
    public void onProfileChanged(ProfileInfoChangedEvent event) {
        fillInfo();
    }


    @SuppressLint("StaticFieldLeak")
    private class GetMarkerAddress extends AsyncTask<Double, Void, String> {
        @Override
        protected String doInBackground(Double... floats) {
            Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(floats[0], floats[1], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {

                return addresses.get(0).getAddressLine(0);
            } else
                return getString(R.string.unknown_location);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (isPickupSelect) {
                binding.searchText.setText(s);
            } else {
                binding.destination.setText(s);
            }

            if (markerMode == MarkerMode.origin) {
                travel.setPickupPoint(mMap.getCameraPosition().target);
                travel.setPickupAddress(s);
                //binding.buttonConfirmPickup.setEnabled(false);
            } else {
                travel.setDestinationAddress(s);
                binding.buttonConfirmDestination.setEnabled(true);
                onButtonConfirmDestinationClicked();
            }
        }
    }

    @Subscribe(threadMode = MAIN)
    public void OnGetStatusResultReceived(GetStatusResultEvent event) {
        Log.e("TAG", "OnGetStatusResultReceived :  " + event.status);
        hideProgress();
        isResponseReceived = true;
        if (event.status == 200) {
            moveToTravelActivity(event.getTravel().toJson());
            //startActivityForResult(intent, ACTIVITY_TRAVEL);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ACTIVITY_PROFILE):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.info_edit_profile_success));
                fillInfo();
                break;

            case (ACTIVITY_WALLET):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.account_charge_success));
                break;
            case SEARCH_LOCATION_REQEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null && data.getParcelableExtra("USER_SELECTED_LOCATION") != null) {
                        UserLocation mSelectedLocation = data.getParcelableExtra("USER_SELECTED_LOCATION");
                        moveToMap(mSelectedLocation);
                    }
                } else {
                    getAllDriverLocationTask();
                }
                break;
            case (ACTIVITY_PLACES):
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    //Place place = Autocomplete.getPlaceFromIntent(data);
                    if (isPickupSelect) {
                        mMap.clear();
                        binding.searchText.setText(place.getName());
                        travel.setPickupAddress(place.getAddress().toString());
                        travel.setPickupPoint(place.getLatLng());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 16.0f));
                    } else {
                        cancelTimer();
                        binding.destination.setText(place.getName());
                        travel.setDestinationAddress(place.getAddress().toString());
                        travel.setDestinationPoint(place.getLatLng());
                        binding.buttonConfirmDestination.setEnabled(true);
                        onButtonConfirmDestinationClicked();
                    }
                } else {
                    if (isPickupSelect) {
                        getAllDriverLocationTask();
                    }
                }
                break;

            case (ACTIVITY_VOICE_RECOGNITION):
                if (resultCode == RESULT_OK) {
                    List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (results.size() > 0)
                        findPlace(results.get(0));
                    else
                        AlerterHelper.showWarning(this, getString(R.string.warning_voice_recognizer_failed));
                }
                break;

            case (ACTIVITY_TRAVEL):
                goBackFromServiceSelection();
                break;

        }
    }

    private void moveToMap(UserLocation pSelectedLocation) {
        binding.destination.setText(pSelectedLocation.getLocality());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(pSelectedLocation.getLatitiude()), Double.parseDouble(pSelectedLocation.getLongitude())), 16));
        binding.destination.setText(pSelectedLocation.getAddress());
        travel.setDestinationPoint(new LatLng(Double.parseDouble(pSelectedLocation.getLatitiude()), Double.parseDouble(pSelectedLocation.getLongitude())));
        travel.setDestinationAddress(pSelectedLocation.getAddress());
        binding.buttonConfirmDestination.setEnabled(true);
        onButtonConfirmDestinationClicked();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAcceptDriver(AcceptDriverUIEvent event) {
        driverAcceptedDialog.dismiss();
        Log.e("TAG", "riderAccepted:  0");
        //eventBus.post(new AcceptDriverEvent(event.driverInfo.driver.getId()));
        moveToTravelActivity(event.driverInfo.travel.toJson());
    }

    private void moveToTravelActivity(String travel) {
        cancelTimer();
        Intent intent = new Intent(MainActivity.this, TravelActivity.class);
        intent.putExtra("travel", travel);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }


}
