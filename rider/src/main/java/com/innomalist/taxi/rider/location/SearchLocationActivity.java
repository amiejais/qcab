package com.innomalist.taxi.rider.location;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.databinding.ActivitySearchLocationBinding;
import com.innomalist.taxi.rider.events.GetSearchLocationLatLngEvent;
import com.innomalist.taxi.rider.events.GoogleLocationDataBean;
import com.innomalist.taxi.rider.events.UserLocation;
import com.innomalist.taxi.rider.location.adapter.LocationAutoSuggestAdapter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class SearchLocationActivity extends BaseActivity implements LocationAutoSuggestAdapter.ItemSelectionListener {

    private static final String TAG = SearchLocationActivity.class.getName();
    private ArrayList<UserLocation> mGooglePlaceSearchResults = new ArrayList<>();
    private ActivitySearchLocationBinding mBinding;
    private UserLocation mSelectedLocation;
   // private PlacesClient placesClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_location);
//        Places.initialize(getApplicationContext(), MAP_API_KEY);
//        placesClient = Places.createClient(this);

        initializeToolbar("Location Search", true);

        String initialQuery = getIntent().getStringExtra("InitialQuery");
        if (!TextUtils.isEmpty(initialQuery)) {
            mBinding.mLocationAutoSuggest.setText(initialQuery);
            mBinding.mLocationAutoSuggest.setSelection(initialQuery.length());
            GetLocationPlaces task = new GetLocationPlaces();
            task.execute(initialQuery.trim(), getIntent().getStringExtra("PickupLatLng"));
        }
        setAdapter();
        setListener();
    }

    private void setAdapter() {
        LocationAutoSuggestAdapter mLocationAutoSuggestAdapter = new LocationAutoSuggestAdapter(this, mGooglePlaceSearchResults);
        mBinding.autoSuggestRecyclerView.setAdapter(mLocationAutoSuggestAdapter);
        mBinding.autoSuggestRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.autoSuggestRecyclerView.setHasFixedSize(true);
    }

    private void setListener() {
        mBinding.mLocationAutoSuggest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (text.length() >= 3 && !(text.length() > 20)) {
                    GetLocationPlaces task = new GetLocationPlaces();
                    task.execute(text.trim(), getIntent().getStringExtra("PickupLatLng"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onItemSelected(int position) {
        mSelectedLocation = new UserLocation();
        mSelectedLocation.setLocality(mGooglePlaceSearchResults.get(position).getLocality());
        mSelectedLocation.setPlaceId(mGooglePlaceSearchResults.get(position).getPlaceId());
        mBinding.mLocationAutoSuggest.setText(mGooglePlaceSearchResults.get(position).getLocality());
        mBinding.mLocationAutoSuggest.setSelection(mBinding.mLocationAutoSuggest.length());
        //fetchLatlng(mGooglePlaceSearchResults.get(position).getPlaceId());
        eventBus.post(new GetSearchLocationLatLngEvent(mGooglePlaceSearchResults.get(position).getPlaceId()));
    }


   /* private void fetchLatlng(String placeId) {

// Specify the fields to return (in this example all fields are returned).
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG);

// Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();

        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();

            Log.i(TAG, "Place found: " + place.getName());
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                // Handle error with given status code.
                Log.e(TAG, "Place not found: " + exception.getMessage());
            }
        });
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveSearchResult(GoogleLocationDataBean googleLocationDataBean) {
        if (googleLocationDataBean.status == 200) {
            UserLocation location = googleLocationDataBean.getLatLong(true);
            mSelectedLocation.setLatitiude(location.getLatitiude());
            mSelectedLocation.setLongitude(location.getLongitude());
            sentLocationBack(location);
        } else {

        }
    }

    private void sentLocationBack(UserLocation location) {
        Intent intent = new Intent();
        intent.putExtra("USER_SELECTED_LOCATION", (Parcelable) location);
        setResult(RESULT_OK, intent);
        finish();
    }


    private class GetLocationPlaces extends AsyncTask<String, Void, ArrayList<UserLocation>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<UserLocation> doInBackground(String... params) {
            return LocationUtil.getAutoSuggestionLocations(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(ArrayList<UserLocation> result) {
            mGooglePlaceSearchResults.clear();
            if (result != null && result.size() > 0) {
                mGooglePlaceSearchResults.addAll(result);
            }
            mBinding.autoSuggestRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }
}
