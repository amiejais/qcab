package com.innomalist.taxi.rider.location.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.events.UserLocation;

import java.util.ArrayList;

public class LocationAutoSuggestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<UserLocation> mLocationAutoSuggestList;
    private ItemSelectionListener mListener;

    public LocationAutoSuggestAdapter(ItemSelectionListener listener, ArrayList<UserLocation> locationAutoSuggestList) {
        mLocationAutoSuggestList = locationAutoSuggestList;
        mListener = listener;
    }

    public static class LocationAutoSuggestViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mLocationAutoSuggestListLayout;
        TextView mAutoSuggestLocation;

        public LocationAutoSuggestViewHolder(View itemView) {
            super(itemView);
            mAutoSuggestLocation = (TextView) itemView.findViewById(R.id.recent_locations);
            mLocationAutoSuggestListLayout = (LinearLayout) itemView.findViewById(R.id.mLocationAutoSuggestListLayout);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_itemview, parent, false);

        return new LocationAutoSuggestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        try {
            final LocationAutoSuggestViewHolder locationAutoSuggestViewHolder = (LocationAutoSuggestViewHolder) holder;
            locationAutoSuggestViewHolder.mAutoSuggestLocation.setText(mLocationAutoSuggestList.get(position).getLocality());

            locationAutoSuggestViewHolder.mLocationAutoSuggestListLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mListener.onItemSelected(position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return (null != mLocationAutoSuggestList ? mLocationAutoSuggestList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public interface ItemSelectionListener {
        void onItemSelected(int position);
    }

}