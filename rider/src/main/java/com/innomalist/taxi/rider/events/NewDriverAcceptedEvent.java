package com.innomalist.taxi.rider.events;

import com.google.gson.Gson;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.models.DriverInfo;
import com.innomalist.taxi.common.models.Travel;

public class NewDriverAcceptedEvent {
    public DriverInfo info;

    public NewDriverAcceptedEvent(String json, int distance, int duration, String travel) {
        this.info = new DriverInfo(new Gson().fromJson(json, Driver.class), distance, duration, new Gson().fromJson(travel, Travel.class));
    }
}
