package com.innomalist.taxi.rider.activities.main.dialogs;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.core.content.ContextCompat;

import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.activities.main.MainActivity;
import com.innomalist.taxi.rider.databinding.DialogRequestServiceBinding;
import com.innomalist.taxi.rider.events.AcceptDriverUIEvent;
import com.innomalist.taxi.rider.events.NewDriverAcceptedEvent;
import com.innomalist.taxi.rider.events.ServiceRequestEvent;
import com.innomalist.taxi.rider.events.ServiceRequestResultEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Timer;
import java.util.TimerTask;

public class DriverAcceptedDialog extends AppCompatDialogFragment {
    private static final String ARG_TRAVEL = "travel";
    //DriverAcceptedCardAdapter driversAdapter;
    DialogRequestServiceBinding binding;
    Travel travel;
    EventBus eventBus;
    private static final int MAX_LIMIT = 70000;
    private CountDownTimer countDownTimer;

    public static DriverAcceptedDialog newInstance(Travel travel) {
        DriverAcceptedDialog driverAcceptedDialog = new DriverAcceptedDialog();
        Bundle args = new Bundle();
        args.putString(ARG_TRAVEL, travel.toJson());
        driverAcceptedDialog.setArguments(args);
        return driverAcceptedDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            travel = Travel.fromJson(getArguments().getString(ARG_TRAVEL));
        else {
            AlertDialogBuilder.show(getContext(), "Can't show driver's dialog without having travel info passed into.");
            dismiss();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DialogRequestServiceBinding.inflate(inflater, container, false);
        binding.frameLayout.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
      /*  driversAdapter = new DriverAcceptedCardAdapter(getContext());
        binding.swipeStack.setAdapter(driversAdapter);*/
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        eventBus = EventBus.getDefault();
        eventBus.register(this);
        eventBus.post(new ServiceRequestEvent(travel.getPickupPoint(), travel.getDestinationPoint(), travel.getPickupAddress(), travel.getDestinationAddress(),Integer.parseInt(travel.getServiceId())));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceRequestResult(ServiceRequestResultEvent event) {
        if (event.hasError()) {
            event.showAlert(getContext());
            dismiss();
            return;
        } else if (event.driversSentTo == 0) {
            event.showAlert(getContext(), "No nearby cab's found");
            dismiss();
            return;
        }
        driverWaitingTimer(event);
        // binding.textLoading.setText(getString(R.string.waiting_for_drivers, String.valueOf(event.driversSentTo)));
    }

    private void driverWaitingTimer(ServiceRequestResultEvent event) {
        binding.searchLimitBar.setMax(70);
        countDownTimer = new CountDownTimer(MAX_LIMIT, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int progress = (int) ((MAX_LIMIT - millisUntilFinished) / 1000);
                Log.e("TAG", "Time: " + millisUntilFinished + " &progress: " + progress);
                setProgressAnimate(binding.searchLimitBar, progress);
            }

            @Override
            public void onFinish() {

                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        event.showAlert(getActivity(), "No nearby cab's found");
                    });
                }
                dismiss();
            }

        };
        countDownTimer.start();
    }

    private void cancelTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    private void setProgressAnimate(ProgressBar pb, int progressTo) {
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", pb.getProgress(), progressTo);
        animation.setDuration(300);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDriverAccepted(NewDriverAcceptedEvent event) {
        cancelTimer();
        binding.driversAcceptedLoadingAnimation.pauseAnimation();
        binding.driversAcceptedCard.setVisibility(View.GONE);
        try {
            //driversAdapter.addDriver(event.info);
            //driversAdapter.notifyDataSetChanged();
            EventBus.getDefault().post(new AcceptDriverUIEvent(event.info));
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onStop() {
        eventBus.unregister(this);
        super.onStop();
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        cancelTimer();
        super.onCancel(dialog);
        dialog.dismiss();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        cancelTimer();

        super.onDismiss(dialog);
        dialog.dismiss();
    }
}
