package com.innomalist.taxi.rider.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;
import androidx.appcompat.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.innomalist.taxi.rider.activities.splash.SplashActivity;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

public class MyTaxiApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(getApplicationContext());
        int nightMode = AppCompatDelegate.MODE_NIGHT_NO;
        AppCompatDelegate.setDefaultNightMode(nightMode);
        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new DriverNotificationHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        /*LocaleHelper.setLocale(base,"en");*/
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private class DriverNotificationHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            Intent resultIntent;
            Log.e("TAG","Event : "+ notification.payload.title);
           /* if (!notification.isAppInFocus) {
                resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                resultIntent.putExtra("IsFromNote", true);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(resultIntent);
            }else{
                *//*Intent intent=new Intent();
                intent.putExtra("Type",notification.payload.title);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);*//*
            }*/
        }
    }

}