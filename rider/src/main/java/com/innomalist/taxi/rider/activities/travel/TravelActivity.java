package com.innomalist.taxi.rider.activities.travel;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.tabs.TabLayout;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.activities.chargeAccount.QCabPaymentActivity;
import com.innomalist.taxi.common.activities.travels.fragments.ReviewDialog;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.events.RideDriverLocationEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestResultEvent;
import com.innomalist.taxi.common.events.ServiceCancelEvent;
import com.innomalist.taxi.common.events.ServiceCancelResultEvent;
import com.innomalist.taxi.common.location.MapHelper;
import com.innomalist.taxi.common.models.Coupon;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.DirectionsJSONParser;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.LocationHelper;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.activities.coupon.CouponActivity;
import com.innomalist.taxi.rider.activities.main.MainActivity;
import com.innomalist.taxi.rider.activities.travel.adapters.TravelTabsViewPagerAdapter;
import com.innomalist.taxi.rider.activities.travel.fragments.TabStatisticsFragment;
import com.innomalist.taxi.rider.databinding.ActivityTravelBinding;
import com.innomalist.taxi.rider.events.GetCurrentDriverLocation;
import com.innomalist.taxi.rider.events.ReviewDriverResultEvent;
import com.innomalist.taxi.rider.events.ServiceChangeRequestEvent;
import com.innomalist.taxi.rider.events.ServiceFinishedEvent;
import com.innomalist.taxi.rider.events.ServiceRequestErrorEvent;
import com.innomalist.taxi.rider.events.ServiceRequestResultEvent;
import com.innomalist.taxi.rider.events.ServiceStartedEvent;
import com.transitionseverywhere.TransitionManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.innomalist.taxi.common.utils.CommonUtils.deleteCountry;

public class TravelActivity extends BaseActivity implements OnMapReadyCallback, ReviewDialog.onReviewFragmentInteractionListener, TabStatisticsFragment.onTravelInfoReceived {
    private static final int ACTIVITY_COUPON = 700;
    private static final int ACTIVITY_PLACES = 701;
    private static final int ACTIVITY_VOICE_RECOGNITION = 702;
    public static final String NOTE_ACTION = "com.qcab.rider_Note_Action";
    ActivityTravelBinding binding;
    Travel travel;
    Marker pickupMarker;
    Marker driverMarker;
    Marker destinationMarker;
    LatLng driverLocation;
    GoogleMap gMap;
    TravelTabsViewPagerAdapter travelTabsViewPagerAdapter;
    DirectionsJSONParser directionToPassengerRouter;
    private TimerTask timerTask;
    private Handler handler;
    private MediaPlayer mMediaPlayer;
    private NoteUpdateReceiver mNoteUpdateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_travel);
        travel = Travel.fromJson(getIntent().getStringExtra("travel"));
        binding.slideCancel.setOnSlideCompleteListener(slideView -> cancelTravelAlert());
        binding.slideCall.setOnSlideCompleteListener(slideView -> onCallDriverClicked(null));
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        travelTabsViewPagerAdapter = new TravelTabsViewPagerAdapter(getSupportFragmentManager(), TravelActivity.this, travel);
        binding.viewpager.setAdapter(travelTabsViewPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewpager);
        binding.destination.setText(travel.getDestinationAddress());
        handler = new Handler();
        if (travel.getRating() != null) {
            travelTabsViewPagerAdapter.deletePage(2);
            TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            if (tab != null)
                tab.select();
        }
        if (travel.getStartTimestamp() != null) {
            binding.slideCall.setVisibility(View.GONE);
            binding.slideCancel.setVisibility(View.GONE);
        } else {
            playSound();
        }
        binding.destination.setOnClickListener(view -> {
            findPlace("");
        });

        binding.destination.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (binding.destination.getRight() - binding.destination.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    Log.e("TAg", "setOnTouchListener");
                    displaySpeechRecognizer();
                    return true;
                }
            }
            return false;
        });

        Log.e("TAG", "travel:ServceOId : " + travel.getServiceId());
        binding.sos.setOnClickListener(view -> startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "100", null))));
    }


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter noteIntentFilter = new IntentFilter();
        noteIntentFilter.addAction(NOTE_ACTION);
        noteIntentFilter.setPriority(1);
        mNoteUpdateReceiver = new NoteUpdateReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mNoteUpdateReceiver, noteIntentFilter);
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mNoteUpdateReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mNoteUpdateReceiver);
        }
    }

    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.RECORD_AUDIO)
                .check();
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            try {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, getString(R.string.default_language));

                startActivityForResult(intent, ACTIVITY_VOICE_RECOGNITION);
            } catch (ActivityNotFoundException e) {
                AlertDialogBuilder.show(TravelActivity.this, getString(R.string.question_install_speech), getString(R.string.error), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                    if (result == AlertDialogBuilder.DialogResult.OK) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://market.android.com/details?id=com.google.android.voicesearch"));
                        startActivity(browserIntent);
                    }
                });
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceFinished(ServiceFinishedEvent event) {
        String message;
        travel.setFinishTimestamp(new Timestamp(System.currentTimeMillis()));
        if (event.isCreditUsed)
            message = getString(R.string.travel_finished_taken_from_balance, event.amount);
        else
            message = getString(R.string.travel_finished_not_sufficient_balance, event.amount);
        new MaterialDialog.Builder(this)
                .title(R.string.message_default_title)
                .content(message)
                .positiveText(R.string.alert_ok)
                .onPositive((dialog, which) -> {
                    if (travelTabsViewPagerAdapter.getCount() == 2) {
                        finish();
                        return;
                    }
                    FragmentManager fm = getSupportFragmentManager();
                    ReviewDialog reviewDialog = ReviewDialog.newInstance();
                    reviewDialog.show(fm, "fragment_review_travel");
                }).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceCanceled(ServiceCancelResultEvent event) {
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    eventBus.post(new ServiceCancelEvent());
            });
            return;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReviewResult(ReviewDriverResultEvent event) {
        if (event.status == 200) {
            if (travel.getFinishTimestamp() != null) {
                startMainActivity(driverLocation);
                return;
            }
            AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_review_sent));
            travelTabsViewPagerAdapter.deletePage(2);
            TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            if (tab != null)
                tab.select();

        } else
            event.showAlert(TravelActivity.this);
    }

    public void onChargeAccountClicked(View view) {
        Intent intent = new Intent(TravelActivity.this, QCabPaymentActivity.class);
        if (travel.getCostBest() - CommonUtils.rider.getBalance() > 0)
            intent.putExtra("defaultAmount", travel.getCostBest() - CommonUtils.rider.getBalance());
        startActivity(intent);
    }

    public void onApplyCouponClicked(View view) {
        Intent intent = new Intent(TravelActivity.this, CouponActivity.class);
        intent.putExtra("select_mode", true);
        startActivityForResult(intent, ACTIVITY_COUPON);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTravelStarted(ServiceStartedEvent event) {
        travel.setStartTimestamp(new Timestamp(System.currentTimeMillis()));
        AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_travel_started));

        initializeTimerTask();
        TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
        binding.slideCall.setVisibility(View.GONE);
        binding.slideCancel.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        gMap = googleMap;
        gMap.getUiSettings().setMyLocationButtonEnabled(false);
        gMap.getUiSettings().setCompassEnabled(false);
        gMap.getUiSettings().setMapToolbarEnabled(false);
        gMap.setTrafficEnabled(false);
        gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_map_style));

        googleMap.setOnMapLoadedCallback(this::initializeTimerTask);
        eventBus.post(new GetCurrentDriverLocation());
    }


    private void showRout(LatLng startLatLng, LatLng endLatLng) {
        List<LatLng> locations = new ArrayList<>();
        locations.add(startLatLng);
        locations.add(endLatLng);
        MapHelper.centerLatLngsInMap(gMap, locations, true);
        if (directionToPassengerRouter != null) {
            directionToPassengerRouter.removeLine();
        }

        directionToPassengerRouter = new DirectionsJSONParser(gMap, startLatLng, endLatLng);
        directionToPassengerRouter.run();
    }

    public void initializeTimerTask() {
        updateMarkers();
        final Timer timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(() -> {
                    if (driverLocation != null) {
                        timer.cancel();
                        timerTask = null;
                        handler.removeCallbacks(this);
                        if (travel.getStartTimestamp() == null) {
                            showRout(driverLocation, travel.getPickupPoint());
                        } else {
                            showRout(driverLocation, travel.getDestinationPoint());
                        }

                    }
                });
            }
        };

        timer.schedule(timerTask, 1000, 2000);

    }

    void updateMarkers() {
        if (gMap == null) {
            return;
        }
        List<LatLng> locations = new ArrayList<>();
        if (pickupMarker == null)
            pickupMarker = gMap.addMarker(new MarkerOptions()
                    .position(travel.getPickupPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_pickup)));
        else
            pickupMarker.setPosition(travel.getPickupPoint());
        if (destinationMarker == null)
            destinationMarker = gMap.addMarker(new MarkerOptions()
                    .position(travel.getDestinationPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_destination)));
        else
            destinationMarker.setPosition(travel.getDestinationPoint());
        if (driverLocation != null) {
            locations.add(driverLocation);
            if (travel.getStartTimestamp() != null)
                locations.add(travel.getDestinationPoint());
            else
                locations.add(travel.getPickupPoint());
            if (driverMarker == null)
                driverMarker = gMap.addMarker(new MarkerOptions()
                        .position(driverLocation)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));
        } else {
            locations.add(travel.getPickupPoint());
            locations.add(travel.getDestinationPoint());
        }

        MapHelper.centerLatLngsInMap(gMap, locations, true);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCallRequested(ServiceCallRequestResultEvent event) {
        LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    onCallDriverClicked(null);
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.call_request_sent));
    }

    @Override
    public void onReviewTravelClicked(ReviewDriverEvent reviewDriverEvent) {
        eventBus.post(reviewDriverEvent);

    }

    public void onCallDriverClicked(View view) {
        if (travel.getDriver() == null || travel.getDriver().getMobileNumber() == 0L) {
            Toast.makeText(this, "Driver number not available. Please try restarting your app.", Toast.LENGTH_SHORT).show();
            return;
        }
        String formatNumber = deleteCountry("" + travel.getDriver().getMobileNumber());
        formatNumber = TextUtils.isEmpty(formatNumber) ? "" + travel.getDriver().getMobileNumber() : formatNumber;
        formatNumber = "0" + formatNumber;
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", formatNumber, null));
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ACTIVITY_PLACES):
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    //Place place = Autocomplete.getPlaceFromIntent(data);

                    travel.setDestinationAddress(place.getName().toString());
                    travel.setDestinationPoint(place.getLatLng());
                    updateNewDestination();
                }
                break;

            case (ACTIVITY_VOICE_RECOGNITION):
                if (resultCode == RESULT_OK) {
                    List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (results.size() > 0)
                        findPlace(results.get(0));
                    else
                        AlerterHelper.showWarning(this, getString(R.string.warning_voice_recognizer_failed));
                }
                break;

            case ACTIVITY_COUPON:
                if (resultCode == RESULT_OK) {
                    Coupon coupon = (Coupon) data.getSerializableExtra("coupon");
                    String message = "";
                    if (coupon.getFlatDiscount() == 0 && coupon.getDiscountPercent() != 0)
                        message = "Coupon with discount of " + coupon.getDiscountPercent() + "% has been applied.";
                    if (coupon.getFlatDiscount() != 0 && coupon.getDiscountPercent() == 0)
                        message = "Coupon with discount of " + getString(R.string.unit_money, coupon.getFlatDiscount()) + " has been applied.";
                    if (coupon.getFlatDiscount() != 0 && coupon.getDiscountPercent() != 0)
                        message = "Coupon with discount of " + getString(R.string.unit_money, coupon.getFlatDiscount()) + " and " + coupon.getDiscountPercent() + "% has been applied.";
                    if (message.equals(""))
                        return;
                    AlerterHelper.showInfo(TravelActivity.this, message);
                    travelTabsViewPagerAdapter.statisticsFragment.onUpdatePrice(data.getDoubleExtra("costAfterCoupon", travel.getCostBest()));
                }
        }
    }


    private void updateNewDestination() {
        eventBus.post(new ServiceChangeRequestEvent(travel.getPickupPoint(), travel.getDestinationPoint(), travel.getPickupAddress(), travel.getDestinationAddress(), travel.getId()));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceRequestResult(ServiceRequestResultEvent event) {
        if (event.hasError()) {
            event.showAlert(getApplicationContext());
            return;
        }
        binding.destination.setText(travel.getDestinationAddress());
        initializeTimerTask();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceRequestErrorResult(ServiceRequestErrorEvent event) {
        if (event.hasError()) {
            event.showAlert(getApplicationContext());
            return;
        }
        showAlert("There is problem updating the rout. Please ask your driver to drop to you new destination.");
    }

    public void findPlace(String initialQuery) {
        try {
            AutocompleteFilter autocompleteFilter = (new AutocompleteFilter.Builder()).setCountry("IN").build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(autocompleteFilter)
                            .build(this);
            if (!TextUtils.isEmpty(initialQuery)) {
                intent.putExtra("initial_query", initialQuery);
            }
            startActivityForResult(intent, ACTIVITY_PLACES);

           /* if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), AUTOCOMPLETE_API_KEY);
            }

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.NAME, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields).setInitialQuery(initialQuery).setCountry("IN").setLocationRestriction(toBounds(new LatLng(travel.getPickupPoint().latitude, travel.getPickupPoint().longitude), 40000))
                    .build(this);

            startActivityForResult(intent, ACTIVITY_PLACES);*/

        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
    }

   /* public RectangularBounds toBounds(LatLng center, double radiusInMeters) {

        double distanceFromCenterToCorner = radiusInMeters * Math.sqrt(2.0);
        LatLng southwestCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 225.0);
        LatLng northeastCorner =
                SphericalUtil.computeOffset(center, distanceFromCenterToCorner, 45.0);

        return RectangularBounds.newInstance(southwestCorner, northeastCorner);
    }*/


    @Override
    public void onReceived(LatLng driverLocation, float cost) {

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetCurrentDriverLocation(RideDriverLocationEvent rideDriverLocationEvent) {
        if (gMap == null) {
            return;
        }
        if (rideDriverLocationEvent != null) {
            driverLocation = new LatLng(rideDriverLocationEvent.lat, rideDriverLocationEvent.lng);

            if (driverMarker == null) {
                driverMarker = gMap.addMarker(new MarkerOptions()
                        .position(driverLocation)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));
            } else {
                MapHelper.animateMarkerToLocation(driverLocation, driverMarker);
                // MapHelper.moveMap(gMap, driverLocation);
            }

            List<LatLng> topLocation = new ArrayList<>();
            topLocation.add(driverLocation);
            if (travel.getStartTimestamp() != null)
                topLocation.add(travel.getDestinationPoint());
            else
                topLocation.add(travel.getPickupPoint());
            MapHelper.centerLatLngsInMap(gMap, topLocation, true);

        }
    }


    private void cancelTravelAlert() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.app_name))
                .content(getString(R.string.cancellation_msg))
                .positiveText(getString(R.string.alert_yes))
                .onPositive((dialog, which) -> {
                    eventBus.post(new ServiceCancelEvent());
                })
                .negativeText(getString(R.string.alert_no))
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCancelTravel(CommonResponse event) {
        if (event.getRequestTag().equalsIgnoreCase("cancelTravel")) {
            AlertDialogBuilder.show(TravelActivity.this, getString(R.string.service_canceled), AlertDialogBuilder.DialogButton.OK, result -> startMainActivity(driverLocation));
        } else {

        }
       /* travel.setStartTimestamp(new Timestamp(System.currentTimeMillis()));
        AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_travel_started));

        initializeTimerTask();
        TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
        binding.slideCall.setVisibility(View.GONE);
        binding.slideCancel.setVisibility(View.GONE);*/
    }

    private void playSound() {
        try {
            stopSound();
            mMediaPlayer = new MediaPlayer();
            AssetFileDescriptor descriptor = getAssets().openFd("rider_sound.mp3");
            mMediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mMediaPlayer.prepare();
            mMediaPlayer.setLooping(false);
            mMediaPlayer.start();
            mMediaPlayer.setOnCompletionListener(mediaPlayer -> stopSound());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopSound() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void startMainActivity(LatLng latLng) {

        Intent intent = new Intent(this, MainActivity.class);
        if (latLng != null) {
            double[] array = LocationHelper.LatLngToDoubleArray(latLng);
            intent.putExtra("currentLocation", array);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    public class NoteUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra("Type").equalsIgnoreCase("cancelTravel")) {
                startMainActivity(driverLocation);
            } else if (intent.getStringExtra("Type").equalsIgnoreCase("startTravel")) {
                onTravelStarted(null);
            } else if (intent.getStringExtra("Type").equalsIgnoreCase("finishedTaxi")) {
                onServiceFinished(null);
            }
        }
    }
}
