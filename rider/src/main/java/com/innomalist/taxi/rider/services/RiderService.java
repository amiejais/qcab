package com.innomalist.taxi.rider.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.innomalist.taxi.common.events.BackgroundServiceStartedEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageResultEvent;
import com.innomalist.taxi.common.events.ChargeAccountEvent;
import com.innomalist.taxi.common.events.ChargeAccountResultEvent;
import com.innomalist.taxi.common.events.ConnectEvent;
import com.innomalist.taxi.common.events.ConnectResultEvent;
import com.innomalist.taxi.common.events.EditProfileInfoEvent;
import com.innomalist.taxi.common.events.EditProfileInfoResultEvent;
import com.innomalist.taxi.common.events.GetAppVersionResultEvent;
import com.innomalist.taxi.common.events.GetStatusEvent;
import com.innomalist.taxi.common.events.GetStatusResultEvent;
import com.innomalist.taxi.common.events.GetTransactionsRequestEvent;
import com.innomalist.taxi.common.events.GetTransactionsResultEvent;
import com.innomalist.taxi.common.events.GetTravelsEvent;
import com.innomalist.taxi.common.events.GetTravelsResultEvent;
import com.innomalist.taxi.common.events.HideTravelEvent;
import com.innomalist.taxi.common.events.HideTravelResultEvent;
import com.innomalist.taxi.common.events.LoginEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.events.ProfileInfoChangedEvent;
import com.innomalist.taxi.common.events.RideDriverLocationEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestResultEvent;
import com.innomalist.taxi.common.events.ServiceCancelEvent;
import com.innomalist.taxi.common.events.ServiceCancelResultEvent;
import com.innomalist.taxi.common.events.SocketConnectionEvent;
import com.innomalist.taxi.common.events.WriteComplaintEvent;
import com.innomalist.taxi.common.events.WriteComplaintResultEvent;
import com.innomalist.taxi.common.models.AppVersion;
import com.innomalist.taxi.common.models.RegisterEvent;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.models.RiderResultEvent;
import com.innomalist.taxi.common.network.ApiCallback;
import com.innomalist.taxi.common.network.RetrofitClient;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.common.utils.ServerResponse;
import com.innomalist.taxi.driver.events.GetCityEvent;
import com.innomalist.taxi.driver.models.CityList;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.events.AcceptDriverEvent;
import com.innomalist.taxi.rider.events.AddCouponRequestEvent;
import com.innomalist.taxi.rider.events.AddCouponResultEvent;
import com.innomalist.taxi.rider.events.ApplyCouponRequestEvent;
import com.innomalist.taxi.rider.events.ApplyCouponResultEvent;
import com.innomalist.taxi.rider.events.CRUDAddressRequestEvent;
import com.innomalist.taxi.rider.events.CRUDAddressResultEvent;
import com.innomalist.taxi.rider.events.CalculateFareRequestEvent;
import com.innomalist.taxi.rider.events.CalculateFareResultEvent;
import com.innomalist.taxi.rider.events.GetCouponsRequestEvent;
import com.innomalist.taxi.rider.events.GetCouponsResultEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationResultEvent;
import com.innomalist.taxi.rider.events.GetPromotionsRequestEvent;
import com.innomalist.taxi.rider.events.GetPromotionsResultEvent;
import com.innomalist.taxi.rider.events.GetSearchLocationLatLngEvent;
import com.innomalist.taxi.rider.events.GetTravelInfoEvent;
import com.innomalist.taxi.rider.events.GetTravelInfoResultEvent;
import com.innomalist.taxi.rider.events.GoogleLocationDataBean;
import com.innomalist.taxi.rider.events.NewDriverAcceptedEvent;
import com.innomalist.taxi.rider.events.ReviewDriverResultEvent;
import com.innomalist.taxi.rider.events.ServiceChangeRequestEvent;
import com.innomalist.taxi.rider.events.ServiceFinishedEvent;
import com.innomalist.taxi.rider.events.ServiceRequestErrorEvent;
import com.innomalist.taxi.rider.events.ServiceRequestEvent;
import com.innomalist.taxi.rider.events.ServiceRequestResultEvent;
import com.innomalist.taxi.rider.events.ServiceStartedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;

import static com.innomalist.taxi.common.utils.Config.MAP_API_KEY;

public class RiderService extends Service {
    private MyPreferenceManager mPref;
    private Socket socket;
    EventBus eventBus = EventBus.getDefault();

    @Subscribe
    public void connectSocket(ConnectEvent connectEvent) {
        try {
            IO.Options options = new IO.Options();
            options.query = "token=" + connectEvent.token;
            options.reconnection = true;
            socket = IO.socket(getString(R.string.server_address), options);
            socket.on(Socket.EVENT_CONNECT, args -> {
                eventBus.post(new ConnectResultEvent(ServerResponse.OK.getValue()));
                eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT));
            })
                    .on(Socket.EVENT_DISCONNECT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_DISCONNECT)))
                    .on(Socket.EVENT_CONNECTING, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECTING)))
                    .on(Socket.EVENT_CONNECT_ERROR, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT_ERROR)))
                    .on(Socket.EVENT_CONNECT_TIMEOUT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT_TIMEOUT)))
                    .on(Socket.EVENT_RECONNECT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT)))
                    .on(Socket.EVENT_RECONNECTING, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECTING)))
                    .on(Socket.EVENT_RECONNECT_ATTEMPT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_ATTEMPT)))
                    .on(Socket.EVENT_RECONNECT_ERROR, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_ERROR)))
                    .on(Socket.EVENT_RECONNECT_FAILED, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_FAILED)))
                    .on("error", args -> {
                        try {
                            JSONObject obj = new JSONObject(args[0].toString());
                            eventBus.post(new ConnectResultEvent(ServerResponse.UNKNOWN_ERROR.getValue(), obj.getString("message")));
                        } catch (JSONException c) {
                            eventBus.post(new ConnectResultEvent(ServerResponse.UNKNOWN_ERROR.getValue(), args[0].toString()));
                        }
                    }).on("driverInLocation", args -> {
                try {
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(RiderService.this)
                                    .setContentTitle(getString(R.string.app_name))
                                    .setDefaults(NotificationCompat.DEFAULT_LIGHTS | NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE)
                                    .setContentText(getString(R.string.notification_driver_in_location));
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    if (notificationManager != null) {
                        notificationManager.notify(0, mBuilder.build());
                    }
                } catch (Exception c) {
                    c.printStackTrace();
                }
            }).on("startTravel", args -> eventBus.post(new ServiceCancelEvent())).on("cancelTravel", args -> eventBus.post(new ServiceCancelResultEvent(200)))
                    .on("driverAccepted", args -> eventBus.post(new NewDriverAcceptedEvent(args[0].toString(), (Integer) args[1], (Integer) args[2], args[3].toString())))
                    .on("finishedTaxi", args -> eventBus.post(new ServiceFinishedEvent((Integer) args[0], (Boolean) args[1], Float.parseFloat(args[2].toString()))))
                    .on("riderInfoChanged", args -> {
                        MyPreferenceManager SP = new MyPreferenceManager(getApplicationContext());
                        SP.putString("rider_user", args[0].toString());
                        CommonUtils.rider = Rider.fromJson(args[0].toString());
                        eventBus.postSticky(new ProfileInfoChangedEvent());
                    })
                    .on("travelInfoReceived", args -> {
                        eventBus.post(new GetTravelInfoResultEvent((int) args[0], (int) args[1], Float.parseFloat(args[2].toString()), Float.valueOf(args[3].toString()), Float.valueOf(args[4].toString())));
                    }).on("driverLocation", args -> {
                try {
                    Log.e("TAG,", "driverLocation called");
                    eventBus.post(new RideDriverLocationEvent(Double.valueOf(args[0].toString()), Double.valueOf(args[1].toString())));
                } catch (Exception c) {
                    c.printStackTrace();
                }
            });
            socket.connect();

        } catch (Exception ignored) {
        }
    }

    @Subscribe
    public void register(RegisterEvent event) {
        ApiCallback<RiderResultEvent> callback = new ApiCallback<>("RiderRegister");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).registerRider(event).enqueue(callback);
    }

    @Subscribe
    public void login(LoginEvent event) {
        ApiCallback<RiderResultEvent> callback = new ApiCallback<>("RiderLogin");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).loginRider(event).enqueue(callback);
        //new LoginRequest().execute(String.valueOf(event.mobile), String.valueOf(event.version));
    }

    @Subscribe
    public void getVersionStatus(AppVersion event) {
        Log.e("TAg", "getVersionStatus");
        ApiCallback<GetAppVersionResultEvent> callback = new ApiCallback<>("AppVersions");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getAppVersions().enqueue(callback);
        //socket.emit("getVersions", (Ack) args -> eventBus.post(new GetAppVersionResultEvent(args)));
    }

    @Subscribe
    public void getStatus(GetStatusEvent event) {
        ApiCallback<GetStatusResultEvent> callback = new ApiCallback<>("UnfinishedRiderTravel");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getUnfinishedTravel().enqueue(callback);
        //socket.emit("getStatus", (Ack) args -> eventBus.post(new GetStatusResultEvent(args)));
    }

    @Subscribe
    public void EditProfile(EditProfileInfoEvent editProfileInfoEvent) {
        ApiCallback<EditProfileInfoResultEvent> callback = new ApiCallback<>("editProfile");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).editProfile(editProfileInfoEvent).enqueue(callback);
        // socket.emit("editProfile", editProfileInfoEvent.userInfo, (Ack) args -> eventBus.post(new EditProfileInfoResultEvent((int) args[0])));
    }

    @Subscribe
    public void requestTaxi(ServiceRequestEvent event) {
        socket.emit("requestTaxi", event.pickupPoint, event.destinationPoint, event.pickupLocation, event.dropOffLocation, event.serviceId, (Ack) args -> {
            if ((Integer) args[0] == 200)
                eventBus.post(new ServiceRequestResultEvent((int) args[1]));
            else {
                if ((int) args[0] == 666)
                    eventBus.post(new ServiceRequestErrorEvent((int) args[0], args[1].toString()));
                else
                    eventBus.post(new ServiceRequestErrorEvent((int) args[0]));
            }
        });
    }

    @Subscribe
    public void requestChangeLocation(ServiceChangeRequestEvent event) {
        socket.emit("requestChangeLocation", event.pickupPoint, event.destinationPoint, event.pickupLocation, event.dropOffLocation, event.travelId, (Ack) args -> {
            if ((Integer) args[0] == 200)
                eventBus.post(new ServiceRequestResultEvent((int) args[1]));
            else {
                if ((int) args[0] == 666)
                    eventBus.post(new ServiceRequestErrorEvent((int) args[0], args[1].toString()));
                else
                    eventBus.post(new ServiceRequestErrorEvent((int) args[0]));
            }
        });
    }

    @Subscribe
    public void cancelTravel(ServiceCancelEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("cancelTravel");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).cancelTravel().enqueue(callback);
       // socket.emit("cancelTravel", (Ack) args -> eventBus.post(new ServiceCancelResultEvent((int) args[0])));
    }

    @Subscribe
    public void acceptDriver(AcceptDriverEvent acceptDriverEvent) {
        Log.e("TAG", "riderAccepted:  1");
        if (socket != null && socket.connected()) {
            socket.emit("riderAccepted", acceptDriverEvent.driverId);
            Log.e("TAG", "riderAccepted:  2");
        }

    }

    @Subscribe
    public void getTravels(GetTravelsEvent getTravelsEvent) {
        ApiCallback<GetTravelsResultEvent> callback = new ApiCallback<>("getTravels");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getTravels().enqueue(callback);
        // socket.emit("getTravels", (Ack) args -> eventBus.postSticky(new GetTravelsResultEvent((int) args[0], args[1].toString())));
    }

    @Subscribe
    public void getDriverLocation(final GetTravelInfoEvent event) {
        socket.emit("getTravelInfo");
    }

    @Subscribe
    public void reviewDriver(ReviewDriverEvent event) {
        ApiCallback<ReviewDriverResultEvent> callback = new ApiCallback<>("reviewDriver");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).reviewDriver(event).enqueue(callback);
        //socket.emit("reviewDriver", event.getScore(), event.getReview(), (Ack) args -> eventBus.post(new ReviewDriverResultEvent((int) args[0])));
    }

    @Subscribe
    public void ChangeProfileImage(ChangeProfileImageEvent changeProfileImageEvent) {
        File file = new File(changeProfileImageEvent.path);
        byte[] data = new byte[(int) file.length()];
        try {
            int len = new FileInputStream(file).read(data);
            if (len > 0)
                socket.emit("changeProfileImage", data, (Ack) args -> eventBus.post(new ChangeProfileImageResultEvent((int) args[0], args[1].toString())));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void getDriversLocation(final GetDriversLocationEvent event) {
        ApiCallback<GetDriversLocationResultEvent> callback = new ApiCallback<>("getDriversLocation");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getDriversLocation(event).enqueue(callback);
        //socket.emit("getDriversLocation", event.devicePoints, (Ack) args -> eventBus.post(new GetDriversLocationResultEvent((Integer) args[0], (JSONArray) args[1])));
    }

    @Subscribe
    public void WriteComplaint(final WriteComplaintEvent event) {
        ApiCallback<WriteComplaintResultEvent> callback = new ApiCallback<>("writeComplaint");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).writeComplaint(event).enqueue(callback);
        //socket.emit("writeComplaint", event.travelId, event.subject, event.content, (Ack) args -> eventBus.post(new WriteComplaintResultEvent((int) args[0])));
    }

    @Subscribe
    public void chargeAccount(ChargeAccountEvent event) {
        ApiCallback<ChargeAccountResultEvent> callback = new ApiCallback<>("ChargeAccount");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).chargeAccount(event).enqueue(callback);
        //socket.emit("chargeAccount", event.type, event.stripeToken, event.amount, (Ack) args -> eventBus.post(new ChargeAccountResultEvent((int) args[0], args.length > 1 ? args[1].toString() : null)));
    }

    @Subscribe
    public void HideTravel(final HideTravelEvent event) {
        ApiCallback<HideTravelResultEvent> callback = new ApiCallback<>("hideTravel");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).hideTravel(event).enqueue(callback);
        //socket.emit("hideTravel", event.travelId, (Ack) args -> eventBus.post(new HideTravelResultEvent(ServerResponse.OK.getValue())));
    }

    @Subscribe
    public void callRequest(ServiceCallRequestEvent event) {
        socket.emit("callRequest", (Ack) args -> eventBus.post(new ServiceCallRequestResultEvent((int) args[0])));
    }

    @Subscribe
    public void onCalculateFareRequested(CalculateFareRequestEvent event) {
        ApiCallback<CalculateFareResultEvent> callback = new ApiCallback<>("CalculateFare");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).calculateFare(event).enqueue(callback);

       /* socket.emit("calculateFare", event.pickupPosition, event.destinationPosition, (Ack) args -> {
            eventBus.post(new CalculateFareResultEvent(args));
        });*/
    }

    @Subscribe
    public void crudAddress(CRUDAddressRequestEvent event) {
        ApiCallback<CRUDAddressResultEvent> callback = new ApiCallback<>("crudAddress");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).crudAddress(event).enqueue(callback);
       /* socket.emit("crudAddress", event.mode.getValue(), event.address, (Ack) args -> {
            if (event.mode == CRUD.READ)
                eventBus.post(new CRUDAddressResultEvent((int) args[0], event.mode, (JSONArray) args[1]));
            else
                eventBus.post(new CRUDAddressResultEvent((int) args[0], event.mode));
        });*/
    }

    @Subscribe
    public void getPromotions(GetPromotionsRequestEvent event) {
        ApiCallback<GetPromotionsResultEvent> callback = new ApiCallback<>("GetPromotions");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getPromotions().enqueue(callback);
        //socket.emit("getPromotions", (Ack) args -> eventBus.post(new GetPromotionsResultEvent(args)));
    }

    @Subscribe
    public void getCoupons(GetCouponsRequestEvent event) {
        ApiCallback<GetCouponsResultEvent> callback = new ApiCallback<>("getCoupons");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getCoupons().enqueue(callback);
        // socket.emit("getCoupons", (Ack) args -> eventBus.post(new GetCouponsResultEvent(args)));
    }

    @Subscribe
    public void applyCoupon(ApplyCouponRequestEvent event) {
        ApiCallback<ApplyCouponResultEvent> callback = new ApiCallback<>("applyCoupons");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).applyCoupons(event).enqueue(callback);
        //socket.emit("applyCoupon", event.code, (Ack) args -> eventBus.post(new ApplyCouponResultEvent(args)));
    }

    @Subscribe
    public void addCoupon(AddCouponRequestEvent event) {
        ApiCallback<AddCouponResultEvent> callback = new ApiCallback<>("addCoupon");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).addCoupons(event).enqueue(callback);
        // socket.emit("addCoupon", event.code, (Ack) args -> eventBus.post(new AddCouponResultEvent(args)));
    }

    @Subscribe
    public void getTransactions(GetTransactionsRequestEvent event) {
        ApiCallback<GetTransactionsResultEvent> callback = new ApiCallback<>("getTransactions");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).getTransactions().enqueue(callback);
        //socket.emit("getTransactions", (Ack) args -> eventBus.post(new GetTransactionsResultEvent(args)));
    }

    @Subscribe
    public void notificationPlayerId(NotificationPlayerId event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("notificationPlayerId");
        RetrofitClient.getClient(mPref.getString("rider_token", null)).sendNotePlayerId(event).enqueue(callback);
    }

    @Subscribe
    public void searchLocationLatLng(GetSearchLocationLatLngEvent event) {
        ApiCallback<GoogleLocationDataBean> callback = new ApiCallback<>("searchLatLng");
        RetrofitClient.getGoogleClient().getLatLongUsingLocId(event.getPlaceId(), MAP_API_KEY).enqueue(callback);
    }

    @Subscribe
    public void getCityDetails(GetCityEvent event) {
        ApiCallback<CityList> callback = new ApiCallback<>("getCityList");
        RetrofitClient.getClient("").getCityDetails().enqueue(callback);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        EventBus.getDefault().register(this);
        eventBus.post(new BackgroundServiceStartedEvent());
        mPref = MyPreferenceManager.getInstance(this.getApplicationContext());
        return Service.START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
