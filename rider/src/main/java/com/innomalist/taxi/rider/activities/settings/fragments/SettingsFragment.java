package com.innomalist.taxi.rider.activities.settings.fragments;

import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;

import com.innomalist.taxi.rider.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.app_preferences);
    }
}
