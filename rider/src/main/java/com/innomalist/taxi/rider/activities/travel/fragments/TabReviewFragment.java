package com.innomalist.taxi.rider.activities.travel.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innomalist.taxi.common.components.BaseFragment;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.rider.R;
import com.innomalist.taxi.rider.databinding.FragmentTravelReviewBinding;

import org.greenrobot.eventbus.EventBus;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class TabReviewFragment extends BaseFragment {
    FragmentTravelReviewBinding binding;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = EventBus.getDefault();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_travel_review,container,false);
        binding.ratingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> binding.buttonSaveReview.setEnabled(true));
        binding.buttonSaveReview.setOnClickListener(view -> {
            ReviewDriverEvent reviewDriverEvent = new ReviewDriverEvent((int)binding.ratingBar.getRating() * 20,binding.reviewText.getText().toString());
            eventBus.post(new ReviewDriverEvent((int)binding.ratingBar.getRating() * 20,binding.reviewText.getText().toString()));
        });
        return binding.getRoot();
    }
}
