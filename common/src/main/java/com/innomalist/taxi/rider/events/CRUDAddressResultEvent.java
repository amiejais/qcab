package com.innomalist.taxi.rider.events;

import com.innomalist.taxi.common.models.Address;
import com.innomalist.taxi.common.models.CRUD;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class CRUDAddressResultEvent extends AbstractApiResponse {
    public CRUD crud;
    public List<Address> addresses;

}
