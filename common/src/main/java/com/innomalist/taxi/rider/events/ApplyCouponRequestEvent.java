package com.innomalist.taxi.rider.events;

public class ApplyCouponRequestEvent {
    public String code;

    public ApplyCouponRequestEvent(String code) {
        this.code = code;
    }

}
