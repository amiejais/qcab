package com.innomalist.taxi.rider.events;

import com.innomalist.taxi.common.models.ServiceCategory;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.ArrayList;

public class CalculateFareResultEvent extends AbstractApiResponse {
    public ArrayList<ServiceCategory> data;

}
