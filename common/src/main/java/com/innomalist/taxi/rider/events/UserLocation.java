package com.innomalist.taxi.rider.events;

import android.os.Parcel;
import android.os.Parcelable;

import com.innomalist.taxi.common.network.AbstractApiResponse;

public class UserLocation extends AbstractApiResponse implements Parcelable {

    private static final long serialVersionUID = 1L;

    private String locality;
    private String latitiude;
    private String longitude;
    private String placeId;
    private String city;
    private String country;
    private String postalCode;

    public UserLocation() {
    }

    public UserLocation(String city, String country, String postal_code) {
        this.city = city;
        this.country = country;
        this.postalCode = postal_code;
    }


    public UserLocation(String latitude, String longitude) {
        this.latitiude = latitude;
        this.longitude = longitude;
    }

    public UserLocation(String name) {
        this.locality = name;
    }


    protected UserLocation(Parcel in) {
        locality = in.readString();
        latitiude = in.readString();
        longitude = in.readString();
        placeId = in.readString();
        city = in.readString();
        country = in.readString();
        postalCode = in.readString();
    }

    public static final Creator<UserLocation> CREATOR = new Creator<UserLocation>() {
        @Override
        public UserLocation createFromParcel(Parcel in) {
            return new UserLocation(in);
        }

        @Override
        public UserLocation[] newArray(int size) {
            return new UserLocation[size];
        }
    };

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLatitiude() {
        return latitiude;
    }

    public void setLatitiude(String latitiude) {
        this.latitiude = latitiude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "UserLocation{" +
                "locality='" + locality + '\'' +
                ", latitiude=" + latitiude +
                ", longitude=" + longitude +
                ", city=" + city +
                ", country=" + country +
                ", postal code=" + postalCode +
                ", placeId='" + placeId + '\'' +
                '}';
    }

    public String getAddress() {
        return locality;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(locality);
        parcel.writeString(latitiude);
        parcel.writeString(longitude);
        parcel.writeString(placeId);
        parcel.writeString(city);
        parcel.writeString(country);
        parcel.writeString(postalCode);
    }
}