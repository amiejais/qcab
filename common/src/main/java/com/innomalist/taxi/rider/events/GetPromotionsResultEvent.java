package com.innomalist.taxi.rider.events;

import com.innomalist.taxi.common.models.Promotion;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class GetPromotionsResultEvent extends AbstractApiResponse{
    public List<Promotion> data;

}
