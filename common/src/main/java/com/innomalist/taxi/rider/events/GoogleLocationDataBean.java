package com.innomalist.taxi.rider.events;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.io.Serializable;
import java.util.List;

public class GoogleLocationDataBean extends AbstractApiResponse implements Serializable {

    @SerializedName("results")
    @Expose
    private List<Result> results = null;

    @SerializedName("result")
    @Expose
    private Result latLongResult;


    public UserLocation getLocationName() {
        UserLocation location = new UserLocation();
        if (results != null && !results.isEmpty()) {
            for (int i = 0; i < results.size(); i++) {
                if (results.get(i) != null && !TextUtils.isEmpty(results.get(i).getFormattedAddress())) {
                    location.setLatitiude(String.valueOf(results.get(i).getGeometry().getLocation().getLat()));
                    location.setLongitude(String.valueOf(results.get(i).getGeometry().getLocation().getLng()));
                    location.setLocality(results.get(i).getFormattedAddress());
                    break;
                }
            }
        }
        return location;
    }


    public UserLocation getLatLong(boolean isPlaceIdAvailable) {
        UserLocation location = null;
        if (isPlaceIdAvailable && latLongResult != null && latLongResult.getGeometry() != null && latLongResult.getGeometry().getLocation() != null) {
            location = new UserLocation();
            location.setLatitiude(String.valueOf(latLongResult.getGeometry().getLocation().getLat()));
            location.setLongitude(String.valueOf(latLongResult.getGeometry().getLocation().getLng()));
        } else if (results != null && !results.isEmpty()) {
            location = new UserLocation();
            for (Result result : results) {
                if (result != null && result.getGeometry() != null && result.getGeometry().getLocation() != null) {
                    location.setLatitiude(String.valueOf(result.getGeometry().getLocation().getLat()));
                    location.setLongitude(String.valueOf(result.getGeometry().getLocation().getLng()));

                    if (result.getAddressComponents() != null && !result.getAddressComponents().isEmpty()) {
                        BLOCK:
                        for (int j = 0; j < result.getAddressComponents().size(); j++) {
                            List<String> types = result.getAddressComponents().get(j).getTypes();
                            if (types != null && !types.isEmpty()) {
                                for (int k = 0; k < types.size(); k++) {
                                    if (types.get(k).equals("sublocality_level_1")) {
                                        location.setLocality(result.getAddressComponents().get(j).getLongName());
                                        break BLOCK;
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        return location;
    }
}

class Result implements Serializable {

    @SerializedName("formatted_address")
    @Expose
    private String formattedAddress;

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;

    @SerializedName("address_components")
    @Expose
    private List<AddressComponent> addressComponents = null;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }


    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }


    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

}

class Geometry implements Serializable {

    @SerializedName("location")
    @Expose
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}

class Location implements Serializable {

    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("lng")
    @Expose
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

}

class AddressComponent implements Serializable {

    @SerializedName("long_name")
    @Expose
    private String longName;
    @SerializedName("short_name")
    @Expose
    private String shortName;
    @SerializedName("types")
    @Expose
    private List<String> types = null;

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}