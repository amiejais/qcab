package com.innomalist.taxi.rider.events;

import com.innomalist.taxi.common.models.Coupon;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class GetCouponsResultEvent extends AbstractApiResponse {
    public List<Coupon> data;

}
