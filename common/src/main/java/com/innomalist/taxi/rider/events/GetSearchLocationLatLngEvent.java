package com.innomalist.taxi.rider.events;

public class GetSearchLocationLatLngEvent {
    private String placeId;

    public GetSearchLocationLatLngEvent(String pPLaceId) {
        placeId = pPLaceId;
    }

    public String getPlaceId() {
        return placeId;
    }
}
