package com.innomalist.taxi.rider.events;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.innomalist.taxi.common.models.Address;
import com.innomalist.taxi.common.models.CRUD;
import com.innomalist.taxi.common.utils.LatLngDeserializer;

import org.json.JSONException;
import org.json.JSONObject;

public class CRUDAddressRequestEvent {
    public CRUD mode;
    public JSONObject address = null;

    public CRUDAddressRequestEvent(CRUD mode, Address address) {
        this.mode = mode;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LatLng.class, new LatLngDeserializer());
        Gson customGson = gsonBuilder.create();
        String jsonString = customGson.toJson(address);
        try {
            this.address = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public CRUDAddressRequestEvent(CRUD mode) {
        this.mode = mode;
    }
}
