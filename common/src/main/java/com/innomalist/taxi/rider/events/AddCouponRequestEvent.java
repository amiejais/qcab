package com.innomalist.taxi.rider.events;

public class AddCouponRequestEvent {
    public String code;

    public AddCouponRequestEvent(String code) {
        this.code = code;
    }
}
