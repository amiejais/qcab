package com.innomalist.taxi.rider.events;

import com.innomalist.taxi.common.models.DriverLocation;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.ArrayList;

public class GetDriversLocationResultEvent extends AbstractApiResponse {
    public ArrayList<DriverLocation> data;
}