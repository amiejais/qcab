package com.innomalist.taxi.rider.events;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.models.DevicePoints;

import org.json.JSONException;
import org.json.JSONObject;

public class CalculateFareRequestEvent {


    @SerializedName("pickupPosition")
    @Expose
    public DevicePoints pickupPosition;

    @SerializedName("destinationPosition")
    @Expose
    public DevicePoints destinationPosition;

    public CalculateFareRequestEvent(LatLng startPoints, LatLng endPoints) {
        pickupPosition = new DevicePoints(startPoints.longitude, startPoints.latitude);
        destinationPosition = new DevicePoints(endPoints.longitude, endPoints.latitude);
    }
}
