package com.innomalist.taxi.rider.events;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.models.DevicePoints;

public class GetDriversLocationEvent {

    @SerializedName("point")
    @Expose
    public DevicePoints point;

    public GetDriversLocationEvent(LatLng points) {
        point = new DevicePoints(points.longitude, points.latitude);
    }
}
