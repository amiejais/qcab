package com.innomalist.taxi.driver.models;

import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class CityList extends AbstractApiResponse {
    private List<City> data;

    public List<City> getCityList() {
        return data;
    }
}
