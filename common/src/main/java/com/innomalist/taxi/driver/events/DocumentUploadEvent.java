package com.innomalist.taxi.driver.events;

import java.util.List;

import okhttp3.MultipartBody;

public class DocumentUploadEvent {

    private boolean isUserLoggedIn;
    private String type;
    private List<MultipartBody.Part> filetoupload;

    public DocumentUploadEvent(String dType, List<MultipartBody.Part> parts, boolean isUserLogin) {
        type = dType;
        filetoupload = parts;
        isUserLoggedIn = isUserLogin;
    }

    public List<MultipartBody.Part> getPartList() {
        return filetoupload;
    }

    public boolean isUserLoggedIn() {
        return isUserLoggedIn;
    }

    public String getType() {
        if (type.equalsIgnoreCase("Driving Licence")) {
            return "DL";
        } else if (type.equalsIgnoreCase("Smart Card")) {
            return "RC";
        } else if (type.equalsIgnoreCase("Commercial Licence")) {
            return "CVL";
        } else if (type.equalsIgnoreCase("Adhaar Card")) {
            return "AADHAAR";
        } else if (type.equalsIgnoreCase("Voter Id")) {
            return "VOTERID";
        } else if (type.equalsIgnoreCase("Pan Card")) {
            return "PAN";
        } else if (type.equalsIgnoreCase("Sale Deed")) {
            return "PAN";
        } else if (type.equalsIgnoreCase("NOC")) {
            return "PAN";
        } else if (type.equalsIgnoreCase("Permit")) {
            return "PAN";
        } else if (type.equalsIgnoreCase("Insurance")) {
            return "PAN";
        }
        return "OTHER";
    }
}
