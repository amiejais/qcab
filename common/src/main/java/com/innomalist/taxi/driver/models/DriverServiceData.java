package com.innomalist.taxi.driver.models;

public class DriverServiceData {
    private int id;
    private String title;
    private Integer is_active;
    private Integer primary;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer isIs_active() {
        return is_active;
    }

    public void setIs_active(Integer is_active) {
        this.is_active = is_active;
    }

    public Integer getPrimary() {
        return primary;
    }
}