package com.innomalist.taxi.driver.models;

public class City extends Common {
    private int id;
    private String name;

    public City(int i, String select_city) {
        this.id = i;
        name = select_city;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
