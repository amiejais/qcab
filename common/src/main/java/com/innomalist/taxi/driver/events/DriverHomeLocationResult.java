package com.innomalist.taxi.driver.events;

import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class DriverHomeLocationResult extends AbstractApiResponse {

    private List<RequestTime> data;

    public List<RequestTime> getData() {
        return data;
    }

    public class RequestTime {
        private Integer id;
        private Integer driver_id;
        private String req_time;

        public Integer getId() {
            return id;
        }

        public Integer getDriver_id() {
            return driver_id;
        }

        public String getReq_time() {
            return req_time;
        }
    }
}
