package com.innomalist.taxi.driver.events;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class RideAcceptNewEvent extends AbstractApiResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("travel")
        @Expose
        private Travel travel;
        @SerializedName("rider")
        @Expose
        private Rider rider;

        public Travel getTravel() {
            travel.setRider(rider);
            return travel;
        }

        public void setTravel(Travel travel) {
            this.travel = travel;
        }

        public Rider getRider() {
            return rider;
        }

        public void setRider(Rider rider) {
            this.rider = rider;
        }

    }

    public boolean isSuccessful() {
        return status == 200;
    }
}
