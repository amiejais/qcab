package com.innomalist.taxi.driver.events;

public class RejectRequestEvent {

    private int driver_id;
    private int rider_id;

    public RejectRequestEvent(int driverId, int riderId) {
        driver_id = driverId;
        rider_id = riderId;
    }
}
