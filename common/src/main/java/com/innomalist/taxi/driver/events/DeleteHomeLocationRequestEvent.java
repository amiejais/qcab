package com.innomalist.taxi.driver.events;

public class DeleteHomeLocationRequestEvent {
    private int request_id;

    public DeleteHomeLocationRequestEvent(int id) {
        request_id = id;
    }
}
