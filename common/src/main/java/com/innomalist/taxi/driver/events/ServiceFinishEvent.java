package com.innomalist.taxi.driver.events;

import com.google.android.gms.maps.model.LatLng;
import com.innomalist.taxi.common.events.BaseRequestEvent;
import com.innomalist.taxi.common.events.BaseResultEvent;
import com.innomalist.taxi.common.utils.ServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceFinishEvent  {

    public String service_id;
    public int duration;
    public int distance;
    public JSONObject destinationPoint;
    public String dropOffLocation;
    public String log;

    public ServiceFinishEvent(String serviceId, int duration, LatLng destinationPoint, String destinationAddress, int distance, String log) {
        this.service_id = serviceId;
        this.duration = duration;
        try {
            JSONObject arrayDrop = new JSONObject();
            arrayDrop.put("x", destinationPoint.longitude);
            arrayDrop.put("y", destinationPoint.latitude);
            this.destinationPoint = arrayDrop;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.dropOffLocation = destinationAddress;
        this.distance = distance;
        this.log = log;
    }
}
