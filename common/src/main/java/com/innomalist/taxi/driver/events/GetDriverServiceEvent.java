package com.innomalist.taxi.driver.events;

public class GetDriverServiceEvent {
    private Integer categoryId;

    public GetDriverServiceEvent(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
