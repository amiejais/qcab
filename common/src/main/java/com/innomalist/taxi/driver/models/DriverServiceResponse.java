package com.innomalist.taxi.driver.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class DriverServiceResponse extends AbstractApiResponse {

    @SerializedName("data")
    @Expose
    private List<DriverServiceData> data = null;

    public List<DriverServiceData> getData() {
        return data;
    }

    public void setData(List<DriverServiceData> data) {
        this.data = data;
    }

}