package com.innomalist.taxi.driver.events;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.network.AbstractApiResponse;

public class ServiceFinishResultEvent extends AbstractApiResponse {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public class Data {
        public boolean isCreditUsed;
        public float amount;
    }

}
