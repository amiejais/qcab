package com.innomalist.taxi.driver.events;

import com.innomalist.taxi.common.network.AbstractApiResponse;
import com.innomalist.taxi.driver.models.Report;
import com.innomalist.taxi.driver.models.Stats;

import java.util.ArrayList;

public class GetStatisticsResultEvent extends AbstractApiResponse {
    public ArrayList<Report> reports;
    public Stats stats;

}
