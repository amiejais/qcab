package com.innomalist.taxi.driver.events;

public class GetStatisticsEvent {
    public int timeQuery;

    public enum QueryTime {
        DAILY(1),
        WEEKLY(2),
        MONTHLY(3);

        private int queryTime;

        QueryTime(int value) {
            this.queryTime = value;
        }

        public static QueryTime get(int code) {
            for (QueryTime s : values()) {
                if (s.queryTime == code) return s;
            }
            return null;
        }

        public int getValue() {
            return queryTime;
        }
    }


    public GetStatisticsEvent(int timeQuery) {
        //super(new GetStatisticsResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue(),null,null));
        this.timeQuery = timeQuery;
    }

}
