package com.innomalist.taxi.driver.events;

public class DriverHomeLocationRequestEvent {

    private String requestTag;

    public String getRequestTag() {
        return requestTag;
    }

    public DriverHomeLocationRequestEvent(String tagName) {
        requestTag = tagName;
    }
}
