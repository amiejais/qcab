package com.innomalist.taxi.driver.events;

import java.util.ArrayList;

public class SerDriverServicesEvent {
    private ArrayList<Integer> serviceIds;

    public SerDriverServicesEvent(ArrayList<Integer> serviceIds) {
        this.serviceIds = serviceIds;
    }
}
