package com.innomalist.taxi.driver.events;

import com.innomalist.taxi.common.models.Request;
import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.ArrayList;

public class GetRequestsResultEvent extends AbstractApiResponse {
    public ArrayList<Request> data;

}
