package com.innomalist.taxi.driver.events;

import com.google.android.gms.maps.model.LatLng;

public class SetDriverHomeLocationEvent {

    private double home_lat;
    private double home_lng;

    public SetDriverHomeLocationEvent(LatLng location) {
        home_lat = location.latitude;
        home_lng = location.longitude;
    }
}
