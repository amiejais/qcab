package com.innomalist.taxi.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DevicePoints {

    @SerializedName("x")
    @Expose
    private Double x;
    @SerializedName("y")
    @Expose
    private Double y;

    public DevicePoints(Double xPoint, Double yPont) {
        x = xPoint;
        y = yPont;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

}