package com.innomalist.taxi.common.location;

import android.animation.ValueAnimator;
import android.util.Log;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import static java.lang.Math.toDegrees;

public class MapHelper {
    public static void centerLatLngsInMap(GoogleMap googleMap, List<LatLng> locations, boolean animate) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng location : locations)
            builder.include(location);
        LatLngBounds bounds = builder.build();
        int padding = 100; // in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        if (animate)
            googleMap.animateCamera(cu);
        else
            googleMap.moveCamera(cu);
    }

    public static void animateMarkerToLocation(final LatLng endPosition, final Marker marker) {

        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        float rotation = getBearing(startPosition, endPosition);
                        marker.setPosition(newPosition);
                        if (!Float.isNaN(rotation)) {
                            marker.setRotation(rotation);
                        }
                        marker.setAnchor(.5f, .5f);
                        marker.setFlat(true);

                    } catch (Exception ex) {
                        Log.e("TAg", "animateMarkerNew Exception: " + ex.getMessage());
                    }
                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    //Method for finding bearing between two points
    private static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    public static void moveMap(GoogleMap googleMap, LatLng endPosition) {
        final CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(endPosition)
                // .bearing(rotation)// Sets the center of the map to Mountain View
                .zoom(18)                   // Sets the zoom
                .tilt(googleMap.getCameraPosition().tilt)// Sets the orientation of the camera to east
                .build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


}
