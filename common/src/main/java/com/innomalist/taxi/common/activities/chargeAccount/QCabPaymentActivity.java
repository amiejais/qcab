package com.innomalist.taxi.common.activities.chargeAccount;

import android.app.Activity;
import android.content.IntentFilter;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.innomalist.taxi.common.R;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.databinding.ActivityChargeAccountBinding;
import com.innomalist.taxi.common.events.ChargeAccountEvent;
import com.innomalist.taxi.common.events.ChargeAccountResultEvent;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.NumberThousandWatcher;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;

import in.juspay.godel.analytics.GodelTracker;
import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;


public class QCabPaymentActivity extends BaseActivity {
    private ActivityChargeAccountBinding binding;
    private InstapayListener listener;

    private enum PaymentMode {
        paypal,
        instamojo
    }

    PaymentMode paymentMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_charge_account);
        initializeToolbar(getString(R.string.title_charge),true);

        binding.editText.setText(String.valueOf(Math.round(getIntent().getDoubleExtra("defaultAmount", 0f))));
        binding.paymentPaypal.setOnClickListener(view -> {
            binding.checkoutButton.setText(getString(R.string.checkout_filled, getString(R.string.checkout_paypal)));
            paymentMode = PaymentMode.paypal;
            binding.checkoutButton.setEnabled(true);
        });
        binding.paymentInstamojo.setOnClickListener(view -> {
            binding.checkoutButton.setText(getString(R.string.checkout_filled, getString(R.string.checkout_instamojo)));
            paymentMode = PaymentMode.instamojo;
            binding.checkoutButton.setEnabled(true);
        });


        if (!getResources().getBoolean(R.bool.payment_stripe_enabled))
            binding.paymentStripe.setVisibility(View.GONE);
        if (!getResources().getBoolean(R.bool.payment_web_enabled))
            binding.paymentOnline.setVisibility(View.GONE);
        if (getResources().getBoolean(R.bool.payment_web_enabled) && !getResources().getBoolean(R.bool.payment_stripe_enabled)) {
            binding.paymentOnline.callOnClick();
            binding.layoutMethods.setVisibility(View.GONE);
            binding.titleMethod.setVisibility(View.GONE);
        }
        if (!getResources().getBoolean(R.bool.payment_web_enabled) && getResources().getBoolean(R.bool.payment_stripe_enabled)) {
            binding.paymentStripe.callOnClick();
            binding.layoutMethods.setVisibility(View.GONE);
            binding.titleMethod.setVisibility(View.GONE);
        }
        binding.editText.addTextChangedListener(new NumberThousandWatcher(binding.editText));

        Double balance = CommonUtils.driver != null ? CommonUtils.driver.getBalance() : CommonUtils.rider.getBalance();
        binding.textCurrentBalance.setText(getString(R.string.unit_money, balance));
        binding.chargeAddFirst.setText(NumberFormat.getInstance().format(getResources().getInteger(R.integer.charge_first)));
        binding.chargeAddSecond.setText(NumberFormat.getInstance().format(getResources().getInteger(R.integer.charge_second)));
        binding.chargeAddThird.setText(NumberFormat.getInstance().format(getResources().getInteger(R.integer.charge_third)));
        binding.chargeAddFirst.setOnClickListener(view -> addCharge(binding.chargeAddFirst));
        binding.chargeAddSecond.setOnClickListener(view -> addCharge(binding.chargeAddSecond));
        binding.chargeAddThird.setOnClickListener(view -> addCharge(binding.chargeAddThird));
    }

    public void onCheckoutClicked(View view) {
        if (binding.editText.getText().toString().isEmpty()) {
            AlerterHelper.showError(QCabPaymentActivity.this, getString(R.string.error_charge_field_empty));
            return;
        }
        int amount = (Integer.parseInt(binding.editText.getText().toString().replace(",", "")));
        if (amount < getResources().getInteger(R.integer.minimum_charge_amount)) {
            AlerterHelper.showError(QCabPaymentActivity.this, getString(R.string.error_charge_field_low, getResources().getInteger(R.integer.minimum_charge_amount)));
            return;
        }
        if (paymentMode == PaymentMode.paypal) {
            Toast.makeText(this, "under construction", Toast.LENGTH_SHORT).show();
        } else {
            boolean isDriver = CommonUtils.driver != null;

            if (isDriver) {
                Driver driver = CommonUtils.driver;
                callInstamojoPay(driver.getEmail(), String.valueOf(driver.getMobileNumber()), binding.editText.getText().toString(), "Add Money to Driver wallet", driver.getFirstName());
            } else {
                Rider rider = CommonUtils.rider;
                callInstamojoPay(rider.getEmail(), String.valueOf(rider.getMobileNumber()), binding.editText.getText().toString(), "Add Money to User wallet", rider.getFirstName());

            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void AccountCharged(ChargeAccountResultEvent event) {
        if (event.hasError())
            event.showAlert(QCabPaymentActivity.this);
        else {
            setResult(RESULT_OK);
            finish();
        }

    }

    void addCharge(Button button) {
        try {
            binding.editText.setText(String.valueOf(NumberFormat.getInstance().parse(button.getText().toString()).intValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {

            if (TextUtils.isEmpty(email)) {
                email = "qcab2018@gmail.com";
            }

            if (TextUtils.isEmpty(buyername)) {
                buyername = "QCab";
            }
            phone = deleteCountry(phone);

            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", false);
            pay.put("send_email", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {
                Log.e("TAg", "payment Respone :: " + response);
                if (!TextUtils.isEmpty(response)) {
                    eventBus.post(new ChargeAccountEvent("instamojo", response, Float.valueOf(binding.editText.getText().toString())));
                    //GodelTracker.getInstance().trackPaymentStatus(response, GodelTracker.SUCCESS);
                }
            }

            @Override
            public void onFailure(int code, String reason) {
                Toast.makeText(getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                        .show();
                //GodelTracker.getInstance().trackPaymentStatus(reason, GodelTracker.FAILURE);
            }
        };
    }

    private String deleteCountry(String phone) {
        PhoneNumberUtil phoneInstance = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phoneNumber = phoneInstance.parse(phone, "IN");
            return "" + phoneNumber.getNationalNumber();
        } catch (Exception e) {
        }
        return phone;
    }

}
