package com.innomalist.taxi.common.events;

public class ChargeAccountEvent {
    public String type;
    public String token;
    public float amount;

    public ChargeAccountEvent(String type, String response, float valueOf) {
        this.type = type;
        this.token = response;
        this.amount = valueOf;
    }
}
