package com.innomalist.taxi.common.network;

import android.text.TextUtils;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;

import static com.innomalist.taxi.common.network.IApiRequet.GOOGLE_BASE_URL;

public class RetrofitClient {


    static OkHttpClient getOkHttpClient(String token) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest;

            if (TextUtils.isEmpty(token)) {
                newRequest = chain.request().newBuilder()
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("Accept", "application/json")
                        .header("Accept-Encoding", "identity")
                        .header("Connection", "close")
                        .build();

            } else {
                newRequest = chain.request().newBuilder()
                        .addHeader("token", token)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .header("Connection", "close")
                        .build();
            }

            return chain.proceed(newRequest);
        }).connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS).build();
        return client;
    }


    static OkHttpClient getFormDataOkHttpClient(String token) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest;

            if (TextUtils.isEmpty(token)) {
                newRequest = chain.request().newBuilder()
                        .header("Content-Type", "multipart/form-data")
                        .build();

            } else {
                newRequest = chain.request().newBuilder()
                        .addHeader("token", token)
                        .addHeader("Content-Type", "multipart/form-data")
                        .build();
            }

            return chain.proceed(newRequest);
        }).connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS).build();
        return client;
    }

    public static IApiRequet getClient(String token) {
        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl(IApiRequet.BASE_URL)
                .client(getOkHttpClient(token))
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json locationData to object
                .build();

        return retrofit.create(IApiRequet.class);
    }

    public static IApiRequet getFormDataClient(String token) {
        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl(IApiRequet.BASE_URL)
                .client(getFormDataOkHttpClient(token))
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json locationData to object
                .build();

        return retrofit.create(IApiRequet.class);
    }

    public static IApiRequet getGoogleClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GOOGLE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json locationData to object
                .build();
        return retrofit.create(IApiRequet.class);
    }


}