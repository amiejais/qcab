package com.innomalist.taxi.common.events;

import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.network.AbstractApiResponse;

public class GetStatusResultEvent extends AbstractApiResponse {
    private Travel data;

    public Travel getTravel() {
        return data;
    }
}
