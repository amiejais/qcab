package com.innomalist.taxi.common.activities.travels;

import android.Manifest;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.R;
import com.innomalist.taxi.common.activities.travels.adapters.TravelsRecyclerViewAdapter;
import com.innomalist.taxi.common.activities.travels.fragments.WriteComplaintDialog;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.databinding.ActivityTravelsBinding;
import com.innomalist.taxi.common.events.GetTravelsEvent;
import com.innomalist.taxi.common.events.GetTravelsResultEvent;
import com.innomalist.taxi.common.events.HideTravelEvent;
import com.innomalist.taxi.common.events.HideTravelResultEvent;
import com.innomalist.taxi.common.events.WriteComplaintEvent;
import com.innomalist.taxi.common.events.WriteComplaintResultEvent;
import com.innomalist.taxi.common.models.FileDownload;
import com.innomalist.taxi.common.models.RiderBill;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.network.ApiCallback;
import com.innomalist.taxi.common.network.RetrofitClient;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.tylersuehr.esr.ContentItemLoadingStateFactory;
import com.tylersuehr.esr.EmptyStateRecyclerView;
import com.tylersuehr.esr.ImageTextStateDisplay;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class TravelsActivity extends BaseActivity implements WriteComplaintDialog.onWriteComplaintInteractionListener {

    private String subjectText = "";
    private String contentText = "";
    private long lastSelectedTravelId;
    ActivityTravelsBinding binding;
    private MyPreferenceManager SP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SP = MyPreferenceManager.getInstance(this.getApplicationContext());
        binding = DataBindingUtil.setContentView(TravelsActivity.this, R.layout.activity_travels);
        initializeToolbar(getString(R.string.title_travel), true);
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_LOADING, ContentItemLoadingStateFactory.newListLoadingState(this));
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_EMPTY, new ImageTextStateDisplay(this, R.drawable.empty_state, "Oops!", "Nothing to show here :("));
        binding.recyclerView.setStateDisplay(EmptyStateRecyclerView.STATE_ERROR, new ImageTextStateDisplay(this, R.drawable.empty_state, "SORRY...!", "Something went wrong :("));
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_LOADING);
        getTravels();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWriteComplaintResult(WriteComplaintResultEvent event) {
        LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelsActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    eventBus.post(new WriteComplaintEvent(lastSelectedTravelId, subjectText, contentText));
                    LoadingDialog.show(TravelsActivity.this, getString(R.string.sending_complaint));
                }
            });
        } else {
            AlerterHelper.showInfo(TravelsActivity.this, getString(R.string.message_complaint_sent));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHideTravelResult(HideTravelResultEvent event) {
        if (event.hasError())
            return;
        AlerterHelper.showInfo(TravelsActivity.this, getString(R.string.info_travel_hidden));
        getTravels();

    }

    private void getTravels() {
        eventBus.post(new GetTravelsEvent());
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_LOADING);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTravelsReceived(GetTravelsResultEvent event) {
        if (event.status != 200) {
            binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_ERROR);
           /* event.showError(TravelsActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    getTravels();
                else
                    finish();
            });*/
            return;
        }
        if (event.data.size() == 0) {
            binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_EMPTY);
            return;
        }
        binding.recyclerView.invokeState(EmptyStateRecyclerView.STATE_OK);
        loadList(event.data);
    }

    private void loadList(final ArrayList<Travel> travels) {
        if (travels == null)
            return;

        final TravelsRecyclerViewAdapter adapter = new TravelsRecyclerViewAdapter(TravelsActivity.this, travels, new TravelsRecyclerViewAdapter.OnTravelItemInteractionListener() {
            @Override
            public void onHideTravel(Travel travel) {
                AlertDialogBuilder.show(TravelsActivity.this, getString(R.string.question_hide_travel), AlertDialogBuilder.DialogButton.OK_CANCEL, result -> {
                    if (result == AlertDialogBuilder.DialogResult.OK)
                        eventBus.post(new HideTravelEvent(travel.getId()));
                });
            }

            @Override
            public void onWriteComplaint(Travel travel) {
                lastSelectedTravelId = travel.getId();
                FragmentManager fm = getSupportFragmentManager();
                (new WriteComplaintDialog()).show(fm, "fragment_complaint");

            }

            @Override
            public void onDownloadInvoice(final Integer travelId) {
                TedPermission.with(TravelsActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                RiderBill riderBill = new RiderBill();
                                riderBill.setTravelid(travelId);
                                ApiCallback<FileDownload> apiCallback = new ApiCallback<>("BillInfo");
                                RetrofitClient.getClient(null).getBillInfo(riderBill).enqueue(apiCallback);

                               /* FileDownload fileDownload = new FileDownload();
                                fileDownload.statusCode = 200;
                                fileDownload.file = "bills/7473.pdf";
                                onGetBillDetails(fileDownload);*/
                                // SaveImage(getBitmapFromView(root));
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                            }
                        }).setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .setDeniedMessage("If not allowed, the invoice would not be saved")
                        .check();

            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(TravelsActivity.this);
        llm.setOrientation(RecyclerView.VERTICAL);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(llm);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSaveComplaintClicked(WriteComplaintEvent event) {
        event.travelId = lastSelectedTravelId;
        eventBus.post(event);
        LoadingDialog.show(TravelsActivity.this, getString(R.string.sending_complaint));
    }

    public static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/qcab");
        myDir.mkdirs();

        String fname = "Invoice-" + System.currentTimeMillis() + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Toast.makeText(TravelsActivity.this, "Invoice saved in qcab folder", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetBillDetails(FileDownload event) {
        if (event.getStatus() == 200) {
            RetrofitClient.getClient(null).downloadBill(event.getFile()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            writeResponseBodyToDisk(response.body(), event.getFile());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(TravelsActivity.this, "onFailure" + t.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }

    }


    private boolean writeResponseBodyToDisk(ResponseBody body, String travelId) {
        try {
            travelId = travelId.replace("/bills/", "");
            File myDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());
            myDir.mkdirs();

            String fname = "Invoice-" + travelId;
            File futureStudioIconFile = new File(myDir, fname);
            //if (futureStudioIconFile.exists()) futureStudioIconFile.delete();

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d("File Download: ", fileSizeDownloaded + " of " + fileSize);
                    Toast.makeText(TravelsActivity.this, "Bill  successfully download at Downloads folder", Toast.LENGTH_SHORT).show();

                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}