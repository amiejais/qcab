package com.innomalist.taxi.common.activities.signup;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.afollestad.materialdialogs.MaterialDialog;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.R;
import com.innomalist.taxi.common.activities.signup.adapter.CarListAdapter;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.databinding.ActivitySignUpSecondStepBinding;
import com.innomalist.taxi.common.models.Car;
import com.innomalist.taxi.common.models.CarDetailList;
import com.innomalist.taxi.common.models.DriverResultEvent;
import com.innomalist.taxi.common.models.RegisterEvent;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.models.RiderResultEvent;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.events.DocumentUploadEvent;
import com.innomalist.taxi.driver.models.City;
import com.innomalist.taxi.driver.models.CityList;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.events.GetCarEvent;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.innomalist.taxi.common.utils.ImageUtils.deleteImages;
import static com.innomalist.taxi.common.utils.ImageUtils.getFilePath;

public class SignUpSecondStepActivity extends BaseActivity {

    private MyPreferenceManager mPref;
    private ActivitySignUpSecondStepBinding mBinding;
    private boolean isDriver;
    private int count;
    private int mCurrentDoc = -1;
    private ArrayList<String> mIdProofList;
    private String mProfilePicPath;
    private String mFirstImagePath;
    private String mSecondImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_second_step);
        mPref = MyPreferenceManager.getInstance(getApplicationContext());
        mBinding.signUpBtn.setOnClickListener(view -> doValidateAndRegisterUser());
        isDriver = getIntent().getStringExtra("User").equalsIgnoreCase(getString(com.innomalist.taxi.common.R.string.driver_registration));
        setSupportActionBar(mBinding.toolbar);
        if (getSupportActionBar() != null && getIntent() != null) {
            getSupportActionBar().setTitle(getIntent().getStringExtra("User").equalsIgnoreCase(getString(R.string.driver_registration)) ? getString(R.string.driver_registration) : getString(R.string.user_registration));
            mBinding.edtNumber.setText(getIntent().getStringExtra("Mobile"));
        }

        mBinding.profileImage.setOnClickListener(view -> {
            onGetPictureCalled(0);
        });
        if (isDriver) {
            mIdProofList = new ArrayList<>();
            mBinding.driverDocAcceptanceLayout.setVisibility(View.VISIBLE);
            mBinding.driverAcceptanceLayout.setVisibility(View.VISIBLE);
            mBinding.driverLayout.setVisibility(View.VISIBLE);
            //eventBus.post(new GetCityEvent());
            eventBus.post(new GetCarEvent());

            mBinding.ivIdOne.setOnClickListener(view -> {
                onGetPictureCalled(1);
            });

            mBinding.ivIdTwo.setOnClickListener(view -> {
                onGetPictureCalled(2);
            });

            mBinding.tvTC.setOnClickListener(view -> {
                showTermsAndCondition();
            });

        }
    }


    private void doValidateAndRegisterUser() {

        if (isDriver) {
            if (count < 1) {
                showError("please wait. getting car details");
                return;
            }
            mIdProofList.clear();
            if (!TextUtils.isEmpty(mFirstImagePath)) {
                mIdProofList.add(mFirstImagePath);
            }
            if (!TextUtils.isEmpty(mSecondImagePath)) {
                mIdProofList.add(mSecondImagePath);
            }
        }

        String mobileNumber = mBinding.edtNumber.getText().toString();
        String firstName = mBinding.edtFirstName.getText().toString();
        String lastName = mBinding.edtLastName.getText().toString();
        String email = mBinding.edtEmail.getText().toString();
        String address = mBinding.edtAddress.getText().toString();
        String carRegNumber = mBinding.edtCarRegNumber.getText().toString();
        if (TextUtils.isEmpty(mobileNumber)) {
            showError(getString(R.string.invalid_mobile_number));
            return;
        } else if (isDriver && TextUtils.isEmpty(mProfilePicPath)) {
            showError(getString(R.string.select_profile_picture));
            return;
        } else if (TextUtils.isEmpty(firstName)) {
            showError(getString(R.string.first_name_alert));
            return;
        } else if (TextUtils.isEmpty(lastName)) {
            showError(getString(R.string.second_name_alert));
            return;
        } else if (mBinding.spCityIdSpinner.getSelectedItemPosition() == 0) {
            showError(getString(R.string.city_alert));
            return;
        } else if (isDriver && mBinding.spCarIdSpinner.getSelectedItemPosition() == 0) {
            showError(getString(R.string.invalid_car_alert));
            return;
        } else if (isDriver && mBinding.spCarProdYearSpinner.getSelectedItemPosition() == 0) {
            showError(getString(R.string.invalid_prod_year_alert));
            return;
        } else if (isDriver && mBinding.spCarColorSpinner.getSelectedItemPosition() == 0) {
            showError(getString(R.string.invalid_car_color_alert));
            return;
        } else if (isDriver && TextUtils.isEmpty(carRegNumber)) {
            showError(getString(R.string.invalid_number_alrt));
            return;
        } else if (isDriver && mBinding.spIdentityDocumentSpinner.getSelectedItemPosition() == 0) {
            showError(getString(R.string.invalid_document_alert));
            return;
        } else if (isDriver && mIdProofList.isEmpty()) {
            showError(getString(R.string.invalid_id_icture_alert));
            return;
        } else if (isDriver && !mBinding.cbDriverDocAcceptance.isChecked()) {
            showError(getString(R.string.invalid_accpet_doc_alert));
            return;
        } else if (isDriver && !mBinding.cbDriverAcceptance.isChecked()) {
            showError(getString(R.string.invalid_accept_tc_alert));
            return;
        }

        LoadingDialog.show(this, getString(R.string.registreing_msg));
        // int cityId = ((City) mBinding.spCityIdSpinner.getSelectedItem()).getId()
        int cityId = mBinding.spCityIdSpinner.getSelectedItemPosition();
        if (isDriver) {
            eventBus.post(new RegisterEvent(mobileNumber, firstName, lastName, email, address, cityId, ((Car) mBinding.spCarIdSpinner.getSelectedItem()).getId(), (String) mBinding.spCarColorSpinner.getSelectedItem(), (String) mBinding.spCarProdYearSpinner.getSelectedItem(), carRegNumber));
        } else {
            eventBus.post(new RegisterEvent(mobileNumber, firstName, lastName, email, address, cityId));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveGetCarResult(CarDetailList carDetailList) {
        ArrayList<Car> mCarList = new ArrayList<>();
        mCarList.add(0, new Car(0, "Select Car"));
        if (carDetailList.status == 200) {
            mCarList.addAll(carDetailList.getCarList());
        }
        setCarAdapter(mCarList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveGetCityResult(CityList cityList) {
        ArrayList<City> mCarList = new ArrayList<>();
        mCarList.add(0, new City(0, "Select City"));
        if (cityList.status == 200) {
            mCarList.addAll(cityList.getCityList());
        }
        setCityAdapter(mCarList);
    }

    private void setCarAdapter(ArrayList<Car> mCarList) {

        CarListAdapter carAdapter = new CarListAdapter(this, R.layout.simple_spinner_item, mCarList);
        mBinding.spCarIdSpinner.setAdapter(carAdapter);
        count = count + 1;
    }

    private void setCityAdapter(ArrayList<City> cityArrayList) {

        CarListAdapter cityAdapter = new CarListAdapter(this, R.layout.simple_spinner_item, cityArrayList);
        mBinding.spCityIdSpinner.setAdapter(cityAdapter);
        count = count + 1;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignUpResultEvent(RiderResultEvent event) {
        LoadingDialog.dismiss();
        if (event.status == 200) {
            CommonUtils.rider = event.user;
            mPref.putString("rider_user", new Gson().toJson(event.user, Rider.class));
            mPref.putString("rider_token", event.token);
        }
        sentResultBack(event.status, -1);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignUpResultEvent(DriverResultEvent event) {
        if (event.status == 411) { // user is disabled or block
            mPref.putString("driver_token_1", event.token);
            eventBus.post(new DocumentUploadEvent((String) mBinding.spIdentityDocumentSpinner.getSelectedItem(), getDocumentForUpload(), false));
        } else {
            LoadingDialog.dismiss();
            sentResultBack(event.status, -1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDocumentResultReceived(CommonResponse event) {
        LoadingDialog.dismiss();

        if (!TextUtils.isEmpty(mFirstImagePath)) {
            deleteImages(this, mFirstImagePath);
        }
        if (!TextUtils.isEmpty(mSecondImagePath)) {
            deleteImages(this, mSecondImagePath);
        }
        sentResultBack(411, event.status);
    }

    private void sentResultBack(int status, int uploadCode) {
        Intent data = new Intent();
        data.putExtra("uploadCode", uploadCode);
        data.putExtra("statusCode", status);
        data.putExtra("profilePicPath", mProfilePicPath);
        setResult(RESULT_OK, data);
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // or get a single image only
            Image image = ImagePicker.getFirstImageOrNull(data);
            Uri destinationUri = Uri.fromFile(new File(getCacheDir(), System.currentTimeMillis() + ".jpg"));
            UCrop.of(Uri.fromFile(new File(image.getPath())), destinationUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(200, 200)
                    .start(SignUpSecondStepActivity.this);
        }
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            try {
                final Uri resultUri = UCrop.getOutput(data);
                if (resultUri == null)
                    return;
                if (mCurrentDoc == 0) {
                    mProfilePicPath = getFilePath(this, resultUri);
                    mBinding.profileImage.setImageURI(resultUri);
                } else if (mCurrentDoc == 1) {
                    mFirstImagePath = getFilePath(this, resultUri);
                    mBinding.ivIdOne.setImageURI(resultUri);
                } else {
                    mSecondImagePath = getFilePath(this, resultUri);
                    mBinding.ivIdTwo.setImageURI(resultUri);
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            try {
                throw UCrop.getError(data);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    private List<MultipartBody.Part> getDocumentForUpload() {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (String image : mIdProofList) {
            parts.add(createMultipartBody(image));
        }
        return parts;
    }

    private MultipartBody.Part createMultipartBody(String filePath) {
        File file = new File(filePath); // -- 1 --
        RequestBody requestBody = createRequestBody(file);
        return MultipartBody.Part.createFormData("filetoupload", file.getName(), requestBody);
    }

    private RequestBody createRequestBody(File file) {
        return RequestBody.create(MediaType.parse("image/*"), file);
    }

    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void showTermsAndCondition() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.app_name))
                .content(getTermsAndCondition())
                .positiveText(getString(R.string.alert_ok))
                .show();
    }

    private String getTermsAndCondition() {
        return "\n" +
                "SERVICE AGREEMENT \n" +
                "This Agreement is made & drawn up on this .day of.. .20 Agreement Ref. No. \n" +
                "\n" +
                "BETWEEN \n" +
                "\n" +
                "Nu Logi & AI Systems Pvt. Ltd., New Delhi,\"The First Party\" Transport Integrator& Technology Platform, Hereinafter called as Q-CAB QuiikMove. \n" +
                "AND MR./MRS./Company Name       SO/DO/WO/Proprietor/Partner/Director \n" +
                "\n" +
                "Address: \n" +
                "\n" +
                "\"The Second Party\"Cab Owner The Service Provider, Hereinafter called as Q-CAB Partner. \n" +
                "\n" +
                "It is mutually agreed as follows: \n" +
                "\n" +
                "\n" +
                "1. That the Second Party is entering into this agreement with the First Party with consent and knowledge of entire policy. \n" +
                "\n" +
                "2. That the Second Party, without any coercion is attaching his/her/their respective vehicle with the First Party and is willing to do the same by registering their said cab as mentioned in the Registration Form. \n" +
                "\n" +
                "3. That this agreement is valid for a period of One (1) year. \n" +
                "\n" +
                "4. That the Second Party submits the necessary documents as per mentioned in the Registration Form to the First Party, \n" +
                "\n" +
                "5. That the Second Party confirms that the First Party Q-CAB does not own or in any way control the vehicles used by the passenger. \n" +
                "\n" +
                "6. That the First Party shall not be held liable or responsible in any manner whatsoever for any insufficiency or deficiency of services provided by the Second Party. Also makes no representation or warranty to the Passenger of any type or form. \n" +
                "\n" +
                "7. That the First Party is not responsible or liable for any loss or damage to the Second Party & Passenger for wrong instruction, malfunction, IT-systems problem, telecommunication problem or other circumstances whether or not beyond out of control. \n" +
                "\n" +
                "8. That the Second Party must ensure comprehensive insurance without limitation for vehicle, driver, passenger & 3rd party etc. \n" +
                "\n" +
                "9. That the driver must have their own registration with the First Party. In case of driver change, the Second Party must intimate the same to the First Party. \n" +
                "\n" +
                "10. That the First Parties liabilities to the Second Party in any event include failure from the First Party shall be restricted to a maximum amount of INR 200 (Two Hundred Only) during the agreement period. \n" +
                "\n" +
                "11. That the Second Party shall ensure that the passenger pays the taxi fare, all other related fee, fee imposed by laws, toll charges etc. while travelling. \n" +
                "\n" +
                "12. That the Second Party should ensure arrival of taxi on or before the pickup time. \n" +
                "\n" +
                "13. That any payments related issues with passenger will be dealt by the First Party & on duty driver only. Also any other conflicts. \n" +
                "\n" +
                "14. That the goods left in car shall not be pilfered or tampered with and same should be reported by driver directly to the First Party.\n" +
                "\n" +
                "15. That the Second Party must ensure that the driver must know all prominent destinations & routes within & outside the operating city. \n" +
                "\n" +
                "16. That discounts of any type will be decided by the First Party & the Second Party shall agrees to the same without protest.\n" +
                "\n" +
                "17. That the driver is the sole responsibility of the Second Party and ensures excellent behaviour & discipline on duty.The driver must be polite, shall not drive rashly, must follow all traffic rules, not consume liquor/cigarate/bidi/gutkha etc. on duty. The First Party has the sole right to ask for the replacements of driver on required. \n" +
                "\n" +
                "18. That the condition of the vehicle must be good & clean always, must have GPS certified by the First Party without any temper and random audit may be carried out to check the same & must be in acceptable standards. \n" +
                "\n" +
                "19. That the Second Party ensures that the taxi is fully compliant with all laws, rules & regulations and maintains all local license, permits etc. The First Party will not be responsible for any of these irregularities. \n" +
                "\n" +
                "20. That any complaint by passenger will be dealt with seriously and enquiry will be done for the same. \n" +
                "\n" +
                "21. That the mobile device with app must be strictly switched on during duty hours. \n" +
                "\n" +
                "22. That the fare for the passenger will be at flat rate for any point of time & at any location of the operating city/town. The fare & Service Charge details will be provided to the Second Party in a separate Annexure. The Service Charge may change from time to time without any intimation as per law & local regulations. \n" +
                "\n" +
                "23. That the First Party reserves the right to terminate the Agreement in part or in whole at any point of time, with immediate effect, if services are found unsatisfactory and/or breach of any terms or due to any other reasons whatsoever. The Second Party can also terminate the Agreement with prior notice siting reasons. \n" +
                "\n" +
                "24. That Any dispute or differences arising out of this agreement, shall first be mutually resolved by both the parties, failing which help of law will be taken as per rules & regulations. The Jurisdiction will be at the location of operation or at Guwahati. \n" +
                "\n" +
                "\n" +
                "IN WITNESS WHEREOF the parties hereto have executed these presence on the day month and year first hereinabove mentioned. \n" +
                "For & on behalf of The First Party Q-CAB For & on behalf of The Second Party Cab Owner \n" +
                "\n" +
                "\n" +
                "\n";
    }


    public void onGetPictureCalled(int docNo) {
        mCurrentDoc = docNo;
        TedPermission.with(SignUpSecondStepActivity.this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }


    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            ImagePicker.create(SignUpSecondStepActivity.this)
                    .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                    .folderMode(true) // folder mode (false by default)
                    .toolbarFolderTitle(getString(R.string.picker_folder)) // folder selection title
                    .toolbarImageTitle(getString(R.string.picker_tap_select)) // image selection title
                    .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                    .single() // single mode
                    .limit(10) // max images can be selected (99 by default)
                    .showCamera(true) // show camera or not (true by default)
                    .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                    .theme(R.style.ImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                    .start();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };


}
