package com.innomalist.taxi.common.events;

public class RideDriverLocationEvent {

    public Double lat;

    public Double lng;

    public RideDriverLocationEvent(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
