package com.innomalist.taxi.common.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.R;
import com.innomalist.taxi.common.activities.signup.SignUpSecondStepActivity;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.databinding.ActivityDocumentUploadBinding;
import com.innomalist.taxi.driver.events.DocumentUploadEvent;
import com.innomalist.taxi.driver.models.CommonResponse;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.innomalist.taxi.common.utils.ImageUtils.deleteImages;
import static com.innomalist.taxi.common.utils.ImageUtils.getFilePath;

public class DocumentUploadActivity extends BaseActivity {

    private ActivityDocumentUploadBinding mBinding;
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE_REQUEST = 2;
    private int mCurrentDoc;
    private String firstImagePath;
    private String secondImagePath;
    private ArrayList<String> mIdProofList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_document_upload);
        initializeToolbar(getString(R.string.upload_document), true);
        mBinding.doc1.setOnClickListener(view -> {
            mCurrentDoc = 1;
            selectImage();
        });

        mBinding.doc2.setOnClickListener(view -> {
            mCurrentDoc = 2;
            selectImage();
        });

        mBinding.uploadDoc.setOnClickListener(view -> {
            mIdProofList = new ArrayList<>();
            if (!TextUtils.isEmpty(firstImagePath)) {
                mIdProofList.add(firstImagePath);
            }
            if (!TextUtils.isEmpty(secondImagePath)) {
                mIdProofList.add(secondImagePath);
            }

            if (mBinding.spIdentityDocumentSpinner.getSelectedItemPosition() == 0) {
                showToastMessage("Please select the document type ");
            } else if (mIdProofList.isEmpty()) {
                showToastMessage("You have not taken the document pictures");
                return;
            }
            LoadingDialog.show(DocumentUploadActivity.this, "Please wait...");
            eventBus.post(new DocumentUploadEvent((String) mBinding.spIdentityDocumentSpinner.getSelectedItem(), getDocumentForUpload(), true));
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDocumentResultReceived(CommonResponse event) {
        LoadingDialog.dismiss();
        if (event.status == 200) {
            if (!TextUtils.isEmpty(firstImagePath)) {
                deleteImages(this, firstImagePath);
            }
            if (!TextUtils.isEmpty(secondImagePath)) {
                deleteImages(this, secondImagePath);
            }
            setResult(RESULT_OK);
            finish();
        } else {
            showAlert(getString(R.string.unkwon_error_msg));
        }
    }

    private void selectImage() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        final CharSequence[] items = {"Take Photo", "Choose from Library",
                                "Cancel"};
                        new AlertDialog.Builder(DocumentUploadActivity.this)
                                .setTitle("Add Photo!")
                                .setItems(items, (dialog, item) -> {
                                    if (items[item].equals("Take Photo")) {
                                        cameraIntent();
                                    } else if (items[item].equals("Choose from Library")) {
                                        galleryIntent();
                                    } else if (items[item].equals("Cancel")) {
                                        dialog.dismiss();
                                    }
                                }).show();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                }).setPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setDeniedMessage("Please allowed storage permission. If not allowed, the documents would not be saved")
                .check();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_IMAGE_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                Uri selectedImage = data.getData();

                if (mCurrentDoc == 1) {
                    firstImagePath = getFilePath(this, selectedImage);
                    mBinding.doc1.setImageURI(selectedImage);
                } else if (mCurrentDoc == 2) {
                    secondImagePath = getFilePath(this, selectedImage);
                    mBinding.doc2.setImageURI(selectedImage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        File imgUrlFile = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            imgUrlFile.createNewFile();
            fo = new FileOutputStream(imgUrlFile);
            fo.write(bytes.toByteArray());
            fo.close();

            if (mCurrentDoc == 1) {
                firstImagePath = imgUrlFile.getAbsolutePath();
                mBinding.doc1.setImageBitmap(thumbnail);
            } else if (mCurrentDoc == 2) {
                secondImagePath = imgUrlFile.getAbsolutePath();
                mBinding.doc2.setImageBitmap(thumbnail);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    List<MultipartBody.Part> getDocumentForUpload() {
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (String image : mIdProofList) {
            parts.add(createMultipartBody(image));
        }
        return parts;
    }

    private MultipartBody.Part createMultipartBody(String filePath) {
        File file = new File(filePath); // -- 1 --
        RequestBody requestBody = createRequestBody(file);
        return MultipartBody.Part.createFormData("filetoupload", file.getName(), requestBody);
    }

    private RequestBody createRequestBody(File file) {
        return RequestBody.create(MediaType.parse("image/*"), file);
    }
}
