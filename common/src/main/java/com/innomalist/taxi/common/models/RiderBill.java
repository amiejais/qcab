package com.innomalist.taxi.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RiderBill  {

    @SerializedName("travelid")
    @Expose
    private int travelid;

    public int getTravelid() {
        return travelid;
    }

    public void setTravelid(int travelid) {
        this.travelid = travelid;
    }
}