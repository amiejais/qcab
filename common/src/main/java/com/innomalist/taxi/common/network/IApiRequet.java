package com.innomalist.taxi.common.network;

import android.app.DownloadManager;
import android.database.Observable;

import com.firebase.ui.auth.data.model.User;
import com.innomalist.taxi.common.events.AcceptRequestEvent;
import com.innomalist.taxi.common.events.ChangeStatusEvent;
import com.innomalist.taxi.common.events.ChangeStatusResultEvent;
import com.innomalist.taxi.common.events.ChargeAccountEvent;
import com.innomalist.taxi.common.events.ChargeAccountResultEvent;
import com.innomalist.taxi.common.events.EditProfileInfoEvent;
import com.innomalist.taxi.common.events.EditProfileInfoResultEvent;
import com.innomalist.taxi.common.events.GetAppVersionResultEvent;
import com.innomalist.taxi.common.events.GetStatusResultEvent;
import com.innomalist.taxi.common.events.GetTransactionsResultEvent;
import com.innomalist.taxi.common.events.GetTravelsResultEvent;
import com.innomalist.taxi.common.events.HideTravelEvent;
import com.innomalist.taxi.common.events.HideTravelResultEvent;
import com.innomalist.taxi.common.events.LoginEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.events.WriteComplaintEvent;
import com.innomalist.taxi.common.events.WriteComplaintResultEvent;
import com.innomalist.taxi.common.models.Car;
import com.innomalist.taxi.common.models.CarDetailList;
import com.innomalist.taxi.common.models.DriverResultEvent;
import com.innomalist.taxi.common.models.FileDownload;
import com.innomalist.taxi.common.models.RegisterEvent;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.common.models.RiderBill;
import com.innomalist.taxi.common.models.RiderResultEvent;
import com.innomalist.taxi.driver.events.AutoAcceptEvent;
import com.innomalist.taxi.driver.events.DeleteHomeLocationRequestEvent;
import com.innomalist.taxi.driver.events.DriverHomeLocationResult;
import com.innomalist.taxi.driver.events.RideAcceptNewEvent;
import com.innomalist.taxi.driver.events.SerDriverServicesEvent;
import com.innomalist.taxi.driver.events.ServiceFinishEvent;
import com.innomalist.taxi.driver.events.ServiceFinishResultEvent;
import com.innomalist.taxi.driver.models.CityList;
import com.innomalist.taxi.driver.models.Common;
import com.innomalist.taxi.driver.models.DriverServiceResponse;
import com.innomalist.taxi.driver.events.GetDriverServiceEvent;
import com.innomalist.taxi.driver.events.GetRequestsResultEvent;
import com.innomalist.taxi.driver.events.GetStatisticsEvent;
import com.innomalist.taxi.driver.events.GetStatisticsResultEvent;
import com.innomalist.taxi.driver.events.PaymentRequestResultEvent;
import com.innomalist.taxi.driver.events.RejectRequestEvent;
import com.innomalist.taxi.driver.events.SetDriverHomeLocationEvent;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.events.AddCouponRequestEvent;
import com.innomalist.taxi.rider.events.AddCouponResultEvent;
import com.innomalist.taxi.rider.events.ApplyCouponRequestEvent;
import com.innomalist.taxi.rider.events.ApplyCouponResultEvent;
import com.innomalist.taxi.rider.events.CRUDAddressRequestEvent;
import com.innomalist.taxi.rider.events.CRUDAddressResultEvent;
import com.innomalist.taxi.rider.events.CalculateFareRequestEvent;
import com.innomalist.taxi.rider.events.CalculateFareResultEvent;
import com.innomalist.taxi.rider.events.GetCouponsResultEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationEvent;
import com.innomalist.taxi.rider.events.GetDriversLocationResultEvent;
import com.innomalist.taxi.rider.events.GetPromotionsResultEvent;
import com.innomalist.taxi.rider.events.GoogleLocationDataBean;
import com.innomalist.taxi.rider.events.ReviewDriverResultEvent;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by KNPX0678 on 2018-03-01.
 */

public interface IApiRequet {

    String BASE_URL = "http://103.127.28.145:8080/";

    String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/";

    @GET("place/details/json")
    Call<GoogleLocationDataBean> getLatLongUsingLocId(@Query("placeid") String placeid, @Query("key") String key);


    @POST("driver_register")
    Call<DriverResultEvent> registerDriver(@Body RegisterEvent loginRO);

    @POST("rider_register")
    Call<RiderResultEvent> registerRider(@Body RegisterEvent loginRO);

    @Multipart
    @POST("client/uploadfiles")
    Call<CommonResponse> uploadDocument(@Part List<MultipartBody.Part> imagePartList, @Part("type") RequestBody type);

    @POST("driver_login")
    Call<DriverResultEvent> loginDriver(@Body LoginEvent loginRO);

    @POST("rider_login")
    Call<RiderResultEvent> loginRider(@Body LoginEvent loginRO);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("bill/rider")
    Call<FileDownload> getBillInfo(@Body RiderBill travelId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @GET("{fileName}")
    Call<ResponseBody> downloadBill(@Path(value = "fileName", encoded = true) String fileName);

    @POST("client/changeStatus")
    Call<ChangeStatusResultEvent> changeDriverStatus(@Body ChangeStatusEvent loginRO);

    @GET("client/getStatus")
    Call<GetStatusResultEvent> getUnfinishedTravel();

    @POST("client/calculateFare")
    Call<CalculateFareResultEvent> calculateFare(@Body CalculateFareRequestEvent event);

    @POST("client/notificationPlayerId")
    Call<CommonResponse> sendNotePlayerId(@Body NotificationPlayerId event);

    @GET("client/getTravels")
    Call<GetTravelsResultEvent> getTravels();

    @GET("client/getTransactions")
    Call<GetTransactionsResultEvent> getTransactions();

    @GET("client/getVersions")
    Call<GetAppVersionResultEvent> getAppVersions();

    @GET("client/getPromotions")
    Call<GetPromotionsResultEvent> getPromotions();

    @GET("client/getCoupons")
    Call<GetCouponsResultEvent> getCoupons();

    @POST("client/addCoupon")
    Call<AddCouponResultEvent> addCoupons(@Body AddCouponRequestEvent event);

    @POST("client/applyCoupon")
    Call<ApplyCouponResultEvent> applyCoupons(@Body ApplyCouponRequestEvent event);

    @POST("client/chargeAccount")
    Call<ChargeAccountResultEvent> chargeAccount(@Body ChargeAccountEvent event);

    @GET("client/requestPayment")
    Call<PaymentRequestResultEvent> getDriverRequestPayment();

    @POST("client/getStats")
    Call<GetStatisticsResultEvent> getStats(@Body GetStatisticsEvent event);

    @GET("client/getRequests")
    Call<GetRequestsResultEvent> getRequests();

    @POST("client/editProfile")
    Call<EditProfileInfoResultEvent> editProfile(@Body EditProfileInfoEvent event);

    @POST("client/writeComplaint")
    Call<WriteComplaintResultEvent> writeComplaint(@Body WriteComplaintEvent event);

    @POST("client/hideTravel")
    Call<HideTravelResultEvent> hideTravel(@Body HideTravelEvent event);

    @POST("client/reviewDriver")
    Call<ReviewDriverResultEvent> reviewDriver(@Body ReviewDriverEvent event);

    @POST("client/crudAddress")
    Call<CRUDAddressResultEvent> crudAddress(@Body CRUDAddressRequestEvent event);

    @POST("client/getDriversLocation")
    Call<GetDriversLocationResultEvent> getDriversLocation(@Body GetDriversLocationEvent event);

    @POST("client/insertDriverReject")
    Call<CommonResponse> rejectRequest(@Body RejectRequestEvent event);

    @GET("client/requestDriverHomeLocation")
    Call<CommonResponse> driverHomeLocationRequest();

    @GET("client/getDriverHomeLocRequest")
    Call<DriverHomeLocationResult> getDriverHomeLocationRequestTime();

    @POST("client/addDriverHomeLocation")
    Call<CommonResponse> setDriverHomeLocation(@Body SetDriverHomeLocationEvent event);

    @POST("client/delDriverHomeLocationReq")
    Call<CommonResponse> deleteDriverHomeLocationRequest(@Body DeleteHomeLocationRequestEvent event);

    @POST("client/getDriverAllowedServices")
    Call<DriverServiceResponse> getDriverServices(@Body GetDriverServiceEvent event);

    @POST("client/updateDriverServices")
    Call<CommonResponse> setDriverServices(@Body SerDriverServicesEvent event);

    @GET("client/getCars")
    Call<CarDetailList> getCarDetails();

    @GET("client/getCities")
    Call<CityList> getCityDetails();

    @POST("client/startTravel")
    Call<CommonResponse> startTravel();

    @POST("client/cancelTravel")
    Call<CommonResponse> cancelTravel();

    @POST("client/finishedTaxi")
    Call<ServiceFinishResultEvent> finishTaxi(@Body ServiceFinishEvent event);

    @POST("client/driverAccepted")
    Call<RideAcceptNewEvent> driverAccepted(@Body AcceptRequestEvent event);

    @POST("client/driverAccepted")
    Call<RideAcceptNewEvent> driverAccepted1(@Body AutoAcceptEvent event);

}
