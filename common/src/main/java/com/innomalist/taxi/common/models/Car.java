package com.innomalist.taxi.common.models;

import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.driver.models.Common;

public class Car extends Common {

    @SerializedName("media")
    private Media media;

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    public Car(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}