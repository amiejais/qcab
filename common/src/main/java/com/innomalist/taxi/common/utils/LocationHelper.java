package com.innomalist.taxi.common.utils;

import android.location.Location;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

public class LocationHelper {

    public static int distFrom(double lat1, double lng1, double lat2, double lng2) {
        Location locationA = new Location("LocationA");
        locationA.setLatitude(lat1);
        locationA.setLongitude(lng1);
        Location locationB = new Location("LocationB");
        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);
        locationA.distanceTo(locationB);

        return (int) locationA.distanceTo(locationB);
    }

    public static double[] LatLngToDoubleArray(LatLng position) {
        return new double[]{position.latitude, position.longitude};
    }

    public static LatLng DoubleArrayToLatLng(double[] position) {
        return new LatLng(position[0], position[1]);
    }
}
