package com.innomalist.taxi.common.events;

public class AcceptRequestEvent {
    public long travelId;

    public AcceptRequestEvent(long travelId){
        this.travelId = travelId;
    }
    /*public Double cost;
    public AcceptRequestEvent(long travelId, Double cost){
        this.travelId = travelId;
        this.cost = cost;
    }*/
}
