package com.innomalist.taxi.common.network;


import android.text.TextUtils;
import android.util.Log;

import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.network.event.ApiErrorEvent;
import com.innomalist.taxi.common.network.event.ApiErrorWithMessageEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Use this class to have a callback which can be used for the api calls in {@link }.
 * Such a callback can be invalidated to not notify its caller about the api response.
 * Furthermore it handles finishing the request after the caller has handled the response.
 */
public class ApiCallback<T extends AbstractApiResponse> implements Callback<T> {

    /**
     * Indicates if the callback was invalidated.
     */
    private boolean isInvalidated;

    /**
     * The tag of the request which uses this callback.
     */
    private final String requestTag;

    /**
     * Creates an {@link ApiCallback} with the passed request tag. The tag is used to finish
     * the request after the response has been handled.
     *
     * @param requestTag The tag of the request which uses this callback.
     */
    public ApiCallback(String requestTag) {
        isInvalidated = false;
        this.requestTag = requestTag;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (isInvalidated || call.isCanceled()) {
            return;
        }
        T result = response.body();
        if (result == null) {
            return;
        }
        Log.e("TAG", " requestTag : " + requestTag + " & Status: " + result.status);
        if (response.isSuccessful()) {
            result.setRequestTag(requestTag);
            EventBus.getDefault().post(result);
        } else {
            EventBus.getDefault().post(
                    new ApiErrorWithMessageEvent(requestTag, "Server not available."));


            //TODO: If the Network response code is not between (200..300) and error body is
            //similar to {@link AbstractApiResponse} then use below commented code.
           /* try {
                AbstractApiResponse abstractApiResponse = (AbstractApiResponse) ApiClient.getRetrofit().responseBodyConverter(
                        AbstractApiResponse.class,
                        AbstractApiResponse.class.getAnnotations())
                        .convert(response.errorBody());
                // Do error handling here

                EventBus.getDefault().post(
                        new ApiErrorWithMessageEvent(requestTag, abstractApiResponse.getMessage()));

            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        LoadingDialog.dismiss();
        if (!call.isCanceled() && !isInvalidated) {
            EventBus.getDefault().post(new ApiErrorEvent(requestTag, t));
        }
    }

    /**
     * Invalidates this callback. This means the caller doesn't want to be called back anymore.
     */
    public void invalidate() {
        isInvalidated = true;
    }


    /**
     * This is for callbacks which extend ApiCallback and want to modify the response before it is
     * delivered to the caller. It is bit different from the interceptors as it allows to implement
     * this method and change the response.
     *
     * @param result The api response.
     */
    @SuppressWarnings("UnusedParameters")
    protected void modifyResponseBeforeDelivery(T result) {
        // Do nothing here. Only for subclasses.
    }

    /**
     * Call this method if No internet connection or other use.
     *
     * @param resultMsgUser User defined messages.
     */
    public void postUnexpectedError(String resultMsgUser) {
        EventBus.getDefault().post(new ApiErrorWithMessageEvent(requestTag, resultMsgUser));
    }
}
