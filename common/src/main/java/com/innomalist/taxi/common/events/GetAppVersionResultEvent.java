package com.innomalist.taxi.common.events;

import com.innomalist.taxi.common.network.AbstractApiResponse;

public class GetAppVersionResultEvent extends AbstractApiResponse {
    public VersionDetail data;
}
