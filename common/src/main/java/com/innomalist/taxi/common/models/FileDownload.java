package com.innomalist.taxi.common.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.innomalist.taxi.common.network.AbstractApiResponse;

public class FileDownload extends AbstractApiResponse {

    @SerializedName("file")
    @Expose
    public String file;

    public int getStatus() {
        return status;
    }

    public String getFile() {
        return file;
    }
}