package com.innomalist.taxi.common.events;

public class WriteComplaintEvent {
    public long travelId;
    public String subject;
    public String content;

    public WriteComplaintEvent(long travelId, String subject, String content) {
        this.travelId = travelId;
        this.subject = subject;
        this.content = content;
    }
}
