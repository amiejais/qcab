package com.innomalist.taxi.common.events;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.network.AbstractApiResponse;
import com.innomalist.taxi.common.utils.LatLngDeserializer;
import com.innomalist.taxi.common.utils.ServerResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetTravelsResultEvent extends AbstractApiResponse {
    public ArrayList<Travel> data;
   /* public GetTravelsResultEvent(int response, String travelEntities) {

        Type type = new TypeToken<List<Travel>>() {
        }.getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LatLng.class, new LatLngDeserializer());
        Gson customGson = gsonBuilder.create();
        this.data = customGson.fromJson(travelEntities, type);
    }*/
}
