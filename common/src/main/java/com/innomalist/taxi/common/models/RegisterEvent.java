package com.innomalist.taxi.common.models;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

public class RegisterEvent {

    private final Long mobile;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String address;
    private final int city_id;

    private int car_id = 0;
    private int car_production_year = 0;
    private String car_color = null;
    private String car_plate = null;
    private String player_id = null;

    public RegisterEvent(String mobile, String firstName, String lastName, String email, String address, int city) {
        this.mobile = Long.valueOf(mobile);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.city_id = city;
    }


    public RegisterEvent(String mobile, String firstName, String lastName, String email, String address, int city, int pCarId, String carColor, String carProdYear, String carRegNumber) {
        this.mobile = Long.valueOf(mobile);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.city_id = city;

        car_id = pCarId;
        car_color = carColor;
        car_production_year = Integer.parseInt(carProdYear);
        car_plate = carRegNumber;
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        player_id = status.getSubscriptionStatus().getUserId();
    }

}
