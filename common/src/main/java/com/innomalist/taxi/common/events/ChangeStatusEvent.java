package com.innomalist.taxi.common.events;

public class ChangeStatusEvent {
    public Status statusCode;

    public ChangeStatusEvent(Status statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusCode(Status statusCode) {
        this.statusCode = statusCode;
    }

    public enum Status {
        ONLINE("online"),
        OFFLINE("offline");
        private String value;

        Status(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


}
