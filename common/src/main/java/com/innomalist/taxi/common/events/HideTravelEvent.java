package com.innomalist.taxi.common.events;

import com.innomalist.taxi.common.utils.ServerResponse;

public class HideTravelEvent {
    public Integer travelId;

    public HideTravelEvent(Integer travelId) {
        this.travelId = travelId;
    }
}
