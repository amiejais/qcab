package com.innomalist.taxi.common.models;

import com.innomalist.taxi.common.network.AbstractApiResponse;

public class RiderResultEvent extends AbstractApiResponse {
    public Rider user;
    public String token;

    public Rider getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }
}
