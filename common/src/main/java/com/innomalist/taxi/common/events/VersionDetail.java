package com.innomalist.taxi.common.events;

public class VersionDetail {

    public int driver_min_ver_android;
    public int driver_min_ver_ios;
    public int rider_min_ver_android;
    public int rider_min_ver_ios;
}
