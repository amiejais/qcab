package com.innomalist.taxi.common.events;

public class LoginEvent {
    public long mobile;
    public int version;

    public LoginEvent(long mobile, int version){
        this.mobile = mobile;
        this.version = version;
    }
}
