package com.innomalist.taxi.common.events;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.innomalist.taxi.common.models.Transaction;
import com.innomalist.taxi.common.network.AbstractApiResponse;
import com.innomalist.taxi.common.utils.ServerResponse;

import java.lang.reflect.Type;
import java.util.List;

public class GetTransactionsResultEvent extends AbstractApiResponse {
    public List<Transaction> data;

    /*public GetTransactionsResultEvent(){
        super(ServerResponse.REQUEST_TIMEOUT);
    }

    public GetTransactionsResultEvent(Object... args) {
        super(args);
        if(response != ServerResponse.OK)
            return;
        Type type = new TypeToken<List<Transaction>>() {}.getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        //gsonBuilder.registerTypeAdapter(LatLng.class, new LatLngDeserializer());

        Gson customGson = gsonBuilder.create();
        this.data = customGson.fromJson(args[1].toString(),type);
    }*/
}
