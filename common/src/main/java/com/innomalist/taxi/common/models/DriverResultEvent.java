package com.innomalist.taxi.common.models;

import com.innomalist.taxi.common.network.AbstractApiResponse;

public class DriverResultEvent extends AbstractApiResponse {
    public Driver user;
    public String token;

    public Driver getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }
}
