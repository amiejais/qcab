package com.innomalist.taxi.common.models;

import com.innomalist.taxi.common.network.AbstractApiResponse;

import java.util.List;

public class CarDetailList extends AbstractApiResponse {
    private List<Car> data;

    public List<Car> getCarList() {
        return data;
    }
}
