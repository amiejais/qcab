package com.innomalist.taxi.common.models;

public class DriverInfo {
    public Driver driver;
    public Double distance;
    public Integer duration;
    public Travel travel;

    public DriverInfo(Driver driver, int distance, int duration, Travel cost) {
        this.driver = driver;
        this.distance = (double) distance / 1000;
        this.duration = duration / 60;
        this.travel = cost;
        travel.setDriver(driver);
    }
}
