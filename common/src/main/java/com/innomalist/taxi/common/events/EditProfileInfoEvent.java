package com.innomalist.taxi.common.events;

public class EditProfileInfoEvent {
    public String userInfo;

    public EditProfileInfoEvent(String userInfo) {
        this.userInfo = userInfo;
    }
}
