package com.innomalist.taxi.common.models;

import java.io.Serializable;

public class Request implements Serializable {
    public TravelSerializable travel;
    public Double distance;
    public Double fromDriver;
    public Double cost;
    public boolean nearest;

    public Request(TravelSerializable travel, Integer distance, Integer fromDriver, Double cost, boolean isNearest) {
        this.travel = travel;
        this.distance = distance.doubleValue() / 1000;
        this.fromDriver = fromDriver.doubleValue() / 1000;
        this.nearest = isNearest;
        this.cost = cost;
    }

    public Request(TravelSerializable travel, Integer distance, Integer fromDriver, Double cost) {
        this.travel = travel;
        this.distance = distance.doubleValue() / 1000;
        this.fromDriver = fromDriver.doubleValue() / 1000;
        this.cost = cost;
    }

    public Request(TravelSerializable travel, Integer distance, Double cost) {
        this.travel = travel;
        this.distance = distance.doubleValue() / 1000;
        this.cost = cost;
    }
}
