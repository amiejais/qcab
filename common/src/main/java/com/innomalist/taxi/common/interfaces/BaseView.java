package com.innomalist.taxi.common.interfaces;

public interface BaseView {

    void showProgress();

    void hideProgress();

}
