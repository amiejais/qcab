package com.innomalist.taxi.driver.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.events.BackgroundServiceStartedEvent;
import com.innomalist.taxi.common.events.ConnectEvent;
import com.innomalist.taxi.common.events.ConnectResultEvent;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.services.DriverService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class DriverBaseActivity extends BaseActivity {
    MyPreferenceManager SP;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SP = MyPreferenceManager.getInstance(getApplicationContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        boolean isServiceRunning = isMyServiceRunning(DriverService.class);
        Log.e("TAg", " DriverBaseActivity onStart " + isServiceRunning);
        if (!isServiceRunning)
            startService(new Intent(this, DriverService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAg", " DriverBaseActivity onResume");
    }

    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        Log.e("TAg", " DriverBaseActivity onServiceStarted");
        tryConnect();
    }

    public void tryConnect() {
        Log.e("TAg", " DriverBaseActivity tryConnect");
        String token = SP.getString("driver_token", null);
        if (token != null && !token.isEmpty()) {
            eventBus.post(new ConnectEvent(token));
           /* if (connectionProgressDialog == null) {
                connectionProgressDialog = new MaterialDialog.Builder(this)
                        .title(getString(com.innomalist.taxi.common.R.string.connection_dialog_title))
                        .content(R.string.event_reconnecting)
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
            } else {
                connectionProgressDialog.setContent(R.string.event_reconnecting);
            }*/
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        Log.e("TAg", " DriverBaseActivity onConnectedResult");
        if (connectionProgressDialog != null)
            connectionProgressDialog.dismiss();
        onReconnected();
    }

    public void onReconnected() {

    }
}