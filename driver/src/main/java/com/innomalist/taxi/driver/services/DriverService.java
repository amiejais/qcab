package com.innomalist.taxi.driver.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.innomalist.taxi.common.events.AcceptRequestEvent;
import com.innomalist.taxi.common.events.BackgroundServiceStartedEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageResultEvent;
import com.innomalist.taxi.common.events.ChangeStatusEvent;
import com.innomalist.taxi.common.events.ChangeStatusResultEvent;
import com.innomalist.taxi.common.events.ChargeAccountEvent;
import com.innomalist.taxi.common.events.ChargeAccountResultEvent;
import com.innomalist.taxi.common.events.ConnectEvent;
import com.innomalist.taxi.common.events.ConnectResultEvent;
import com.innomalist.taxi.common.events.EditProfileInfoEvent;
import com.innomalist.taxi.common.events.EditProfileInfoResultEvent;
import com.innomalist.taxi.common.events.GetAppVersionResultEvent;
import com.innomalist.taxi.common.events.GetStatusEvent;
import com.innomalist.taxi.common.events.GetStatusResultEvent;
import com.innomalist.taxi.common.events.GetTransactionsRequestEvent;
import com.innomalist.taxi.common.events.GetTransactionsResultEvent;
import com.innomalist.taxi.common.events.GetTravelsEvent;
import com.innomalist.taxi.common.events.GetTravelsResultEvent;
import com.innomalist.taxi.common.events.HideTravelEvent;
import com.innomalist.taxi.common.events.HideTravelResultEvent;
import com.innomalist.taxi.common.events.LoginEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.events.ProfileInfoChangedEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestResultEvent;
import com.innomalist.taxi.common.events.ServiceCancelEvent;
import com.innomalist.taxi.common.events.ServiceCancelResultEvent;
import com.innomalist.taxi.common.events.SocketConnectionEvent;
import com.innomalist.taxi.common.events.WriteComplaintEvent;
import com.innomalist.taxi.common.events.WriteComplaintResultEvent;
import com.innomalist.taxi.common.models.AppVersion;
import com.innomalist.taxi.common.models.CarDetailList;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.models.DriverResultEvent;
import com.innomalist.taxi.common.models.RegisterEvent;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.common.network.ApiCallback;
import com.innomalist.taxi.common.network.RetrofitClient;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.common.utils.ServerResponse;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.events.AutoAcceptEvent;
import com.innomalist.taxi.driver.events.CancelRequestEvent;
import com.innomalist.taxi.driver.events.ChangeHeaderImageEvent;
import com.innomalist.taxi.driver.events.ChangeHeaderImageResultEvent;
import com.innomalist.taxi.driver.events.DeleteHomeLocationRequestEvent;
import com.innomalist.taxi.driver.events.DocumentUploadEvent;
import com.innomalist.taxi.driver.events.DriverHomeLocationRequestEvent;
import com.innomalist.taxi.driver.events.DriverHomeLocationResult;
import com.innomalist.taxi.driver.events.GetCityEvent;
import com.innomalist.taxi.driver.events.RideAcceptNewEvent;
import com.innomalist.taxi.driver.events.SerDriverServicesEvent;
import com.innomalist.taxi.driver.models.CityList;
import com.innomalist.taxi.driver.models.DriverServiceResponse;
import com.innomalist.taxi.driver.events.GetDriverServiceEvent;
import com.innomalist.taxi.driver.events.GetRequestsRequestEvent;
import com.innomalist.taxi.driver.events.GetRequestsResultEvent;
import com.innomalist.taxi.driver.events.GetStatisticsEvent;
import com.innomalist.taxi.driver.events.GetStatisticsResultEvent;
import com.innomalist.taxi.driver.events.LocationChangedEvent;
import com.innomalist.taxi.driver.events.PaymentRequestEvent;
import com.innomalist.taxi.driver.events.PaymentRequestResultEvent;
import com.innomalist.taxi.driver.events.RejectRequestEvent;
import com.innomalist.taxi.driver.events.RequestReceivedEvent;
import com.innomalist.taxi.driver.events.RiderAcceptedEvent;
import com.innomalist.taxi.driver.events.SendTravelInfoEvent;
import com.innomalist.taxi.driver.events.ServiceFinishEvent;
import com.innomalist.taxi.driver.events.ServiceFinishResultEvent;
import com.innomalist.taxi.driver.events.ServiceInLocationEvent;
import com.innomalist.taxi.driver.events.ServiceStartEvent;
import com.innomalist.taxi.driver.events.SetDriverHomeLocationEvent;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.rider.events.GetCarEvent;
import com.innomalist.taxi.rider.events.ReviewDriverResultEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.RequestBody;


public class DriverService extends Service {

    private MyPreferenceManager mPref;
    final static String DRIVER_ACCEPTED = "driverAccepted";

    public static final String SERVICE_TO_ACTIVITY_ACTION = "com.qcab.driver_ERVICE_TO_ACTIVITY";

    private Socket socket;
    private EventBus eventBus = EventBus.getDefault();

    //Provides access to the Fused Location Provider API.
    private FusedLocationProviderClient mFusedLocationClient;

    //Stores parameters for data to the FusedLocationProviderApi.
    private LocationRequest mLocationRequest;

    //Callback for Location events.
    private LocationCallback mLocationCallback;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL = 4000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    private static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2;

    /**
     * The max time before batched results are delivered by location services. Results may be
     * delivered sooner than this interval.
     */
    private static final long MAX_WAIT_TIME = UPDATE_INTERVAL;
    private CountDownTimer countDownTimer = null;


    @Subscribe
    public void connectSocket(ConnectEvent event) {
        try {
            IO.Options options = new IO.Options();
            options.query = "token=" + event.token;
            socket = IO.socket(getString(R.string.server_address), options);
            socket.on(Socket.EVENT_CONNECT, args -> {
                eventBus.post(new ConnectResultEvent(ServerResponse.OK.getValue()));
                eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT));
            })
                    .on(Socket.EVENT_DISCONNECT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_DISCONNECT)))
                    .on(Socket.EVENT_CONNECTING, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECTING)))
                    .on(Socket.EVENT_CONNECT_ERROR, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT_ERROR)))
                    .on(Socket.EVENT_CONNECT_TIMEOUT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_CONNECT_TIMEOUT)))
                    .on(Socket.EVENT_RECONNECT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT)))
                    .on(Socket.EVENT_RECONNECTING, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECTING)))
                    .on(Socket.EVENT_RECONNECT_ATTEMPT, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_ATTEMPT)))
                    .on(Socket.EVENT_RECONNECT_ERROR, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_ERROR)))
                    .on(Socket.EVENT_RECONNECT_FAILED, args -> eventBus.post(new SocketConnectionEvent(Socket.EVENT_RECONNECT_FAILED)))
                    .on("error", args -> {
                        try {
                            JSONObject obj = new JSONObject(args[0].toString());
                            eventBus.post(new ConnectResultEvent(ServerResponse.UNKNOWN_ERROR.getValue(), obj.getString("message")));
                        } catch (JSONException c) {
                            eventBus.post(new ConnectResultEvent(ServerResponse.UNKNOWN_ERROR.getValue(), args[0].toString()));
                        }
                    })
                    .on("requestReceived", args -> {
                        try {
                            eventBus.post(new RequestReceivedEvent(args[0].toString(), (Integer) args[1], (Integer) args[2], Double.valueOf(args[3].toString()), (boolean) args[4]));
                            /*if (MainActivity.isInBackGround.get()) {
                                sendNotification();
                            }*/
                        } catch (ArrayIndexOutOfBoundsException c) {
                            eventBus.post(new RequestReceivedEvent(args[0].toString(), (Integer) args[1], (Integer) args[2], Double.valueOf(args[3].toString())));

                            c.printStackTrace();
                        }
                    })
                    .on("changeRequestReceived", args -> {
                        try {
                            eventBus.post(new RequestReceivedEvent(args[0].toString(), (Integer) args[1], Double.valueOf(args[2].toString())));
                            /*if (MainActivity.isInBackGround.get()) {
                                sendNotification();
                            }*/
                        } catch (ArrayIndexOutOfBoundsException c) {
                            eventBus.post(new RequestReceivedEvent(args[0].toString(), (Integer) args[1], Double.valueOf(args[2].toString())));
                            c.printStackTrace();
                        }
                    })
                    .on("riderAccepted", args -> {
                        try {
                            Log.e("TAG", "riderAccepted:  3");
                            eventBus.post(new RiderAcceptedEvent(args));
                            Log.e("TAG", "riderAccepted:  4");
                        } catch (Exception c) {
                            Log.e("TAG", "riderAccepted:  5");
                            c.printStackTrace();
                        }
                    })
                    .on("cancelRequest", args -> eventBus.post(new CancelRequestEvent(args)))
                    .on("driverInfoChanged", args -> {
                        MyPreferenceManager SP = new MyPreferenceManager(getApplicationContext());
                        SP.putString("driver_user", args[0].toString());
                        CommonUtils.driver = new Gson().fromJson(args[0].toString(), Driver.class);
                        eventBus.postSticky(new ProfileInfoChangedEvent());
                    })
                    .on("cancelTravel", args -> eventBus.post(new ServiceCancelResultEvent(200)));
            socket.connect();
        } catch (Exception c) {
            Log.e("Connect Socket", c.getMessage());
        }

    }

    @Subscribe
    public void register(RegisterEvent event) {
        ApiCallback<DriverResultEvent> callback = new ApiCallback<>("DriverRegister");
        RetrofitClient.getClient("").registerDriver(event).enqueue(callback);
    }

    @Subscribe
    public void login(LoginEvent event) {
        ApiCallback<DriverResultEvent> callback = new ApiCallback<>("DriverLogin");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).loginDriver(event).enqueue(callback);
    }

    @Subscribe
    public void getVersionStatus(AppVersion event) {
        Log.e("TAg", "getVersionStatus");
        ApiCallback<GetAppVersionResultEvent> callback = new ApiCallback<>("AppVersions");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getAppVersions().enqueue(callback);
    }

    @Subscribe
    public void getStatus(GetStatusEvent event) {
        ApiCallback<GetStatusResultEvent> callback = new ApiCallback<>("UnfinishedDriverTravel");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getUnfinishedTravel().enqueue(callback);
    }


    @Subscribe
    public void FinishTravel(final ServiceFinishEvent event) {
        ApiCallback<ServiceFinishResultEvent> callback = new ApiCallback<>("finishTravel");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).finishTaxi(event).enqueue(callback);

        //socket.emit("finishedTaxi", event.service_id, event.duration, event.destinationPoint, event.dropOffLocation, event.distance, event.log, (Ack) args -> eventBus.post(new ServiceFinishResultEvent((int) args[0], (boolean) args[1], Float.parseFloat(args[2].toString()))));
    }

    @Subscribe
    public void editProfile(final EditProfileInfoEvent event) {
        ApiCallback<EditProfileInfoResultEvent> callback = new ApiCallback<>("EDIT_PROFILE");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).editProfile(event).enqueue(callback);
    }

    @Subscribe
    public void reviewDriver(ReviewDriverEvent event) {
        ApiCallback<ReviewDriverResultEvent> callback = new ApiCallback<>("reviewDriver");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).reviewDriver(event).enqueue(callback);
    }

    @Subscribe
    public void driverHomeLocation(final DriverHomeLocationRequestEvent event) {
        if (event.getRequestTag().equalsIgnoreCase("driverHomeLocationRequestTime")) {
            ApiCallback<DriverHomeLocationResult> callback = new ApiCallback<>("driverHomeLocationRequestTime");
            RetrofitClient.getClient(mPref.getString("driver_token", null)).getDriverHomeLocationRequestTime().enqueue(callback);
        } else {
            ApiCallback<CommonResponse> callback = new ApiCallback<>("driverHomeLocationRequest");
            RetrofitClient.getClient(mPref.getString("driver_token", null)).driverHomeLocationRequest().enqueue(callback);
        }
    }

    @Subscribe
    public void setDriverHomeLocation(final SetDriverHomeLocationEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("SetDriverHomeLocation");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).setDriverHomeLocation(event).enqueue(callback);
    }

    @Subscribe
    public void deleteDriverHomeLocationRequest(final DeleteHomeLocationRequestEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("DeleteHomeLocationRequest");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).deleteDriverHomeLocationRequest(event).enqueue(callback);
    }

    @Subscribe
    public void getDriverServices(final GetDriverServiceEvent event) {
        ApiCallback<DriverServiceResponse> callback = new ApiCallback<>("getDriverServices");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getDriverServices(event).enqueue(callback);
    }


    @Subscribe
    public void getDriverServices(final SerDriverServicesEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("setDriverServices");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).setDriverServices(event).enqueue(callback);
    }

    @Subscribe
    public void PaymentRequested(final PaymentRequestEvent event) {
        ApiCallback<PaymentRequestResultEvent> callback = new ApiCallback<>("RequestPayment");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getDriverRequestPayment().enqueue(callback);
    }

    @Subscribe
    public void notificationPlayerId(NotificationPlayerId event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("notificationPlayerId");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).sendNotePlayerId(event).enqueue(callback);
    }

    @Subscribe
    public void acceptOrder(AcceptRequestEvent event) {
        ApiCallback<RideAcceptNewEvent> callback = new ApiCallback<>("driverAccepted");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).driverAccepted(event).enqueue(callback);

        //socket.emit(DRIVER_ACCEPTED, event.travelId, (Ack) args -> eventBus.post(new RideAcceptNewEvent(args)));
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    @Subscribe
    public void createTimerForAutoAccept(AutoAcceptEvent acceptEvent) {
        Log.e("TAG", "createTimerForAutoAccept  called with  Value ");
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        } else {
            countDownTimer = new CountDownTimer(50000, 1000) {

                public void onTick(long millisUntilFinished) {
                    Log.e("TAG", "createTimerForAutoAccept  onTick Left " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    Log.e("TAG", "createTimerForAutoAccept  onFinish called with Value " + " id: " + acceptEvent.travelId);
                    ApiCallback<RideAcceptNewEvent> callback = new ApiCallback<>("driverAccepted");
                    RetrofitClient.getClient(mPref.getString("driver_token", null)).driverAccepted1(acceptEvent).enqueue(callback);

                    //socket.emit(DRIVER_ACCEPTED, acceptEvent.travelId, (Ack) args -> eventBus.post(new RideAcceptNewEvent(args)));
                }
            };
            countDownTimer.start();
        }

    }

    @Subscribe
    public void rejectRequest(RejectRequestEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("rejectRequest");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).rejectRequest(event).enqueue(callback);
    }

    @Subscribe
    public void changeStatus(final ChangeStatusEvent event) {
        ApiCallback<ChangeStatusResultEvent> callback = new ApiCallback<>("DriverStatus");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).changeDriverStatus(event).enqueue(callback);
    }

    @Subscribe
    public void serviceInLocation(ServiceInLocationEvent event) {
        socket.emit("buzz");
    }

    @Subscribe
    public void startTaxi(ServiceStartEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("startTravel");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).startTravel().enqueue(callback);
        //socket.emit(START_TRAVEL);
    }

    @Subscribe
    public void callRequest(ServiceCallRequestEvent event) {
        socket.emit("callRequest", (Ack) args -> eventBus.post(new ServiceCallRequestResultEvent((int) args[0])));
    }

    @Subscribe
    public void cancelTaxi(ServiceCancelEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("cancelTravel");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).cancelTravel().enqueue(callback);

        //socket.emit(CANCEL_TRAVEL, (Ack) args -> eventBus.post(new ServiceCancelResultEvent((int) args[0])));
    }

    @Subscribe
    public void getTravels(final GetTravelsEvent event) {
        ApiCallback<GetTravelsResultEvent> callback = new ApiCallback<>("getTravels");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getTravels().enqueue(callback);
    }

    @Subscribe
    public void locationChanged(LocationChangedEvent event) {
        if (socket != null)
            socket.emit("locationChanged", event.location.latitude, event.location.longitude);
    }

    @Subscribe
    public void chargeAccount(ChargeAccountEvent event) {
        ApiCallback<ChargeAccountResultEvent> callback = new ApiCallback<>("ChargeAccount");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).chargeAccount(event).enqueue(callback);
    }

    @Subscribe
    public void ChangeProfileImage(ChangeProfileImageEvent event) {
        File file = new File(event.path);
        byte[] data = new byte[(int) file.length()];
        try {
            int len = new FileInputStream(file).read(data);
            if (len > 0)
                socket.emit("changeProfileImage", data, (Ack) args -> eventBus.post(new ChangeProfileImageResultEvent((int) args[0], args[1].toString())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void HideTravel(final HideTravelEvent event) {
        ApiCallback<HideTravelResultEvent> callback = new ApiCallback<>("hideTravel");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).hideTravel(event).enqueue(callback);
    }

    @Subscribe
    public void changeHeaderImage(ChangeHeaderImageEvent event) {
        File file = new File(event.path);
        byte[] data = new byte[(int) file.length()];
        try {
            int len = new FileInputStream(file).read(data);
            if (len > 0)
                socket.emit("changeHeaderImage", data, (Ack) args -> eventBus.post(new ChangeHeaderImageResultEvent((int) args[0], args[1].toString())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void getDriverStatistics(GetStatisticsEvent event) {
        ApiCallback<GetStatisticsResultEvent> callback = new ApiCallback<>("getStats");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getStats(event).enqueue(callback);
    }

    @Subscribe
    public void WriteComplaint(final WriteComplaintEvent event) {
        ApiCallback<WriteComplaintResultEvent> callback = new ApiCallback<>("writeComplaint");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).writeComplaint(event).enqueue(callback);
    }

    @Subscribe
    public void sendTravelInfo(SendTravelInfoEvent event) {
        socket.emit("travelInfo", event.travel.getDistanceReal(), event.travel.getDurationReal(), event.travel.getCost());
    }

    @Subscribe
    public void getTransactions(GetTransactionsRequestEvent event) {
        ApiCallback<GetTransactionsResultEvent> callback = new ApiCallback<>("getTransactions");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getTransactions().enqueue(callback);
    }

    @Subscribe
    public void getRequests(GetRequestsRequestEvent event) {
        ApiCallback<GetRequestsResultEvent> callback = new ApiCallback<>("getRequests");
        RetrofitClient.getClient(mPref.getString("driver_token", null)).getRequests().enqueue(callback);
    }

    @Subscribe
    public void getCarDetails(GetCarEvent event) {
        ApiCallback<CarDetailList> callback = new ApiCallback<>("getCarList");
        RetrofitClient.getClient("").getCarDetails().enqueue(callback);
    }

    @Subscribe
    public void getCityDetails(GetCityEvent event) {
        ApiCallback<CityList> callback = new ApiCallback<>("getCityList");
        RetrofitClient.getClient("").getCityDetails().enqueue(callback);
    }

    @Subscribe
    public void uploadDocuments(DocumentUploadEvent event) {
        ApiCallback<CommonResponse> callback = new ApiCallback<>("uploadDocumentList");
        RetrofitClient.getFormDataClient(event.isUserLoggedIn() ? mPref.getString("driver_token", null) : mPref.getString("driver_token_1", null)).uploadDocument(event.getPartList(), createPartFromString(event.getType())).enqueue(callback);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mPref = MyPreferenceManager.getInstance(this.getApplicationContext());
        eventBus.post(new BackgroundServiceStartedEvent());
        startLocationUpdates();
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPref = MyPreferenceManager.getInstance(this.getApplicationContext());
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {

        removeLocationUpdate();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {

        // Create the location request to start
        // receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setSmallestDisplacement(0);
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        initLocationCallback();
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationClient().requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());
    }


    public void removeLocationUpdate() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        mLocationRequest = null;
        mLocationCallback = null;
        mFusedLocationClient = null;
    }


    public FusedLocationProviderClient getFusedLocationClient() {
        if (mFusedLocationClient == null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        }
        return mFusedLocationClient;
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void initLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onLocationChanged(locationResult.getLastLocation());
            }
        };
    }


    private void onLocationChanged(Location location) {
        Log.e("TAG", "onLocationChanged");
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        sendToActivity(latLng);

        if (mPref.getBoolean("DRIVER_STATUS", false)) {
            Log.e("TAG", "Sent ToServer");
            eventBus.post(new LocationChangedEvent(latLng));
        }
    }


    private void sendToActivity(LatLng mShareLocationModel) {
        Intent intent = new Intent(SERVICE_TO_ACTIVITY_ACTION);
        intent.putExtra("LOCATION_UPDATE_Lat", mShareLocationModel.latitude);
        intent.putExtra("LOCATION_UPDATE_Lon", mShareLocationModel.longitude);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

}