package com.innomalist.taxi.driver.activities.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.innomalist.taxi.common.activities.DocumentUploadActivity;
import com.innomalist.taxi.common.activities.chargeAccount.QCabPaymentActivity;
import com.innomalist.taxi.common.activities.transactions.TransactionsActivity;
import com.innomalist.taxi.common.activities.travels.TravelsActivity;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.events.AcceptRequestEvent;
import com.innomalist.taxi.common.events.GetStatusEvent;
import com.innomalist.taxi.common.events.GetStatusResultEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.events.ProfileInfoChangedEvent;
import com.innomalist.taxi.common.location.MapHelper;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.models.Request;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.DataBinder;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.activities.about.AboutActivity;
import com.innomalist.taxi.driver.activities.driverservices.DriverServiceActivity;
import com.innomalist.taxi.driver.activities.homelocation.DriverHomeLocationActivity;
import com.innomalist.taxi.driver.activities.main.adapters.RequestsFragmentPagerAdapter;
import com.innomalist.taxi.driver.activities.main.fragments.RequestCardFragment;
import com.innomalist.taxi.driver.activities.profile.ProfileActivity;
import com.innomalist.taxi.driver.activities.splash.LanguageChooseDialog;
import com.innomalist.taxi.driver.activities.statistics.StatisticsActivity;
import com.innomalist.taxi.driver.activities.travel.TravelActivity;
import com.innomalist.taxi.driver.databinding.ActivityMainBinding;
import com.innomalist.taxi.driver.events.AutoAcceptEvent;
import com.innomalist.taxi.driver.events.CancelRequestEvent;
import com.innomalist.taxi.common.events.ChangeStatusEvent;
import com.innomalist.taxi.common.events.ChangeStatusResultEvent;
import com.innomalist.taxi.driver.events.GetRequestsRequestEvent;
import com.innomalist.taxi.driver.events.GetRequestsResultEvent;
import com.innomalist.taxi.driver.events.LocationChangedEvent;
import com.innomalist.taxi.driver.events.RejectRequestEvent;
import com.innomalist.taxi.driver.events.RequestReceivedEvent;
import com.innomalist.taxi.driver.events.RideAcceptNewEvent;
import com.innomalist.taxi.driver.events.RiderAcceptedEvent;
import com.innomalist.taxi.driver.ui.DriverBaseActivity;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.innomalist.taxi.driver.services.DriverService.SERVICE_TO_ACTIVITY_ACTION;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class MainActivity extends DriverBaseActivity implements OnMapReadyCallback, LanguageChooseDialog.LanguageChangeListener, RequestCardFragment.OnFragmentInteractionListener, OSSubscriptionObserver {
    private MyPreferenceManager mPref;
    private GoogleMap mMap;
    private Marker mDriverPoint;
    private ActivityMainBinding mBinding;
    private RequestsFragmentPagerAdapter requestCardsAdapter;
    static final int ACTIVITY_PROFILE = 11;
    static final int ACTIVITY_WALLET = 12;
    static final int ACTIVITY_TRAVEL = 14;
    static final int ACTIVITY_UP_DOC = 15;
    private static final int ON_DO_NOT_DISTURB_CALLBACK_CODE = 1012;
    private SupportMapFragment mapFragment;
    private NotificationManager notificationManager;
    private MediaPlayer m;
    private ServiceToActivityReceiver mServiceToActivityReceiver;
    private BottomSheetBehavior bottomSheetBehavior;
    public static AtomicBoolean isInBackGround = new AtomicBoolean(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OneSignal.addSubscriptionObserver(this);

        mBinding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        bottomSheetBehavior = BottomSheetBehavior.from(mBinding.bottomSheet);
        requestCardsAdapter = new RequestsFragmentPagerAdapter(getSupportFragmentManager(), new ArrayList<>());
        mBinding.requestsViewPager.setAdapter(requestCardsAdapter);
        mBinding.requestsViewPager.setOffscreenPageLimit(3);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mPref = MyPreferenceManager.getInstance(this.getApplicationContext());
        setSupportActionBar(mBinding.appbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (mPref.getString("LANG", null) == null) {
            mPref.putString("LANG", "EN");
        }

        mBinding.lang.setText(mPref.getString("LANG", "EN"));

        mBinding.navigationView.setNavigationItemSelectedListener(menuItem -> {
            mBinding.drawerLayout.closeDrawers();
            switch (menuItem.getItemId()) {
                case (R.id.nav_item_home_location):
                    startActivity(new Intent(MainActivity.this, DriverHomeLocationActivity.class));
                    break;
                case (R.id.nav_item_travels):
                    startActivity(new Intent(MainActivity.this, TravelsActivity.class));
                    break;
                case (R.id.nav_item_profile):
                    startActivityForResult(new Intent(MainActivity.this, ProfileActivity.class), ACTIVITY_PROFILE);
                    break;
                case (R.id.nav_item_statistics):
                    startActivity(new Intent(MainActivity.this, StatisticsActivity.class));
                    break;
                case (R.id.nav_item_charge_account):
                    startActivityForResult(new Intent(MainActivity.this, QCabPaymentActivity.class), ACTIVITY_WALLET);
                    break;
                case (R.id.nav_item_transactions):
                    startActivity(new Intent(MainActivity.this, TransactionsActivity.class));
                    break;
                case (R.id.nav_item_driver_service):
                    startActivity(new Intent(MainActivity.this, DriverServiceActivity.class));
                    break;
                case (R.id.nav_item_document_upload):
                    startActivityForResult(new Intent(MainActivity.this, DocumentUploadActivity.class), ACTIVITY_UP_DOC);
                    break;
                case (R.id.nav_item_about):
                    startActivity(new Intent(MainActivity.this, AboutActivity.class));
                    break;
                case (R.id.nav_item_exit):
                    logout();
                    break;
                default:
                    Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        });
        fillInfo();
        eventBus.post(new GetStatusEvent());
        mBinding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
        mBinding.sos.setOnClickListener(view -> startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "100", null))));
        mBinding.lang.setOnClickListener(view -> {
            LanguageChooseDialog languageChooseDialog = new LanguageChooseDialog(MainActivity.this);
            languageChooseDialog.setLanguageChangeListener(MainActivity.this);
            languageChooseDialog.show();
        });
        mBinding.currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDriverPoint != null)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mDriverPoint.getPosition(), 18));
            }
        });

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !notificationManager.isNotificationPolicyAccessGranted()) {
            requestDoNotDisturbPermissionOrSetDoNotDisturbApi23AndUp();
        } /*else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && !NotificationManagerCompat.getEnabledListenerPackages(this).contains(getPackageName())) {
            requestListenerForOtherAppNotification();
        }*/
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestDoNotDisturbPermissionOrSetDoNotDisturbApi23AndUp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && notificationManager.isNotificationPolicyAccessGranted()) {
            return;
        }

        //TO SUPPRESS API ERROR MESSAGES IN THIS FUNCTION, since Ive no time to figrure our Android SDK suppress stuff
        new MaterialDialog.Builder(this)
                .title(getString(R.string.app_name))
                .cancelable(false)
                .content(getString(R.string.note_access_msg))
                .positiveText(getString(R.string.ok))
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                    startActivityForResult(intent, ON_DO_NOT_DISTURB_CALLBACK_CODE);
                })
                .negativeText("No")
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();

    }


    private void requestListenerForOtherAppNotification() {
        if (NotificationManagerCompat.getEnabledListenerPackages(this).contains(getPackageName())) {
            return;
        }

        //TO SUPPRESS API ERROR MESSAGES IN THIS FUNCTION, since Ive no time to figrure our Android SDK suppress stuff
        new MaterialDialog.Builder(this)
                .title(getString(R.string.app_name))
                .cancelable(false)
                .content(getString(R.string.other_app_note_access_msg))
                .positiveText(getString(R.string.ok))
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                    startActivity(intent);
                })
                .negativeText("No")
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setTrafficEnabled(false);
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_map_style));

        mMap.setOnMapLoadedCallback(() -> {
            getLastKnownLocation();
            if (getResources().getBoolean(R.bool.isNightMode)) {
                boolean success = mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.map_night));
                if (!success)
                    Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mBinding.drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onStart() {
        super.onStart();

       /* loadingRequestsLoadingDialog = new MaterialDialog.Builder(this)
                .title("Reloading statusCode")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();*/
        eventBus.post(new GetRequestsRequestEvent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerLocationReceiver();
        isInBackGround.set(false);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestReceived(RequestReceivedEvent event) {
        requestCardsAdapter.clear();

        requestCardsAdapter.add(event.request);
        requestCardsAdapter.notifyDataSetChanged();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        playSound();
        if (event.request.nearest) {
            startAutoStartTimer(event.request.travel.getId(), false);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestsReceived(GetRequestsResultEvent event) {
        if (loadingRequestsLoadingDialog != null)
            loadingRequestsLoadingDialog.dismiss();

        if (event.data == null || event.data.isEmpty()) {
            return;
        }
        requestCardsAdapter.clear();
        if (getIntent().getBooleanExtra("IsFromNote", false)) {
            playSound();
            if (event.data.get(0).nearest) {
                startAutoStartTimer(event.data.get(0).travel.getId(), false);
            }
        }
        requestCardsAdapter = new RequestsFragmentPagerAdapter(getSupportFragmentManager(), event.data);
        mBinding.requestsViewPager.setAdapter(requestCardsAdapter);
        mBinding.requestsViewPager.setOffscreenPageLimit(3);
        if (event.data.size() > 0) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            Log.e("GOT A REQUEST", "TRUE");
        }


    }

    @Override
    public void onStop() {
        isInBackGround.set(true);
        super.onStop();
    }

    private void playSound() {
        try {
            OneSignal.clearOneSignalNotifications();
            stopSound();
            m = new MediaPlayer();
            AssetFileDescriptor descriptor = getAssets().openFd("consequence.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setLooping(true);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopSound() {
        if (m != null && m.isPlaying()) {
            m.stop();
            m.release();
            m = null;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onProfileChanged(ProfileInfoChangedEvent event) {
        fillInfo();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatusChanged(ChangeStatusResultEvent event) {
        if (event.hasError()) {
            event.showError(MainActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    onConnectionSwitchChanged.onCheckedChanged(null, mBinding.switchConnection.isChecked());
                else {
                    mBinding.switchConnection.setEnabled(true);
                    mBinding.switchConnection.setOnCheckedChangeListener(null);
                    mBinding.switchConnection.setChecked(!mBinding.switchConnection.isChecked());
                    mBinding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
                }
            });
            return;
        }
        if (event.status == 200) {
            mPref.putBoolean("DRIVER_STATUS", mBinding.switchConnection.isChecked());
            CommonUtils.driver.setStatus(mBinding.switchConnection.isChecked() ? "online" : "offline");
            mPref.putString("driver_user", new Gson().toJson(CommonUtils.driver, Driver.class));
            mBinding.switchConnection.setEnabled(true);
            if (!mBinding.switchConnection.isChecked() && mDriverPoint != null) {
                mDriverPoint.remove();
                mDriverPoint = null;
            }
        } else {
            mBinding.switchConnection.setEnabled(true);
            mBinding.switchConnection.setOnCheckedChangeListener(null);
            mBinding.switchConnection.setChecked(!mBinding.switchConnection.isChecked());
            mBinding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
        }

    }


    private CompoundButton.OnCheckedChangeListener onConnectionSwitchChanged = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (compoundButton.isPressed()) {
                if (mBinding.switchConnection.isChecked()) {
                    LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if ((manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) || (mDriverPoint != null && mDriverPoint.getPosition() == null)) {
                        mBinding.switchConnection.setChecked(false);
                        CommonUtils.displayPromptForEnablingGPS(MainActivity.this);
                        return;
                    }
                    eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
                    if (mDriverPoint != null)
                        eventBus.post(new LocationChangedEvent(mDriverPoint.getPosition()));
                } else
                    eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.OFFLINE));
                mBinding.switchConnection.setEnabled(false);
            }
            if (!mBinding.switchConnection.isChecked() && mDriverPoint != null) {
                mDriverPoint.remove();
                mDriverPoint = null;
            }
        }
    };

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager manager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers;
        if (manager != null) {
            providers = manager.getProviders(true);
        } else
            return;
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = manager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        LatLng latLng;
        if (bestLocation == null)
            latLng = new LatLng(Float.parseFloat(getString(R.string.defaultLocation).split(",")[0]), Float.parseFloat(getString(R.string.defaultLocation).split(",")[1]));
        else
            latLng = new LatLng(bestLocation.getLatitude(), bestLocation.getLongitude());
        if (mDriverPoint == null && mBinding.switchConnection.isChecked())
            mDriverPoint = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .flat(true)
                    .rotation(0)
                    .anchor(.5f, .5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));

        if (mBinding.switchConnection.isChecked()) {
            if (mDriverPoint != null)
                eventBus.post(new LocationChangedEvent(latLng));

        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
    }

    private void fillInfo() {
        try {
            String name;

            if (CommonUtils.driver.getStatus() != null && CommonUtils.driver.getStatus().equals("blocked")) {
                logout();
                return;
            }

            mBinding.switchConnection.setChecked(CommonUtils.driver.getStatus().equalsIgnoreCase("ONLINE"));
            mPref.putBoolean("DRIVER_STATUS", CommonUtils.driver.getStatus().equalsIgnoreCase("ONLINE"));
            if ((CommonUtils.driver.getFirstName() == null || CommonUtils.driver.getFirstName().isEmpty()) && (CommonUtils.driver.getLastName() == null || CommonUtils.driver.getLastName().isEmpty()))
                name = String.valueOf(CommonUtils.driver.getMobileNumber());
            else
                name = CommonUtils.driver.getFirstName() + " " + CommonUtils.driver.getLastName();
            View header = mBinding.navigationView.getHeaderView(0);
            ((TextView) header.findViewById(R.id.navigation_header_name)).setText(name);
            ((TextView) header.findViewById(R.id.navigation_header_charge)).setText(getString(R.string.drawer_header_balance, CommonUtils.driver.getBalance()));
            ImageView imageView = header.findViewById(R.id.navigation_header_image);
            ImageView headerView = header.findViewById(R.id.navigation_background);
            DataBinder.setMedia(imageView, CommonUtils.driver.getMedia());
            DataBinder.setMedia(headerView, CommonUtils.driver.getCarMedia());
        } catch (Exception ignored) {
        }
    }

    private void logout() {
        mPref.clearPreferences();
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ACTIVITY_PROFILE):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.info_edit_profile_success));
                fillInfo();
                break;

            case (ACTIVITY_WALLET):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.account_charge_success));
                fillInfo();
                break;
            case (ACTIVITY_UP_DOC):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.document_upload_success_msg));
                fillInfo();
                break;

            case (ACTIVITY_TRAVEL):
                /*mBinding.switchConnection.setOnCheckedChangeListener(null);
                mBinding.switchConnection.setChecked(true);
                mBinding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
                onConnectionSwitchChanged.onCheckedChanged(mBinding.switchConnection, mBinding.switchConnection.isChecked());*/
                break;
        }
    }


    @Subscribe(threadMode = MAIN)
    public void OnGetStatusResultReceived(GetStatusResultEvent event) {
        if (loadingRequestsLoadingDialog != null)
            loadingRequestsLoadingDialog.dismiss();

        if (event.status == 200) {
            moveToTravelActivity(event.getTravel().toJson());
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAnotherDriverAcceptedRequest(CancelRequestEvent event) {
        OneSignal.clearOneSignalNotifications();
        stopSound();
        LoadingDialog.dismiss();
        startAutoStartTimer(event.travelId, true);
        requestCardsAdapter.clear();
        requestCardsAdapter.notifyDataSetChanged();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRiderAccepted(RiderAcceptedEvent event) {
        LoadingDialog.dismiss();
        moveToTravelActivity(event.travel.toJson());
        // startActivityForResult(intentTravel, ACTIVITY_TRAVEL);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRiderAcceptedNewEvent(RideAcceptNewEvent event) {
        LoadingDialog.dismiss();
        if (event.isSuccessful()) {
            moveToTravelActivity(event.getData().getTravel().toJson());
        } else {
            showToastMessage(event.getMessage());
        }
        // startActivityForResult(intentTravel, ACTIVITY_TRAVEL);
    }


    private void moveToTravelActivity(String travelObject) {

        stopSound();
        OneSignal.clearOneSignalNotifications();
        Intent intentTravel = new Intent(MainActivity.this, TravelActivity.class);
        intentTravel.putExtra("travel", travelObject);
        intentTravel.putExtra("driverLat", mDriverPoint == null ? -1 : mDriverPoint.getPosition().latitude);
        intentTravel.putExtra("driverLng", mDriverPoint == null ? -1 : mDriverPoint.getPosition().longitude);
        startActivity(intentTravel);
        finish();
    }

    @Override
    public void onAccept(Request request) {
        OneSignal.clearOneSignalNotifications();
        stopSound();
        eventBus.post(new AcceptRequestEvent(request.travel.getId()));
        while (requestCardsAdapter.getCount() > 0)
            requestCardsAdapter.remove(0);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onDecline(Request request) {
        OneSignal.clearOneSignalNotifications();
        stopSound();

        eventBus.post(new RejectRequestEvent(CommonUtils.driver.getId(), request.travel.getRiderId()));
        requestCardsAdapter.clear();
        requestCardsAdapter.notifyDataSetChanged();

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

    }

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom().getSubscribed() && stateChanges.getTo().getSubscribed())
            eventBus.post(new NotificationPlayerId(stateChanges.getTo().getUserId()));
    }

    public void onRefreshRequestsClicked(View view) {
        if (loadingRequestsLoadingDialog == null) {
            loadingRequestsLoadingDialog = new MaterialDialog.Builder(this)
                    .title("Reloading")
                    .content("Please wait...")
                    .progress(true, 0)
                    .cancelable(false)
                    .show();
        } else {
            loadingRequestsLoadingDialog.show();
        }
        eventBus.post(new GetRequestsRequestEvent());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    private void registerLocationReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SERVICE_TO_ACTIVITY_ACTION);
        intentFilter.setPriority(1);
        mServiceToActivityReceiver = new ServiceToActivityReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mServiceToActivityReceiver, intentFilter);
    }

    @Override
    public void onLanguageSelected(LanguageChooseDialog.Language language) {
        Toast.makeText(this, String.valueOf(language), Toast.LENGTH_LONG).show();
        if (language == LanguageChooseDialog.Language.HINDI) {
            mPref.putString("LANG", "HI");
            LocaleHelper.setLocale(this, "hi");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ENGLISH) {
            mPref.putString("LANG", "EN");
            LocaleHelper.setLocale(this, "en");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.SPANISH) {
            mPref.putString("LANG", "ES");
            LocaleHelper.setLocale(this, "es");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.BENGALI) {
            mPref.putString("LANG", "BN");
            LocaleHelper.setLocale(this, "bn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PUNJABI) {
            mPref.putString("LANG", "PA");
            LocaleHelper.setLocale(this, "pa");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.FRENCH) {
            mPref.putString("LANG", "FR");
            LocaleHelper.setLocale(this, "fr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ARABIC) {
            mPref.putString("LANG", "AR");
            LocaleHelper.setLocale(this, "ar");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.IRISH) {
            mPref.putString("LANG", "GA");
            LocaleHelper.setLocale(this, "ga");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GUJARATI) {
            mPref.putString("LANG", "GU");
            LocaleHelper.setLocale(this, "gu");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GERMAN) {
            mPref.putString("LANG", "DE");
            LocaleHelper.setLocale(this, "de");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.INDONESIAN) {
            mPref.putString("LANG", "ID");
            LocaleHelper.setLocale(this, "id");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ITALIAN) {
            mPref.putString("LANG", "IT");
            LocaleHelper.setLocale(this, "it");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KANNADA) {
            mPref.putString("LANG", "KN");
            LocaleHelper.setLocale(this, "kn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KOREAN) {
            mPref.putString("LANG", "KO");
            LocaleHelper.setLocale(this, "ko");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAYALAM) {
            mPref.putString("LANG", "ML");
            LocaleHelper.setLocale(this, "ml");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAY) {
            mPref.putString("LANG", "MS");
            LocaleHelper.setLocale(this, "ms");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MARATHI) {
            mPref.putString("LANG", "MR");
            LocaleHelper.setLocale(this, "mr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.NEPALI) {
            mPref.putString("LANG", "NE");
            LocaleHelper.setLocale(this, "ne");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.DUTCH) {
            mPref.putString("LANG", "NL");
            LocaleHelper.setLocale(this, "nl");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ORIYA) {
            mPref.putString("LANG", "OR");
            LocaleHelper.setLocale(this, "or");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PORTUGUESE) {
            mPref.putString("LANG", "PT");
            LocaleHelper.setLocale(this, "pt");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.RUSSIAN) {
            mPref.putString("LANG", "RU");
            LocaleHelper.setLocale(this, "ru");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TAMIL) {
            mPref.putString("LANG", "TA");
            LocaleHelper.setLocale(this, "ta");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TELUGU) {
            mPref.putString("LANG", "TE");
            LocaleHelper.setLocale(this, "te");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.URDU) {
            mPref.putString("LANG", "UR");
            LocaleHelper.setLocale(this, "ur");
            this.recreate();
        }
    }

    public class ServiceToActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            LatLng latLng = new LatLng(intent.getDoubleExtra("LOCATION_UPDATE_Lat", 0.0), intent.getDoubleExtra("LOCATION_UPDATE_Lon", 0.0));
            update(latLng);
        }
    }

    private void update(LatLng driverLatLng) {
        if (driverLatLng == null || mMap == null) {
            return;
        }
        if (mBinding.switchConnection.isChecked()) {
            if (mDriverPoint == null) {
                mDriverPoint = mMap.addMarker(new MarkerOptions()
                        .position(driverLatLng)
                        .flat(true)
                        .anchor(.5f, .5f)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLatLng, 18));
            } else {
                MapHelper.animateMarkerToLocation(driverLatLng, mDriverPoint);
                MapHelper.moveMap(mMap, driverLatLng);
            }
        } else {
            if (mDriverPoint != null) {
                mDriverPoint.remove();
                mDriverPoint = null;
            }
        }
    }

    private void startAutoStartTimer(int travelId, boolean wantToCancel) {
        eventBus.post(new AutoAcceptEvent(travelId));
    }

    private void Timer() {
        Thread th = new Thread(() -> {
            while (mDriverPoint != null) {
                runOnUiThread(() -> {

                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        th.start();
    }
}
