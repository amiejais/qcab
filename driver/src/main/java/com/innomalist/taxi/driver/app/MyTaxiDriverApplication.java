package com.innomalist.taxi.driver.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.innomalist.taxi.driver.activities.splash.SplashActivity;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

import static com.innomalist.taxi.driver.activities.travel.TravelActivity.NOTE_ACTION;

public class MyTaxiDriverApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(getApplicationContext());
        int nightMode = AppCompatDelegate.MODE_NIGHT_NO;
        AppCompatDelegate.setDefaultNightMode(nightMode);

        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new DriverNotificationHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private class DriverNotificationHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            Intent resultIntent;
            Log.e("TAG", "Title: " + notification.payload.title);
            if (notification.payload.title.equalsIgnoreCase("Cancel Travel")) {

                if (!notification.isAppInFocus) {
                    resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    //resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(resultIntent);
                } else {
                    Intent intent = new Intent(NOTE_ACTION);
                    intent.putExtra("Type", notification.payload.title);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

            } else {
                if (!notification.isAppInFocus) {
                    resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                    resultIntent.putExtra("IsFromNote", true);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    //resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(resultIntent);
                }
            }


        }
    }

}