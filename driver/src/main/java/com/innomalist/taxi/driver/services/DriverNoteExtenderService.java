package com.innomalist.taxi.driver.services;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.activities.main.MainActivity;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;

public class DriverNoteExtenderService extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        // Read Properties from result
        OverrideSettings overrideSettings = new OverrideSettings();
        overrideSettings.extender = builder -> builder;
        //changeRingMode();
        OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
        Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);
        // Return true to stop the notification from displaying
        return true;
    }

    private void changeRingMode() {
        AudioManager mobilemode = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        switch (mobilemode.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                Log.e("MyApp", "Silent mode");
                mobilemode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                mobilemode.setStreamVolume(AudioManager.STREAM_RING, mobilemode.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                Log.e("MyApp", "Vibrate mode");
                mobilemode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                mobilemode.setStreamVolume(AudioManager.STREAM_RING, mobilemode.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                Log.e("MyApp", "Normal mode");
                mobilemode.setStreamVolume(AudioManager.STREAM_RING, mobilemode.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
                break;
        }
    }
}
