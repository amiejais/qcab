package com.innomalist.taxi.driver.activities.travel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.PolyUtil;
import com.innomalist.taxi.common.activities.travels.fragments.ReviewDialog;
import com.innomalist.taxi.common.components.LoadingDialog;
import com.innomalist.taxi.common.events.ChangeStatusEvent;
import com.innomalist.taxi.common.events.ServiceCallRequestResultEvent;
import com.innomalist.taxi.common.events.ServiceCancelEvent;
import com.innomalist.taxi.common.location.MapHelper;
import com.innomalist.taxi.common.models.ReviewDriverEvent;
import com.innomalist.taxi.common.models.Travel;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.DirectionsJSONParser;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.LocationHelper;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.activities.main.MainActivity;
import com.innomalist.taxi.driver.databinding.ActivityTravelBinding;
import com.innomalist.taxi.driver.events.AutoAcceptEvent;
import com.innomalist.taxi.driver.events.CancelRequestEvent;
import com.innomalist.taxi.driver.events.LocationChangedEvent;
import com.innomalist.taxi.driver.events.RequestReceivedEvent;
import com.innomalist.taxi.driver.events.SendTravelInfoEvent;
import com.innomalist.taxi.driver.events.ServiceFinishEvent;
import com.innomalist.taxi.driver.events.ServiceFinishResultEvent;
import com.innomalist.taxi.driver.events.ServiceInLocationEvent;
import com.innomalist.taxi.driver.events.ServiceStartEvent;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.driver.ui.DriverBaseActivity;
import com.innomalist.taxi.rider.events.ReviewDriverResultEvent;
import com.onesignal.OneSignal;
import com.transitionseverywhere.TransitionManager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.innomalist.taxi.common.utils.CommonUtils.deleteCountry;
import static com.innomalist.taxi.driver.services.DriverService.SERVICE_TO_ACTIVITY_ACTION;

public class TravelActivity extends DriverBaseActivity implements OnMapReadyCallback, ReviewDialog.onReviewFragmentInteractionListener {
    Travel travel;
    GoogleMap gMap;
    boolean endTravel = false;
    LatLng currentLocation;
    ActivityTravelBinding binding;
    Marker currentMarker;
    Marker destinationMarker;
    DirectionsJSONParser directionToPassengerRouter;
    List<LatLng> geoLog = new ArrayList<>();
    private ServiceToActivityReceiver mServiceToActivityReceiver;
    private NoteUpdateReceiver mNoteUpdateReceiver;
    public static final String NOTE_ACTION = "com.qcab.driver_Note_Action";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_travel);
        travel = Travel.fromJson(getIntent().getStringExtra("travel"));
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        binding.slideStart.setOnSlideCompleteListener(slideView -> startTravel(true));
        binding.slideFinish.setOnSlideCompleteListener(slideView -> finishTravel());
        binding.slideCancel.setOnSlideCompleteListener(slideView -> {
            eventBus.post(new ServiceCancelEvent());
            eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
        });
        binding.costText.setText(getString(R.string.unit_money, travel.getCostBest()));
        binding.sos.setOnClickListener(view -> startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "100", null))));
        binding.openMap.setOnClickListener(view -> openMap());

    }


    private void openMap() {

        if (travel != null) {
            Uri gmmIntentUri;
            if (travel.getStartTimestamp() != null) {
                gmmIntentUri = Uri.parse("google.navigation:q=" + travel.getDestinationPoint().latitude + "," + travel.getDestinationPoint().longitude);
            } else {
                gmmIntentUri = Uri.parse("google.navigation:q=" + travel.getPickupPoint().latitude + "," + travel.getPickupPoint().longitude);
            }

            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.getUiSettings().setMyLocationButtonEnabled(false);
        gMap.getUiSettings().setCompassEnabled(false);
        gMap.getUiSettings().setMapToolbarEnabled(false);
        gMap.setTrafficEnabled(false);
        gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_map_style));
        LatLng dLocation = new LatLng(getIntent().getDoubleExtra("driverLat", -1), getIntent().getDoubleExtra("driverLng", -1));
        currentLocation = dLocation;
        currentMarker = googleMap.addMarker(new MarkerOptions()
                .position(dLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));
        destinationMarker = googleMap.addMarker(new MarkerOptions()
                .position(travel.getPickupPoint())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_destination)));
        if (travel.getStartTimestamp() != null) {
            startTravel(false);
            binding.edtAddress.setText(travel.getDestinationAddress());
        } else {
            binding.edtAddress.setText(travel.getPickupAddress());
        }

        googleMap.setOnMapLoadedCallback(this::updateDirectionAndLocation);

    }

    private void updateDirectionAndLocation() {
        List<LatLng> locations = new ArrayList<>();
        locations.add(currentMarker.getPosition());
        locations.add(destinationMarker.getPosition());
        MapHelper.centerLatLngsInMap(gMap, locations, true);

        if (directionToPassengerRouter != null) {
            directionToPassengerRouter.removeLine();
        }

        directionToPassengerRouter = new DirectionsJSONParser(gMap, currentMarker.getPosition(), destinationMarker.getPosition());
        directionToPassengerRouter.run();
    }

    private String computeTime(int time) {
        if (time == 0)
            return "00:00";
        int sec = time % 60;
        int min = time / 60;
        return String.format(Locale.getDefault(), "%02d:%02d", min, sec);
    }


    private void Timer() {
        Thread th = new Thread(() -> {
            while (!endTravel) {
                runOnUiThread(() -> {
                    travel.setDurationReal(travel.getDurationReal() + 1);
                    if (binding.getRoot().getResources().getBoolean(R.bool.use_miles))
                        binding.distanceText.setText(binding.getRoot().getContext().getString(R.string.unit_distance_miles, travel.getDistanceReal() / 1609.344f));
                    else
                        binding.distanceText.setText(getString(R.string.unit_distance, (travel.getDistanceReal() / 1000f)));
                    binding.timeText.setText(computeTime(travel.getDurationReal()));
                    binding.costText.setText(getString(R.string.unit_money, travel.getCostBest()));
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        th.start();
    }


    void startTravel(boolean fromScratch) {
        if (fromScratch) {
            travel.setStartTimestamp(new Timestamp(System.currentTimeMillis()));
            eventBus.post(new ServiceStartEvent());
        }
        binding.edtAddress.setText(travel.getDestinationAddress());
        TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
        binding.slideFinish.setVisibility(View.VISIBLE);
        binding.slideCancel.setVisibility(View.GONE);
        binding.slideStart.setVisibility(View.GONE);
        TransitionManager.beginDelayedTransition(binding.layoutActions);
        binding.inLocationButton.setVisibility(View.GONE);
        binding.callButton.setVisibility(View.GONE);
        destinationMarker.setPosition(travel.getDestinationPoint());
        if (fromScratch) {
            updateDirectionAndLocation();
        }
        Timer();

    }

    void finishTravel() {
        finishTravel(travel.getServiceId());
    }

    void finishTravel(String serviceId) {
        endTravel = true;
        String encodedPoly = "";
        if (geoLog.size() > 0)
            encodedPoly = PolyUtil.encode(PolyUtil.simplify(geoLog, 10));
        LatLng destinationPoint = currentLocation == null ? travel.getDestinationPoint() : currentLocation;
        eventBus.post(new ServiceFinishEvent(serviceId, travel.getDurationReal(), destinationPoint, travel.getDestinationAddress(), travel.getDistanceReal(), encodedPoly));
        eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
    }

    public void onInLocationButtonClicked(View v) {
        eventBus.post(new ServiceInLocationEvent());
        TransitionManager.beginDelayedTransition(binding.layoutActions);
        binding.inLocationButton.setVisibility(View.GONE);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void locationChangeRequest(RequestReceivedEvent event) {
        travel.setDestinationAddress(event.request.travel.getDestinationAddress());
        travel.setDestinationPoint(new LatLng(event.request.travel.getDestinationPoint().y, event.request.travel.getDestinationPoint().x));
        travel.setDistanceReal(event.request.travel.getDistanceReal());
        binding.edtAddress.setText(event.request.travel.getDestinationAddress());

        destinationMarker.setPosition(travel.getDestinationPoint());
        updateDirectionAndLocation();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCallRequested(ServiceCallRequestResultEvent event) {
        LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    onCallDriverClicked(null);
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.call_request_sent));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceStartCancel(CommonResponse event) {
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)

                    if (event.getRequestTag().equalsIgnoreCase("cancelTravel")) {
                        eventBus.post(new ServiceCancelEvent());
                    } else {
                        eventBus.post(new ServiceCancelEvent());
                    }

            });
            return;
        }
        if (event.getRequestTag().equalsIgnoreCase("cancelTravel")) {
            AlertDialogBuilder.show(TravelActivity.this, getString(R.string.service_canceled), AlertDialogBuilder.DialogButton.OK, result -> {
                moveToMainActivity();
            });
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServicedFinished(ServiceFinishResultEvent event) {
        //LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    finishTravel();
            });
            return;
        }
        String message;
        if (event.getData().isCreditUsed)
            message = getString(R.string.travel_finished_taken_from_balance_driver, event.getData().amount);
        else
            message = getString(R.string.travel_finished_not_sufficient_balance_driver, event.getData().amount);

        new MaterialDialog.Builder(this)
                .title(R.string.message_default_title)
                .content(message)
                .cancelable(false)
                .positiveText(R.string.alert_ok)
                .onPositive((dialog, which) -> {
                    moveToMainActivity();
                })
                .show();
    }

    public void onCallDriverClicked(View view) {

        if (travel.getRider() == null || travel.getRider().getMobileNumber() == 0L) {
            Toast.makeText(this, "User number not available. Please try restarting your app.", Toast.LENGTH_SHORT).show();
            return;
        }

        String formatNumber = deleteCountry("" + travel.getRider().getMobileNumber());
        formatNumber = TextUtils.isEmpty(formatNumber) ? "" + travel.getRider().getMobileNumber() : formatNumber;
        formatNumber = "0" + formatNumber;

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", formatNumber, null));
        startActivity(intent);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerLocationReceiver();
    }

    private void registerLocationReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SERVICE_TO_ACTIVITY_ACTION);
        intentFilter.setPriority(1);
        mServiceToActivityReceiver = new ServiceToActivityReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mServiceToActivityReceiver, intentFilter);


        IntentFilter noteIntentFilter = new IntentFilter();
        noteIntentFilter.addAction(NOTE_ACTION);
        noteIntentFilter.setPriority(1);
        mNoteUpdateReceiver = new NoteUpdateReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mNoteUpdateReceiver, noteIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mNoteUpdateReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mNoteUpdateReceiver);
        }

        if (mServiceToActivityReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mServiceToActivityReceiver);
        }
    }

    public class ServiceToActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            LatLng latLng = new LatLng(intent.getDoubleExtra("LOCATION_UPDATE_Lat", 0.0), intent.getDoubleExtra("LOCATION_UPDATE_Lon", 0.0));
            update(latLng);
        }
    }

    private void update(LatLng driverLatLng) {
        if (driverLatLng == null || gMap == null) {
            return;
        }
        if (currentLocation.latitude != -1) {
            travel.setDistanceReal(travel.getDurationReal() + LocationHelper.distFrom(currentLocation.latitude, currentLocation.longitude, driverLatLng.latitude, driverLatLng.longitude));
        }
        eventBus.post(new LocationChangedEvent(driverLatLng));
        eventBus.post(new SendTravelInfoEvent(travel));
        geoLog.add(driverLatLng);
        currentLocation = driverLatLng;
        if (currentMarker == null) {
            currentMarker = gMap.addMarker(new MarkerOptions()
                    .position(driverLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_icon)));
        } else {
            MapHelper.animateMarkerToLocation(driverLatLng, currentMarker);
            MapHelper.moveMap(gMap, driverLatLng);
        }
    }

    @Override
    public void onReviewTravelClicked(ReviewDriverEvent reviewDriverEvent) {
        eventBus.post(reviewDriverEvent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReviewResult(ReviewDriverResultEvent event) {
        if (event.status == 200) {
            if (travel.getFinishTimestamp() != null) {
                finish();
                return;
            }
            AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_review_sent));
        } else
            event.showAlert(TravelActivity.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAnotherDriverAcceptedRequest(CancelRequestEvent event) {
        OneSignal.clearOneSignalNotifications();
        startAutoStartTimer(event.travelId, true);
        moveToMainActivity();
    }

    private void startAutoStartTimer(int travelId, boolean wantToCancel) {
        eventBus.post(new AutoAcceptEvent(travelId));
    }

    private void moveToMainActivity() {
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(resultIntent);
        finish();
    }


    public class NoteUpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            OneSignal.clearOneSignalNotifications();
            if (intent.getStringExtra("Type").equalsIgnoreCase("Cancel Travel")) {
                AlertDialogBuilder.show(TravelActivity.this, getString(R.string.service_canceled), AlertDialogBuilder.DialogButton.OK, result -> {
                    moveToMainActivity();
                });
            }
        }
    }

}
