package com.innomalist.taxi.driver.activities.driverservices;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.databinding.ItemDriverServiceBinding;
import com.innomalist.taxi.driver.models.DriverServiceData;

public class DriverServiceViewHolder extends RecyclerView.ViewHolder {

    @SuppressLint("StaticFieldLeak")
    private static ItemDriverServiceBinding mBinder;
    private IDriverServiceClicked driverServiceClicked;
    private Context mContext;

    DriverServiceViewHolder(Context context, ViewGroup parent, IDriverServiceClicked iDriverServiceClicked) {
        super(getView(context, parent));
        mContext = context;
        driverServiceClicked = iDriverServiceClicked;
    }

    private static View getView(Context mContext, ViewGroup parent) {
        mBinder = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_driver_service, parent, false);
        return mBinder.getRoot();
    }

    void bind(final DriverServiceViewHolder holder, final DriverServiceData item) {
        mBinder.tvServiceTitle.setText(item.getTitle());
        mBinder.swServiceStatus.setChecked(item.isIs_active() == 1);
        mBinder.swServiceStatus.setTag(item);
        mBinder.swServiceStatus.setOnCheckedChangeListener((compoundButton, b) -> {
            SwitchCompat driverStatus = holder.itemView.findViewById(R.id.sw_service_status);
            DriverServiceData driverServiceData = (DriverServiceData) driverStatus.getTag();
            if (driverServiceData.getPrimary() == 1) {
                driverStatus.setChecked(!b);
                Toast.makeText(mContext, "You can not activate/deactivate this service", Toast.LENGTH_SHORT).show();
            } else {
                driverStatus.setChecked(b);
                driverServiceData.setIs_active(b ? 1 : 0);
                driverServiceClicked.onServiceClicked(driverServiceData);
            }
        });
    }
}