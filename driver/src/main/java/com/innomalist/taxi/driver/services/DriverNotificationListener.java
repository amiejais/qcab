package com.innomalist.taxi.driver.services;

import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.annotation.RequiresApi;

public class DriverNotificationListener extends NotificationListenerService {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        String mNotificationPkg = sbn.getPackageName();
        if (mNotificationPkg.equalsIgnoreCase("com.ubercab.driver") || mNotificationPkg.equalsIgnoreCase("com.olacabs.oladriver"))
            cancelNotification(sbn.getKey());

        Log.e("TAG", "mNotificationPkg: " + mNotificationPkg);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {

    }
}