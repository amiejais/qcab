package com.innomalist.taxi.driver.activities.homelocation;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.databinding.ActivityHomeLocationBinding;
import com.innomalist.taxi.driver.events.DeleteHomeLocationRequestEvent;
import com.innomalist.taxi.driver.events.DriverHomeLocationRequestEvent;
import com.innomalist.taxi.driver.events.DriverHomeLocationResult;
import com.innomalist.taxi.driver.events.SetDriverHomeLocationEvent;
import com.innomalist.taxi.driver.models.CommonResponse;

import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class DriverHomeLocationActivity extends BaseActivity {
    private static final int ACTIVITY_PLACES = 1021;
    private ActivityHomeLocationBinding binding;
    private MyPreferenceManager mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_location);
        mPref = MyPreferenceManager.getInstance(this);
        initializeToolbar(getString(R.string.driver_home_location), true);
        //Places.initialize(getApplicationContext(), MAP_API_KEY);
        checkAndSetHomeLocation();
        eventBus.post(new DriverHomeLocationRequestEvent("driverHomeLocationRequestTime"));
    }


    private void checkAndSetHomeLocation() {

        Driver driver = CommonUtils.driver;
        if (driver != null && driver.getHomeLat() != null) {
            GetMarkerAddress task = new GetMarkerAddress();
            task.execute(driver.getHomeLat(), driver.getHomeLng());
        } else {
            binding.selectedLocation.setText(getString(R.string.no_home_location_msg));
            binding.selectedLocationRequest.setVisibility(View.GONE);
        }
        binding.selectedLocationRequest.setOnClickListener(view -> {
            eventBus.post(new DriverHomeLocationRequestEvent("requestDriverHomeLocation"));
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        findPlace();
        return super.onOptionsItemSelected(item);
    }


    @Subscribe(threadMode = MAIN)
    public void onGetDriverHomeLocationResult(CommonResponse event) {
        if (event.status == 200) {
            if (event.getRequestTag().equalsIgnoreCase("driverHomeLocationRequest")) {
                showAlert(getString(R.string.success_request_location_msg));
            } else if (event.getRequestTag().equalsIgnoreCase("DeleteHomeLocationRequest")) {
                //showAlert(getString(R.string.success_delete_location_msg));
                new MaterialDialog.Builder(this)
                        .title(getString(R.string.app_name))
                        .content(getString(R.string.success_delete_location_msg))
                        .positiveText(getString(R.string.alert_ok))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                eventBus.post(new DriverHomeLocationRequestEvent("driverHomeLocationRequestTime"));
                            }
                        })
                        .show();
            } else {
                showAlert(getString(R.string.driver_home_location_msg));
            }
        } else if (event.status == 666) {
            showAlert(getString(R.string.already_request_msg));
        } else {
            showAlert(getString(R.string.unkwon_error_msg));
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onRequestedDriverHomeLocationTime(DriverHomeLocationResult event) {
        if (event.status == 200) {
            if (event.getData() != null && !event.getData().isEmpty()) {
                List<DriverHomeLocationResult.RequestTime> timeList = event.getData();

                try {
                    binding.request1TimeLayout.setVisibility(View.VISIBLE);
                    binding.requestTime1.setText(getString(R.string.request_time_1) + " " + formatDate(timeList.get(0).getReq_time()));
                    binding.deleteRequest1.setOnClickListener(view -> {
                        deleteHomeLocationTime(timeList.get(0).getId());
                    });
                    if (timeList.size() > 1) {
                        binding.request2TimeLayout.setVisibility(View.VISIBLE);
                        binding.requestTime2.setText(getString(R.string.request_time_2) + " " + formatDate(timeList.get(1).getReq_time()));
                        binding.deleteRequest1.setOnClickListener(view -> {
                            deleteHomeLocationTime(timeList.get(1).getId());
                        });
                    } else {
                        binding.request2TimeLayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                binding.requestTime1.setText(getString(R.string.not_requested_msg));
                binding.deleteRequest1.setVisibility(View.GONE);
                binding.request2TimeLayout.setVisibility(View.VISIBLE);
                binding.request2TimeLayout.setVisibility(View.GONE);
            }
        } else {
            showAlert(getString(R.string.unknown_error_msg));
        }

    }

    private void deleteHomeLocationTime(int requestTime) {
        eventBus.post(new DeleteHomeLocationRequestEvent(requestTime));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(this, data);
            //Place place = Autocomplete.getPlaceFromIntent(data);
            binding.selectedLocation.setText(getString(R.string.selected_location) + " " + place.getAddress());
            binding.selectedLocationRequest.setVisibility(View.VISIBLE);
            binding.selectedLocation.setVisibility(View.VISIBLE);
            Driver driver = CommonUtils.driver;
            driver.setHomeLat(place.getLatLng().latitude);
            driver.setHomeLng(place.getLatLng().longitude);
            CommonUtils.driver = driver;
            mPref.putString("driver_user", new Gson().toJson(CommonUtils.driver, Driver.class));
            eventBus.post(new SetDriverHomeLocationEvent(place.getLatLng()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void findPlace() {
        try {

            AutocompleteFilter autocompleteFilter = (new AutocompleteFilter.Builder()).setCountry("IN").build();
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(autocompleteFilter)
                            .build(this);
            startActivityForResult(intent, ACTIVITY_PLACES);

           /* if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), MAP_API_KEY);
            }
            List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN")
                    .build(this);


            startActivityForResult(intent, ACTIVITY_PLACES);*/

        } catch (Exception e) {
            // TODO: Handle the error.
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetMarkerAddress extends AsyncTask<Double, Void, String> {
        @Override
        protected String doInBackground(Double... floats) {
            Geocoder geocoder = new Geocoder(DriverHomeLocationActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(floats[0], floats[1], 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0).getAddressLine(0);
            } else
                return getString(R.string.unknown_location);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s) || !s.equalsIgnoreCase(getString(R.string.unknown_location))) {
                binding.locationLayout.setVisibility(View.VISIBLE);
                binding.selectedLocation.setText(s);
            } else {
                binding.locationLayout.setVisibility(View.GONE);
            }

        }

    }

    private String formatDate(String dateInStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(dateInStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        return targetFormat.format(sourceDate);
    }
}
