package com.innomalist.taxi.driver.activities.driverservices;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innomalist.taxi.driver.models.DriverServiceData;

import java.util.List;

public class DriverServiceAdapter extends RecyclerView.Adapter<DriverServiceViewHolder> {

    private Context mContext;
    private List<DriverServiceData> mList;
    private IDriverServiceClicked driverServiceClicked;

    public DriverServiceAdapter(Context context, IDriverServiceClicked iDriverServiceClicked, List<DriverServiceData> userList) {
        mContext = context;
        mList = userList;
        driverServiceClicked = iDriverServiceClicked;
    }

    @NonNull
    @Override
    public DriverServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DriverServiceViewHolder(mContext, parent, driverServiceClicked);
    }

    @Override
    public void onBindViewHolder(@NonNull DriverServiceViewHolder holder, int position) {
        DriverServiceData item = mList.get(position);
        holder.bind(holder,item);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}
