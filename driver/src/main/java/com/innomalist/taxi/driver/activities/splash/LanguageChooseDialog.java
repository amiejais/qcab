package com.innomalist.taxi.driver.activities.splash;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.widget.TextView;

import com.innomalist.taxi.driver.R;

public class LanguageChooseDialog extends Dialog {

    private TextView hindi;
    private TextView english;
    private TextView bengali;
    private TextView punjabi;
    private TextView french;
    private TextView arabic;
    private TextView dutch;
    private TextView gujarati;
    private TextView irish;
    private TextView indonesian;
    private TextView italian;
    private TextView kannada;
    private TextView korean;
    private TextView malayalam;
    private TextView marathi;
    private TextView malay;
    private TextView nepali;
    private TextView oriya;
    private TextView portuguese;
    private TextView russian;
    private TextView tamil;
    private TextView telugu;
    private TextView urdu;
    private TextView german;
    private TextView spanish;

    public enum Language {
        ENGLISH,
        HINDI,
        BENGALI,
        PUNJABI,
        FRENCH,
        ARABIC,
        DUTCH,
        GUJARATI,
        GERMAN,
        IRISH,
        INDONESIAN,
        ITALIAN,
        KANNADA,
        KOREAN,
        MALAYALAM,
        MARATHI,
        MALAY,
        NEPALI,
        ORIYA,
        PORTUGUESE,
        SPANISH,
        RUSSIAN,
        TAMIL,
        TELUGU,
        URDU

    }


    public interface LanguageChangeListener {
        void onLanguageSelected(Language language);
    }

    LanguageChangeListener languageChangeListener;

    public LanguageChooseDialog(@NonNull Context context) {
        super(context);
    }

    public LanguageChooseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected LanguageChooseDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_options);
        hindi = findViewById(R.id.hindi);
        english = findViewById(R.id.english);
        bengali = findViewById(R.id.bengali);
        punjabi = findViewById(R.id.punjabi);
        french = findViewById(R.id.french);

        arabic = findViewById(R.id.arabic);
        dutch = findViewById(R.id.dutch);
        german = findViewById(R.id.german);
        spanish = findViewById(R.id.spanish);
        gujarati = findViewById(R.id.gujrati);
        irish = findViewById(R.id.irish);
        indonesian = findViewById(R.id.indonesian);
        italian = findViewById(R.id.italian);
        kannada = findViewById(R.id.kannada);

        korean = findViewById(R.id.korean);
        malayalam = findViewById(R.id.malayalam);
        marathi = findViewById(R.id.marathi);
        malay = findViewById(R.id.malay);
        nepali = findViewById(R.id.nepali);
        oriya = findViewById(R.id.oriya);

        portuguese = findViewById(R.id.portuguese);
        russian = findViewById(R.id.russian);
        tamil = findViewById(R.id.tamil);

        telugu = findViewById(R.id.telugu);
        urdu = findViewById(R.id.urdu);

        setCancelable(true);

        hindi.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.HINDI);
        });

        english.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.ENGLISH);
        });

        bengali.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.BENGALI);
        });

        punjabi.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.PUNJABI);
        });


        arabic.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.ARABIC);
        });

        german.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.GERMAN);
        });

        spanish.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.SPANISH);
        });

        french.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.FRENCH);
        });

        dutch.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.DUTCH);
        });

        gujarati.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.GUJARATI);
        });

        irish.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.IRISH);
        });

        indonesian.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.INDONESIAN);
        });


        italian.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.ITALIAN);
        });


        kannada.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.KANNADA);
        });

        korean.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.KOREAN);
        });

        malayalam.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.MALAYALAM);
        });

        marathi.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.MARATHI);
        });

        malay.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.MALAY);
        });


        nepali.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.NEPALI);
        });

        oriya.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.ORIYA);
        });

        portuguese.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.PORTUGUESE);
        });

        russian.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.RUSSIAN);
        });

        tamil.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.TAMIL);
        });

        telugu.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.TELUGU);
        });

        urdu.setOnClickListener(view -> {
            dismiss();
            languageChangeListener.onLanguageSelected(Language.URDU);
        });
    }

    public void setLanguageChangeListener(LanguageChangeListener languageChangeListener) {
        this.languageChangeListener = languageChangeListener;
    }
}
