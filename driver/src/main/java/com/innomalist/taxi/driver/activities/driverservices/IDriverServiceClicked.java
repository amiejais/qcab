package com.innomalist.taxi.driver.activities.driverservices;

import com.innomalist.taxi.driver.models.DriverServiceData;

public interface IDriverServiceClicked {
    void onServiceClicked(DriverServiceData id);
}
