package com.innomalist.taxi.driver.activities.driverservices;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.databinding.ActivityDriverServiceBinding;
import com.innomalist.taxi.driver.events.SerDriverServicesEvent;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.driver.models.DriverServiceData;
import com.innomalist.taxi.driver.models.DriverServiceResponse;
import com.innomalist.taxi.driver.events.GetDriverServiceEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class DriverServiceActivity extends BaseActivity implements IDriverServiceClicked {

    private ActivityDriverServiceBinding mServiceBinding;
    private RecyclerView mDriverServiceRecycleView;
    private List<DriverServiceData> mDriverServiceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceBinding = DataBindingUtil.setContentView(this, R.layout.activity_driver_service);

        initializeToolbar("Driver Services", true);
        mDriverServiceRecycleView = mServiceBinding.rvServiceRecycleView;
        eventBus.post(new GetDriverServiceEvent(3));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.driver_service_apply, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        applyChanges();
        return super.onOptionsItemSelected(item);
    }

    private void applyChanges() {
        if (mDriverServiceList == null || mDriverServiceList.isEmpty()) {
            return;
        }
        ArrayList<Integer> mServiceIdList = new ArrayList<>();
        for (DriverServiceData driverServiceData : mDriverServiceList) {
            if (driverServiceData.getPrimary() != 1 && driverServiceData.isIs_active() == 1) {
                mServiceIdList.add(driverServiceData.getId());
            }
        }

        if (!mServiceIdList.isEmpty())
            eventBus.post(new SerDriverServicesEvent(mServiceIdList));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDriverServiceUpdateResponse(CommonResponse commonResponse) {
        if (commonResponse.status == 200) {
            showAlert(getString(R.string.service_update_msg));
        } else {
            showAlert(getString(R.string.unknown_error_msg));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDriverServiceResponse(DriverServiceResponse driverServiceResponse) {
        if (driverServiceResponse.status == 200) {
            mDriverServiceList = driverServiceResponse.getData();
            mDriverServiceRecycleView.setLayoutManager(new LinearLayoutManager(this));
            DriverServiceAdapter mAdapter = new DriverServiceAdapter(this, this, mDriverServiceList);
            mDriverServiceRecycleView.setAdapter(mAdapter);
        } else {
            showAlert(getString(R.string.unknown_error_msg));
        }

    }

    @Override
    public void onServiceClicked(DriverServiceData id) {
        for (DriverServiceData driverServiceData : mDriverServiceList) {
            if (driverServiceData.getId() == id.getId()) {
                driverServiceData.setIs_active(id.isIs_active());
                return;
            }
        }
    }
}
