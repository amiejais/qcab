package com.innomalist.taxi.driver.activities.splash;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.innomalist.taxi.common.activities.signup.SignUpSecondStepActivity;
import com.innomalist.taxi.common.components.BaseActivity;
import com.innomalist.taxi.common.events.BackgroundServiceStartedEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageEvent;
import com.innomalist.taxi.common.events.ChangeProfileImageResultEvent;
import com.innomalist.taxi.common.events.ConnectEvent;
import com.innomalist.taxi.common.events.ConnectResultEvent;
import com.innomalist.taxi.common.events.GetAppVersionResultEvent;
import com.innomalist.taxi.common.events.LoginEvent;
import com.innomalist.taxi.common.events.NotificationPlayerId;
import com.innomalist.taxi.common.models.AppVersion;
import com.innomalist.taxi.common.models.Driver;
import com.innomalist.taxi.common.models.DriverResultEvent;
import com.innomalist.taxi.common.utils.AlertDialogBuilder;
import com.innomalist.taxi.common.utils.AlerterHelper;
import com.innomalist.taxi.common.utils.CommonUtils;
import com.innomalist.taxi.common.utils.LocaleHelper;
import com.innomalist.taxi.common.utils.MyPreferenceManager;
import com.innomalist.taxi.driver.BuildConfig;
import com.innomalist.taxi.driver.DriverEventBusIndex;
import com.innomalist.taxi.driver.R;
import com.innomalist.taxi.driver.activities.main.MainActivity;
import com.innomalist.taxi.driver.databinding.ActivitySplashBinding;
import com.innomalist.taxi.driver.models.CommonResponse;
import com.innomalist.taxi.driver.services.BasicDeviceAdminReceiver;
import com.innomalist.taxi.driver.services.DriverService;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;

public class SplashActivity extends BaseActivity implements LanguageChooseDialog.LanguageChangeListener {

    private MyPreferenceManager mPref;
    private ActivitySplashBinding mBinding;
    private DevicePolicyManager mDevicePolicyManager;
    private ComponentName mComponentName;
    private Long mMobileNumber;
    private int mCount = 0;
    private static final int RC_SIGN_IN = 123;
    private static final int REGISTER_INTENT = 124;
    private static final int ADMIN_INTENT = 1011;
    private boolean isFromNote;
    private boolean isLoginSelected;
    private boolean isRegInProcess = false;
    private String mProfilePicPath;


    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            try {
                if (!isMyServiceRunning(DriverService.class))
                    startService(new Intent(SplashActivity.this, DriverService.class));

            } catch (Exception c) {
                c.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {

        }
    };
    /*private View.OnClickListener onLoginClicked = v -> {
        String resourceName = "testMode";
        int testExists = SplashActivity.this.getResources().getIdentifier(resourceName, "string", SplashActivity.this.getPackageName());
        if (testExists > 0) {
            tryLogin(getString(testExists));
            return;
        }
        if (getResources().getBoolean(R.bool.use_custom_login)) {
            startActivityForResult(new Intent(SplashActivity.this, LoginActivity.class), RC_SIGN_IN);
        } else
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .setTheme(getCurrentTheme())
                            .build(),
                    RC_SIGN_IN);
    };*/


    private void startNumberVerification() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                        .setTheme(getCurrentTheme())
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = LocaleHelper.onAttach(newBase);
        super.attachBaseContext(newBase);
    }

    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setImmersive(true);
        showConnectionDialog = false;
        OneSignal.setSubscription(true);
        try {
            EventBus.builder().addIndex(new DriverEventBusIndex()).installDefaultEventBus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        mBinding.loginButton.setOnClickListener(view -> {
            isLoginSelected = true;
            startNumberVerification();
        });
        mBinding.registrationButton.setOnClickListener(view -> {
            isLoginSelected = false;
            startNumberVerification();
        });
        mPref = MyPreferenceManager.getInstance(this);

        if (mPref.getString("LANG", null) == null) {
            mPref.putString("LANG", "en");
        }

        mBinding.chooseLanguage.setText(mPref.getString("LANG", "EN"));
        mBinding.chooseLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LanguageChooseDialog languageChooseDialog = new LanguageChooseDialog(SplashActivity.this);
                languageChooseDialog.setLanguageChangeListener(SplashActivity.this);
                languageChooseDialog.show();
            }
        });
        isFromNote = getIntent().getBooleanExtra("IsFromNote", false);
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(
                Context.DEVICE_POLICY_SERVICE);
        mComponentName = new ComponentName(this, BasicDeviceAdminReceiver.class);

        Log.e("TAg", "onCreate tryConnect");
        isRegInProcess = false;
        tryConnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mDevicePolicyManager.isAdminActive(mComponentName)) {
            askDeviceAdminPermission();
        } else {
            checkPermissions();
        }

    }

    private void checkPermissions() {
        if (!CommonUtils.isGPSEnabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_enable_gps), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        if (CommonUtils.isInternetDisabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_internet_connection), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    private void tryLogin(String phone) {
        goToLoadingMode();
        if (phone.substring(0, 1).equals("+"))
            phone = phone.substring(1);

        mMobileNumber = Long.valueOf(phone);
        eventBus.post(new LoginEvent(Long.valueOf(phone), BuildConfig.VERSION_CODE));
    }

    public void tryConnect() {
        try {
            Log.e("TAg", "tryConnect");
            String token = mPref.getString("driver_token", null);
            if (token != null && !token.isEmpty()) {
                eventBus.post(new ConnectEvent(token));
                goToLoadingMode();
            } else {
                goToLoginMode();
            }
        } catch (Exception c) {
            c.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        Log.e("TAg", "showError tryConnect");
        if (event.hasError()) {
            goToLoginMode();
            event.showError(SplashActivity.this, result -> {
                Log.e("TAg", "showError tryConnect");
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    tryConnect();
            });
            return;
        }

        if (!isLoginSelected && isRegInProcess) {
            EventBus.getDefault().post(new ChangeProfileImageEvent(mProfilePicPath));
        } else {
            CommonUtils.driver = new Gson().fromJson(mPref.getString("driver_user", "{}"), Driver.class);
            if (mCount == 0) {
                eventBus.post(new AppVersion());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileImageChanged(ChangeProfileImageResultEvent event) {
        if (event.hasError()) {
            Toast.makeText(this, getString(R.string.unable_upload_file), Toast.LENGTH_SHORT).show();
            return;
        }
        appFinishDialog(getString(R.string.disable_msg));

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAppVersionResult(GetAppVersionResultEvent event) {
        if (event.status == 200) {
            Log.e("TAg", "Driver Version : " + event.data.driver_min_ver_android);
            if (mCount == 0) {
                mCount++;
                if (BuildConfig.VERSION_CODE < event.data.driver_min_ver_android) {
                    showVersionAlert();
                } else {
                    sendNotificationPlayerIdToServer();
                }
            }
        } else {
            mPref.clearPreferences();
            appFinishDialog(getString(R.string.network_issue_msg));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginResultEvent(DriverResultEvent event) {
        if (event.status == 200) {
            CommonUtils.driver = event.user;
            mPref.putString("driver_user", new Gson().toJson(event.user, Driver.class));
            mPref.putString("driver_token", event.token);
            tryConnect();
        } else if (event.status == 411) {
            appFinishDialog(getString(R.string.disable_msg));
        } else if (event.status == 412) {
            appFinishDialog(getString(R.string.blocked_msg));
        } else if (event.status == 413) {
            tryRegistration("" + mMobileNumber);
        }
    }

    private void sendNotificationPlayerIdToServer() {
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();

        if (status != null) {
            eventBus.post(new NotificationPlayerId(status.getSubscriptionStatus().getUserId()));
        } else {
            OneSignal.idsAvailable((userId, registrationId) -> {
                Log.d("debug", "User:" + userId);
                eventBus.post(new NotificationPlayerId(userId));

            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationSentResponse(CommonResponse event) {
        Log.e("TAg", "onNotificationSentResponse: " + event.status);
        if (event.status == 200) {
            startMainActivity();
        } else {
            mPref.clearPreferences();
            appFinishDialog(getString(R.string.login_issue_msg));
        }
    }

    private void startMainActivity() {
        String token = mPref.getString("driver_token", null);
        if (TextUtils.isEmpty(token)) {
            appFinishDialog(getString(R.string.relogin));
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (isFromNote) {
            intent.putExtra("IsFromNote", true);
        }
        startActivity(intent);
        finish();
    }

    private void goToLoadingMode() {
        mBinding.llSigninSignup.setVisibility(View.GONE);
        mBinding.progressBar.setVisibility(View.VISIBLE);
    }

    private void goToLoginMode() {
        mBinding.llSigninSignup.setVisibility(View.VISIBLE);
        mBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                String phone;
                if (idpResponse != null) {
                    phone = idpResponse.getPhoneNumber();
                    if (TextUtils.isEmpty(phone)) {
                        return;
                    }
                    if (isLoginSelected) {
                        tryLogin(phone);
                    } else {
                        tryRegistration(phone);
                    }
                }
                return;
            }
            AlerterHelper.showError(SplashActivity.this, getString(R.string.login_failed));
            goToLoginMode();
        } else if (requestCode == REGISTER_INTENT) {
            if (resultCode == RESULT_OK) {
                int status = data.getIntExtra("statusCode", -1);
                int uploadCode = data.getIntExtra("uploadCode", -1);
                mProfilePicPath = data.getStringExtra("profilePicPath");
                if (status == 411 && uploadCode == 200) {
                    if (!TextUtils.isEmpty(mProfilePicPath)) {
                        isRegInProcess = true;
                        completeRegistration();
                    } else {
                        appFinishDialog(getString(R.string.disable_msg));
                    }
                } else if (status == 411 && uploadCode != 200) {
                    if (!TextUtils.isEmpty(mProfilePicPath)) {
                        isRegInProcess = true;
                        completeRegistration();
                    } else {
                        appFinishDialog(getString(R.string.disable_msg));
                    }
                } else if (status == 413) {
                    appFinishDialog(getString(R.string.already_exist_msg));
                } else {
                    appFinishDialog(getString(R.string.unknown_error_msg));
                }
            } else {
                finish();
            }
            return;
        } else if (requestCode == ADMIN_INTENT) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this, getString(R.string.alert_denied_admin_perm_msg), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }


    //arabic;dutch;gujarati;irish;indonesian;italian;kannada;korean;malayalam;marathi;malay;nepali;oriya;portuguese;russian;tamil;telugu;urdu;german;spanish;
    @Override
    public void onLanguageSelected(LanguageChooseDialog.Language language) {
        Toast.makeText(this, String.valueOf(language), Toast.LENGTH_LONG).show();
        if (language == LanguageChooseDialog.Language.HINDI) {
            mPref.putString("LANG", "HI");
            LocaleHelper.setLocale(this, "hi");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ENGLISH) {
            mPref.putString("LANG", "EN");
            LocaleHelper.setLocale(this, "en");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.SPANISH) {
            mPref.putString("LANG", "ES");
            LocaleHelper.setLocale(this, "es");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.BENGALI) {
            mPref.putString("LANG", "BN");
            LocaleHelper.setLocale(this, "bn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PUNJABI) {
            mPref.putString("LANG", "PA");
            LocaleHelper.setLocale(this, "pa");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.FRENCH) {
            mPref.putString("LANG", "FR");
            LocaleHelper.setLocale(this, "fr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ARABIC) {
            mPref.putString("LANG", "AR");
            LocaleHelper.setLocale(this, "ar");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.IRISH) {
            mPref.putString("LANG", "GA");
            LocaleHelper.setLocale(this, "ga");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GUJARATI) {
            mPref.putString("LANG", "GU");
            LocaleHelper.setLocale(this, "gu");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.GERMAN) {
            mPref.putString("LANG", "DE");
            LocaleHelper.setLocale(this, "de");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.INDONESIAN) {
            mPref.putString("LANG", "ID");
            LocaleHelper.setLocale(this, "id");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ITALIAN) {
            mPref.putString("LANG", "IT");
            LocaleHelper.setLocale(this, "it");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KANNADA) {
            mPref.putString("LANG", "KN");
            LocaleHelper.setLocale(this, "kn");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.KOREAN) {
            mPref.putString("LANG", "KO");
            LocaleHelper.setLocale(this, "ko");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAYALAM) {
            mPref.putString("LANG", "ML");
            LocaleHelper.setLocale(this, "ml");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MALAY) {
            mPref.putString("LANG", "MS");
            LocaleHelper.setLocale(this, "ms");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.MARATHI) {
            mPref.putString("LANG", "MR");
            LocaleHelper.setLocale(this, "mr");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.NEPALI) {
            mPref.putString("LANG", "NE");
            LocaleHelper.setLocale(this, "ne");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.DUTCH) {
            mPref.putString("LANG", "NL");
            LocaleHelper.setLocale(this, "nl");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.ORIYA) {
            mPref.putString("LANG", "OR");
            LocaleHelper.setLocale(this, "or");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.PORTUGUESE) {
            mPref.putString("LANG", "PT");
            LocaleHelper.setLocale(this, "pt");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.RUSSIAN) {
            mPref.putString("LANG", "RU");
            LocaleHelper.setLocale(this, "ru");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TAMIL) {
            mPref.putString("LANG", "TA");
            LocaleHelper.setLocale(this, "ta");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.TELUGU) {
            mPref.putString("LANG", "TE");
            LocaleHelper.setLocale(this, "te");
            this.recreate();
        } else if (language == LanguageChooseDialog.Language.URDU) {
            mPref.putString("LANG", "UR");
            LocaleHelper.setLocale(this, "ur");
            this.recreate();
        }
    }


    private void tryRegistration(String mobileNumber) {
        startActivityForResult(new Intent(this, SignUpSecondStepActivity.class).putExtra("User", getString(com.innomalist.taxi.common.R.string.driver_registration)).putExtra("Mobile", "" + mobileNumber), REGISTER_INTENT);
    }

    private void showVersionAlert() {
        new AlertDialog.Builder(this).setCancelable(false).setMessage(getString(R.string.imp_update_msg))
                .setTitle("Alert")
                .setCancelable(false)
                .setPositiveButton(getString(R.string.alert_ok), (dialog, which) -> {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }).setNegativeButton(getString(R.string.alert_cancel), (dialog, which) -> finish()).create().show();
    }

    private void askDeviceAdminPermission() {

        new android.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(getString(R.string.device_admin_permission_msg))
                .setPositiveButton(getString(R.string.alert_ok), (dialog, which) -> {

                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Register for admin");
                    startActivityForResult(intent, ADMIN_INTENT);
                    dialog.dismiss();
                    dialog.cancel();
                }).setNegativeButton(getString(R.string.alert_cancel), (dialog, which) -> {
            dialog.dismiss();
            finish();
        }).create().show();
    }

    private void completeRegistration() {
        eventBus.post(new ConnectEvent(mPref.getString("driver_token_1", "")));
        goToLoadingMode();
    }

    private void appFinishDialog(String string) {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.test))
                .cancelable(false)
                .content(string)
                .positiveText(com.innomalist.taxi.common.R.string.alert_ok)
                .positiveText(getString(R.string.alert_ok))
                .onPositive((dialog, which) -> {
                    mPref.clearPreferences();
                    finish();
                }).show();
    }
}
