package com.innomalist.taxi.driver.events;

import com.google.gson.Gson;
import com.innomalist.taxi.common.models.Rider;
import com.innomalist.taxi.common.models.Travel;

public class RiderAcceptedEvent {
    public Travel travel;

    public RiderAcceptedEvent(Object... args) {
        this.travel = Travel.fromJson(args[0].toString());
        travel.setRider(new Gson().fromJson(args[1].toString(), Rider.class));
    }
}
